<?php
require_once('vendor/bin/connection.php');
require_once("vendor/class/usuario/usuarios_data.php");
require_once("vendor/class/propiedad/propiedad_data.php");
require_once("vendor/class/soporte/soporte_data.php");
require_once("vendor/class/utilidades.php");
/* RECUERDAME DE INDEX */

$user  = "";
$hash = ""; 
    session_start();
    if(isset($_SESSION["hash512"])){
        $bd = connection::getInstance()->getDb();
        $user  = $_SESSION["nombre"];
        $hash = $_SESSION["hash512"];
    }else{
        header("Location:ingresar.php");
    }

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="busca hogar en buenos aires  inmuebles, hogar, venta, alquiler, temporal, simple, facil, accesible, pruebalo gratis, mas filtros de busqueda">
    <title>Panel Soporte - BuscaHogar</title>  
    <?php include_once("vendor/includes/metas.php");  ?>

    <!-- Page level plugin CSS-->
    <link href="vendor/plugin/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <style type="text/css">
        .list-group-item.active {
            z-index: 2;
            color: #fff;
            background-color: #2b84c2;
            border-color: #2b84c2;
        }

        .page-item.active .page-link {
            z-index: 1;
            color: #fff;
            background-color: #2b84c2;
            border-color: #007bff;
        }

        #accordion{
            overflow-y: scroll;
            height: 300px; 
        }
    </style>
  </head>

  <body>
  <div id="loader-wrapper" class="loader-wrapper">
    <div id="loader" class="loader"></div>
  </div>
    <!-- Navigation -->
    <?php include_once("vendor/includes/header.php");  ?>

    <!-- Page Content -->
    <div class="container contenedor pt-5">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class="mt-4 mb-3">Panel de Soporte
        <!--small>Subheading</small-->
        </h1>

        <?php 
          //Utilidades::obtener_menu(4);
        ?>

        <hr>
        <input type="hidden" id="hash" name="hash" value ="<?php echo $hash ?>">
        <div class="row pt-1 pb-4">
        
            <div class="col-12 py-3">
                <div class="row">
                    <div class="col-6 py-3 border-left">
                        <p class="h4">Preguntas Frecuentes</p>
                        <hr>

                        <?php echo Soporte::obtener_preguntas_frecuentes($bd); ?>

                    </div>
                    <div class="col-6 py-3">
                        <p class="h4">Envianos un mensaje</p>
                        <hr>
                                <form name="sentMessage" id="contactForm">
                                        <div class="control-group form-group">
                                            <div class="controls">
                                                <label>Titulo:</label>
                                                <input type="text" class="form-control " id="name_consulta">
                                                <div id="error_name_consulta" class="text-danger" style="display:none">
                                                        <i class="fa fa-exclamation"></i><small> Ingrese su nombre</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group">
                                            <div class="controls">
                                                <label>Mensaje:</label>
                                                <textarea rows="3" cols="100" class="form-control" id="txt_consulta" maxlength="999" style="resize:none"></textarea>
                                            </div>
                                                <div id="error_txt_consulta" class="text-danger" style="display:none">
                                                        <i class="fa fa-exclamation"></i><small> Ingrese su mensaje</small>
                                                </div>
                                        </div>

                                        <div class="control-group form-group">
                                            <button type="submit" class="btn btn-info pull-right text-right" id="contactar">Enviar</button>
                                        </div>
                                        <br><br>
                                        <div id="success"  class="pt-2" style="display:none">
                                                <div class="alert alert-success fade show" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <i class="fa fa-thumbs-up"></i>
                                                        Su mensaje ha sido enviado correctamente

                                                </div>
                                        </div>
                                </form>
                    </div>  
                </div> 
            </div>

            <div class="col-md-8 pb-3">
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered hover" id="dataTable" width="100%" cellspacing="0" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Tipo</th>
                          <th>Estatus</th>
                          <th>Accion</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>#</th>
                          <th>Tipo</th>
                          <th>Estatus</th>
                          <th>Accion</th>
                        </tr>
                      </tfoot>

                      <tbody id="lm">
                        <?php include_once("vendor/class/soporte/lista_mensajes_soporte.php") ?> 
                      </tbody>
                    </table>
                </div>
            </div>
            
            <div class="col-md-4 mb-5">
                <hr>
                <div class="chat-card card">
                    <div class="card-header">
                        <i class="fa fa-comments fa-fw"></i> Chat
                            <div class="btn-group pull-right">
                                <!--a href="#">
                                    <i class="fa fa-refresh fa-fw"></i>
                                </a-->
                                <!--a href="#">
                                    <i class="fa fa-external-link fa-fw"></i>
                                </a-->
                            </div>
                    </div>
                
                <div class="card-body">

                    <div class="list-group list-group-flush small">
                        <ul id="chating" class="chat">

                        </ul>
                    </div>
                    
                </div>

                <div class="card-footer text-muted">
                <textarea id="mensaje" class="col-12" disabled></textarea>

                      <button id="enviar" class="btn btn-outline-info btn-sm text-right pull-right" type="button" disabled>Enviar</button>
   
               
                    <!--div class="input-group">
                      <input type="text" class="form-control form-control-sm" placeholder="Escribe tu mensaje">
                       
                      <div class="input-group-append">
                        <button class="btn btn-outline-info btn-sm" type="button">Enviar</button>
                      </div>
                    </div-->

                  </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

     <!-- Modal Eliminar -->
    <?php include_once("vendor/includes/modal_eliminar.php");  ?>

    <!-- Footer -->
    <?php include_once("vendor/includes/footer.php");  ?>

    <!-- Bootstrap core JavaScript -->
    <?php include_once("vendor/includes/jsreferences.php");  ?>
    <!-- Custom scripts for this page-->
    <script src="vendor/plugin/datatables/jquery.dataTables.js"></script>
    <script src="vendor/plugin/datatables/dataTables.bootstrap4.js"></script>
    <script src="vendor/plugin/datatables/sb-admin-datatables.min.js"></script>
    <script src="js/utilidades.js"></script>
    <script type="text/javascript">

        var id = "";
        var codigo = "";

        $(document).ready(function(){
            $("#loader-wrapper").fadeOut("slow");
        });

        $("a.eliminar").click(function(e){

            id = $(this).attr("cod");
            //console.log(id);

            titulo = $("tr[id="+id+"]").find(".titulo_prop").html();
            //codigo = $("tr[id="+id+"]").find(".codigo_prop").html();

            //console.log(titulo);
            $('#modal_trash').find(".modal-body").html("<strong>Mensaje N# "+ id+"</strong>");

            $('#modal_trash').modal({
                backdrop: 'static',
                keyboard: false
            })
        });

        $('#erase').click(function(e){
            eliminar(id);
            //console.log(id);
        });

        function eliminar(id){
            $.ajax({
                data:  {accion: 6,id : id},
                url:   'vendor/class/soporte/soporte_acciones.php',
                type:  'post',
                dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                    //console.log(data);
                    $('#modal_trash').modal('hide');
                    if(data.estado == 0){
                        //$("#alert_wrong").show();
                    }
                    else{
                        //$("#alert_ok").show();
                        ///window.location.href="mensajes.php";
                        hash = $("#hash").val();
                        cargar_lista(hash);
                    }
                },
                error: function(data){
                    console.log(data);
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/
        }

        $("btn.chat_prop").click(function(e){

            id = $(this).attr("cod");
            console.log(id);
            cargar_chat(id);
        });

        function cargar_chat(id){
            $("#enviar").prop('disabled', false);
            $("#mensaje").prop('disabled', false);
            $.ajax({
                data:  {accion: 7,id : id},
                url:   'vendor/class/soporte/lista_mensajes_chat_soporte.php',
                type:  'post',
                //dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                    //console.log(data);
                        $("#chating").html(data);
                        $(".card-body").scrollTop($(".card-body")[0].scrollHeight);
                        //window.location.href="mensajes.php";
                },
                error: function(data){
                    console.log(data);
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/
        }


        $("#enviar").click(function(e){
          
            hash = $("#hash").val();
            mensaje = $("#mensaje").val();
            //console.log(mensaje);
            if(mensaje.trim() != ""){
                //console.log(id);
                ///console.log("mensaje: "+mensaje);
                //console.log("hash: "+hash);
                enviar_mensaje(id, mensaje, hash);
            }

        });

        function enviar_mensaje(id, mensaje, hash){


            $.ajax({
                data:  {accion: 3,id : id, mensaje : mensaje, hash : hash},
                url:   'vendor/class/soporte/soporte_acciones.php',
                type:  'post',
                dataType: "json",
                success:  function (data) {
                    //console.log(data);
                    if(data.estado == 1){
                       cargar_chat(id);
                       $("#mensaje").val("");
                    }
                },
                error: function(data){
                    console.log(data);
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/
        }

        $('#contactar').click(function(e){
            e.preventDefault();
            error = false;

            validar_inputs("#name_consulta", "#error_name_consulta");
            validar_inputs("#txt_consulta", "#error_txt_consulta");

            if(!error){
                hash = $("#hash").val();
                titulo = $("#name_consulta").val();
                msj = $("#txt_consulta").val();

                //console.log(msj);
                $.ajax({
                        data:  {accion: 2, titulo : titulo, msj : msj, hash, hash},
                        url:   'vendor/class/soporte/soporte_acciones.php',
                        type:  'post',
                        dataType: "json",
                        success:  function (data) {
                                //respuesta = JSON.stringify(data);
                                //console.log(data);
                            $("#name_consulta").val("");
                            $("#txt_consulta").val("");

                            if(data.estado == 0){
                                    $("#success").hide(); 
                            }
                            else{
                                    $("#success").show();
                            }
                            cargar_lista(hash);
                        },
                        error: function(data){
                                console.log(data);
                             // window.location.href="cuenta.php?success=no";
                        }
                });/**/
            }
        });

        function cargar_lista(hash){

            $.ajax({
                data:  {hash : hash},
                url:   'vendor/class/soporte/lista_mensajes_soporte.php',
                type:  'post',
                //dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                    //console.log(data);
                        $("#lm").html(data);
                        //$(".card-body").scrollTop($(".card-body")[0].scrollHeight);
                        //window.location.href="mensajes.php";
                },
                error: function(data){
                    console.log(data);
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/
        }
          
    </script>

  </body>

</html>
