<?php
require_once('vendor/bin/connection.php');
require_once("vendor/class/usuario/usuarios_data.php");
require_once("vendor/class/utilidades.php");

$bd = connection::getInstance()->getDb();
$user  = "";

    /* RECUERDAME DE INDEX */
    //if(isset($_COOKIE["recuerdame"]) && !empty($_COOKIE["recuerdame"])){
        session_start();

        if(isset($_SESSION["hash512"])){
            $user  = $_SESSION["nombre"];
        }
    //}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php Utilidades::obtener_meta($bd); ?>">
  <title>Terminos y Condiciones - BuscaHogar</title>  
  <?php include_once("vendor/includes/metas.php");  ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120003817-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-120003817-1');
    </script>

</head>

<body>
    <?php include_once("vendor/includes/header.php");  ?>
    <div class="container pt-5">
        <div class="row my-5 mx-4 ">
            <div class="col-md-12 col-sm-12 col-xs-12 bg-grey padding-20 margin-bottom-20 rounded py-5 px-5 shadow-sm">
                <small>
                    <?php echo Utilidades::obtener_terminos($bd);?>
                </small>
            </div> 
        </div>
    </div>

  <!-- Footer -->
    <?php include_once("vendor/includes/footer.php");  ?>

    <!-- Bootstrap core JavaScript -->
    <?php include_once("vendor/includes/jsreferences.php");  ?>

</body>

</html>
