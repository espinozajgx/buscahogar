<?php
require_once('vendor/bin/connection.php');
require_once("vendor/class/propiedad/propiedad_data.php");
require_once("vendor/class/utilidades.php");
/* RECUERDAME DE INDEX */

$user  = "";
$hash  = "";
$limit = 10;
$offset = 0;

    session_start();
    if(isset($_SESSION["hash512"])){
        $bd = connection::getInstance()->getDb();
        $user  = $_SESSION["nombre"];
        $hash  = $_SESSION["hash512"];
    }
    else{
        header("Location:ingresar.php");
    }
    $active = 0;
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>Mis Anuncios - BuscaHogar</title>  
    <?php include_once("vendor/includes/metas.php");  ?>
    <link href="vendor/plugin/bootstrap-toggle/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
        .noactive {
            background-color: #f5f5f5 !important;
            font-style: italic;
            /*color: #b8b8b8 !important;/**/
            
        }

        .noactivea {
            opacity: 0.5;
            cursor: not-allowed;
            pointer-events: none;
        }

        .img-panel{
            max-width: 100%;
            height: 235px;
        }

        .cuerpo-panel{
            max-height: 280px;
        }

        /*#activas{
            overflow-y: scroll;
            height: 1024px; 
        }*/
    </style>
  </head>

  <body>
  <div id="loader-wrapper" class="loader-wrapper">
    <div id="loader" class="loader"></div>
  </div>

    <!-- Navigation -->
    <?php include_once("vendor/includes/header.php");  ?>

    <!-- Page Content -->
    <div class="container mt-2">
    
        <!-- Page Heading/Breadcrumbs -->
        <h1 class="mt-5 mb-3">Mi Cuenta
        <!--small>Subheading</small-->
        </h1>
        <?php Utilidades::obtener_menu(2);?>
        <hr>


        <div class="row">
            <input type="hidden" id="limit" value="<?php echo $limit ?>">
            <input type="hidden" id="hash" value="<?php echo $hash ?>">

                <!-- Sidebar Widgets Column -->
                <div class="col-12 col-xs-12 col-sm-12 col-md-3 col-lg-3 mb-3">
                    <div class="row">
                            <!-- Search Widget -->
                            <div class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
                                <div class="card mb-4">
                                    <h5 class="card-header"><small>Buscar</small></h5>
                                
                                    <div class="card-body">
                                        <div class="input-group">
                                            <input id="code" type="text" class="form-control form-control " placeholder="Codigo">
                                            <span class="input-group-btn">
                                              <button id="search" class="btn btn-info btn" type="button"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <!-- Operacion Widget -->
                            <div class="col-xs-4 col-sm-4 col-md-12 col-lg-12">  
                                <div class="card mb-4">
                                    <h5 class="card-header"><small>Operacion</small></h5>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <select id="tipo_ope" class="custom-select" title="Escoger">
                                                    <option value="0">Elegir</option>
                                                    <option value="1">Ventas</option>
                                                    <option value="2">Alquiler</option>
                                                    <option value="3">Alquiler Temporal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Propiedad Widget -->
                            <div class="col-xs-4 col-sm-4 col-md-12 col-lg-12">  
                              <div class="card mb-4">
                                <h5 class="card-header"><small>Propiedad</small></h5>
                                <div class="card-body">
                                  <div class="row">
                                    <div class="col-lg-12">
                                      <select id="tipo_prop" class="custom-select" title="Escoger">
                                        <option value="0">Elegir</option>
                                        <option value="1">Departamento</option>
                                        <option value="2">Casa</option>
                                        <option value="3">Terreno</option>
                                        <option value="4">Oficina</option>
                                        <option value="5">Cochera</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    </div>
                </div>

                <!-- Blog Entries Column -->
                <div class="col-12 col-xs-12 col-sm-12 col-md-9 col-lg-9">   

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Activos</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Vencidos</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Suspendidos</a>
                      </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">

                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                           
                            <?php include_once("vendor/class/propiedad/propiedades_activas.php"); ?>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                           
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                           
                            <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Buscahogar!</h4>
                            <hr>
                            <p class="mb-0">Si tu publicacion entra en esta seccion es por que has infrigido los Terminos y Condiciones!</p>
                            </div>
                            <?php include_once("vendor/class/propiedad/propiedades_suspendidas.php"); ?>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
          <!-- /.row -->

    </div>
    <!-- /.container -->

     <!-- Modal Eliminar -->
    <?php include_once("vendor/includes/modal_eliminar.php");  ?>

    <!-- Footer -->
    <?php include_once("vendor/includes/footer.php");  ?>

    <!-- Bootstrap core JavaScript -->
    <?php include_once("vendor/includes/jsreferences.php");  ?>
    <script src="vendor/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="js/facebook.js"></script>
    <script type="text/javascript">
        var codigo = "";
        var id_tipo_oper = 1
        var id_tipo_prop = 1
        var codigo = ""
        var estado="";
        var busquedas = 0;

        $(document).ready(function(){
            $("#loader-wrapper").fadeOut("slow");
        });

        function compartir(url){
            FB.ui({
              method: 'share',
              href: url
            }, function(response){
                // Debug response (optional)
                console.log(response);
            });
        }

        $("a.shared").on("click",function(e){
            url = $(this).attr("data-href");
            //console.log($(this).attr("data-href"));
            compartir(url);
        });
        
        $('.button_on').click(function() {
            estado="";
            idrow = $(this).attr("cod");
            idelement = this.id;
            console.log("id: " + idrow);
            estatus(idrow,idelement);
            
        });

        function estatus(idrow,idelement){
            //console.log("estatus asignado");
            if($('#' + idelement + ' > .off').css("display")=="none"){

                console.log("apagando");

                estado=0;
                $('#' + idelement + ' > .off, #' + idelement + ' > .on').toggle();

                $('#'+idrow).addClass("noactive");
                $('#'+idrow).find("a.btn-link").addClass("noactivea");

                $('#'+idrow).find("span").removeClass("badge-info").addClass("badge-secondary");
                cambiar_estatus(idrow, estado);
            }else{
                console.log("encendiendo");
                estado=1;
                $('#'+idrow).removeClass("noactive");
                $('#'+idrow).find("img").removeClass("noactivea");
                $('#'+idrow).find("span").removeClass("badge-secondary").addClass("badge-info");
                $('#'+idrow).find("a.btn-link").removeClass("noactivea");
                
                $('#' + idelement + ' > .off, #' + idelement + ' > .on').toggle();
                cambiar_estatus(idrow, estado);
            }   /**/
        }

        function cambiar_estatus(codigo, estado){
            hash = $("#hash").val();

            $.ajax({
                data:  {accion: 4,codigo : codigo, hash : hash, estatus: estado},
                url:   'vendor/class/propiedad/propiedad_acciones.php',
                type:  'post',
                dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                    //console.log(data);
                    //$('#modal_trash').modal('hide');
                    if(data.estado == 0){
                        //$("#alert_wrong").show();
                    }
                    else{
                        //$("#alert_ok").show();
                        //window.location.href="favs.php";
                    }
                },
                error: function(data){
                    console.log(data);
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/

        }

        $(".form-control").on("keyup",function(e){
            var id=$(this).attr("id");

            if(id != null)
                if(id=="place" || id=="street" || id=="high" || id=="price" || id=="surface" || id=="bathroom" || id=="elevator"){
                    $("#"+id).removeClass('is-invalid').addClass('is-valid'); 

                    //ocultar_err_msg("#error_"+id); 
                }
        });

        $(".eliminar").click(function() {
            codigo = $(this).attr("cod")
            //console.log(codigo);
            $("#body_trash").html($("#"+codigo).find(".name_prop").html());
        });/**/

        $("#erase").click(function() {
            //codigo = $(this).attr("cod")
            //console.log(codigo);
            eliminar_estatus(codigo, 3);
        });/**/

        function eliminar_estatus(codigo, estado){
            hash = $("#hash").val();

            $.ajax({
                data:  {accion: 4,codigo : codigo, hash : hash, estatus: estado},
                url:   'vendor/class/propiedad/propiedad_acciones.php',
                type:  'post',
                dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                   // console.log(data);
                    //$('#modal_trash').modal('hide');
                    if(data.estado == 0){
                        //$("#alert_wrong").show();
                    }
                    else{
                        //$("#alert_ok").show();
                        window.location.href="panel";
                    }
                },
                error: function(data){
                    console.log(data);
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/

        }

        $("#tipo_ope").on("change",function(){
            id_tipo_oper = $("#tipo_ope").val();
            id_tipo_prop = $("#tipo_prop").val();
            codigo = "";

            //console.log(id_tipo_oper);
            cargar_panel(id_tipo_oper, id_tipo_prop, codigo)

        });

        $("#tipo_prop").on("change",function(){
            id_tipo_prop = $("#tipo_prop").val();
            id_tipo_oper = $("#tipo_ope").val();
            codigo = "";

            //console.log(id_tipo_prop);
            cargar_panel(id_tipo_oper, id_tipo_prop, codigo)
        });
        
        $("#search").on("click",function(e){
            e.preventDefault();
            codigo = $("#code").val().trim();
            id_tipo_prop = 0;
            id_tipo_oper = 0;

            //console.log(codigo);
            cargar_panel(id_tipo_oper, id_tipo_prop, codigo);
        });

        function cargar_panel(id_tipo_oper, id_tipo_prop, codigo){
            busquedas++;
            hash = $("#hash").val();
            $("#loader-wrapper").fadeIn("slow");
            $.ajax({
                data:  {id_tipo_oper: id_tipo_oper, id_tipo_prop : id_tipo_prop, codigo : codigo, hash : hash},
                url:   'vendor/class/propiedad/propiedades_activas.php',
                type:  'post',
                //dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                    //console.log(data);
                    $('#home').html("");
                    $('#home').html(data);
                    $("#loader-wrapper").fadeOut("slow");
                    //desasignar_eventos_panel();
                    if(busquedas == 1)
                        asignar_eventos_panel();
                },
                error: function(data){
                    console.log(data);
                    $("#loader-wrapper").fadeOut("slow");
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/

        }

        function asignar_eventos_panel(){
            //console.log("asignando eventos");
            $('#home').on('click','a.shared', function() {
                url = $(this).attr("data-href");
                //console.log($(this).attr("data-href"));
                compartir(url);
            })

            $('#home').on('click','.button_on', function() {
                estado="";
                idrow = $(this).attr("cod");
                idelement = this.id;
                console.log("id asignado: " + idelement);
                estatus(idrow,idelement);
            });

            $('#home').on('click','#erase', function() {
                eliminar_estatus(codigo, 3);
            });/**/
        }

        $('#home').on('click','a.page-link',function(){

            id_tipo_oper = $("#tipo_ope").val();
            id_tipo_prop = $("#tipo_prop").val();
            codigo = "";

            offset = parseInt($(this).attr("offset"));
            limit = $("#limit").val();
            
            cantidad = parseInt($("#cantidad").html());

            start = (offset*limit);

            $("#loader-wrapper").fadeIn("slow");
            //offset = ($j*$limit);

                //console.log(limit);
                //console.log(cantidad);
               
                $("li[class='page-item active']").each(function() {
                    //console.log(" label: "+$(this).html());
                    $(this).removeClass("active");
                });

                $(this).parent().prev().removeClass("active");
                $(this).parent().addClass("active");
                $(this).parent().next().removeClass("active");

                if(start == 0)
                    $("#inicio").html(1);
                else
                    $("#inicio").html(start);


                fin = parseInt(start)+parseInt(limit)

                if(fin > cantidad)
                    $("#fin").html(cantidad);
                else
                    $("#fin").html(fin);
                //console.log($(this).html());

            busquedas++;
            hash = $("#hash").val();
            $("#loader-wrapper").fadeIn("slow");
            $.ajax({
                data:  {id_tipo_oper: id_tipo_oper, id_tipo_prop : id_tipo_prop, codigo : codigo, hash : hash, start : start, limit : limit},
                url:   'vendor/class/propiedad/propiedades_activas_paginado.php',
                type:  'post',
                //dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                    console.log(data);
                    $('#activas').html("");
                    $('#activas').html(data);
                    $("#loader-wrapper").fadeOut("slow");
                    //desasignar_eventos_panel();
                    if(busquedas == 1)
                        asignar_eventos_panel();
                },
                error: function(data){
                    //console.log(data);
                    $("#loader-wrapper").fadeOut("slow");
                   // window.location.href="cuenta.php?success=no";
                }
            });/**/

        });

    </script>

  </body>

</html>
