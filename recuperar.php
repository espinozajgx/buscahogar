<?php
require_once('vendor/bin/connection.php');
require_once("vendor/class/usuario/usuarios_data.php");
require_once("vendor/class/utilidades.php");

$bd = connection::getInstance()->getDb();
$user  = "";

    /* RECUERDAME DE INDEX */
    //if(isset($_COOKIE["recuerdame"]) && !empty($_COOKIE["recuerdame"])){
        session_start();

        if(isset($_SESSION["hash512"])){
            $user  = $_SESSION["nombre"];
        }
    //}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php Utilidades::obtener_meta($bd); ?>">
  <title>Recuperar Password - BuscaHogar</title>  
  <?php include_once("vendor/includes/metas.php");  ?>

</head>

<body>
  <!--header-->
  <?php include_once("vendor/includes/header.php");  ?>

    <div class="container ">
        <div class="row my-5">
            <div class="col-lg-3 col-my-5">
            </div>

            <div class="col-lg-6 col-md-10 col-sm-12 my-5 mb-2">
                <div class="card card-login mx-auto mb-5">
                  <div class="card-header">Recuperar Password</div>
                  <div class="card-body">
                    <div class="text-center mt-4 mb-5">
                      <h4>Olvidaste tu clave?</h4>
                      <p>Ingresa tu direccion de email y te enviaremos las instrucciones para recuperar tu Password.</p>
                    </div>
                    <form>
                      <div class="form-group">
                        <input class="form-control" id="email" type="email" aria-describedby="emailHelp" placeholder="Ingresa tu email">
                        <div id="error_email" class="text-danger" style="display:none">
                            <i class="fa fa-exclamation"></i><small> Ingresa tu email</small>
                        </div>
                      </div>
                      <button id="reset" class="btn btn-info btn-block my-2" href="#">Recuperar Password</button>
                        <div id="error_msg" class="text-danger" style="display:none">
                            <i class="fa fa-exclamation"></i><small> Ingresa tu email</small>
                        </div>
                        <div id="ok_msg" class="text-success" style="display:none">
                            <i class="fa fa-check"></i><small> Ingresa tu email</small>
                        </div>
                    </form>
                    <div class="text-center">
                      <a class="d-block small mt-3" href="cuenta">Crea tu cuenta</a>
                      <a class="d-block small" href="ingresar">Ingresar</a>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

  <!-- Footer -->
  <?php include_once("vendor/includes/footer.php");  ?>

    <!-- Bootstrap core JavaScript -->
    <?php include_once("vendor/includes/jsreferences.php");  ?>
    <script src="js/utilidades.js"></script>

    <script type="text/javascript">
        var error = false;

        $(".form-control").on("keyup",function(e){
            var id=$(this).attr("id");

            if(id != null)
                if(id=="email"){
                    $("#"+id).removeClass('is-invalid').addClass('is-valid'); 
                    ocultar_err_msg("#error_"+id); 
                }
        });

        $('#reset').click(function(e){
            e.preventDefault();
            if(!validar_inputs("#email", "#error_email")){

                email = $("#email").val();

                $.ajax({
                    data:  {accion: 5, email : email},
                    url:   'vendor/class/usuario/usuario_acciones.php',
                    type:  'post',
                    dataType: "json",
                    success:  function (data) {
                        //respuesta = JSON.stringify(data);
                        //console.log(data);

                        if(data.estado == 0){
                            $("#ok_msg").hide();
                            $("#error_msg").show();
                            $("#error_msg").find("small").html(" " + data.mensaje);
                        }
                        else{
                            $("#error_msg").hide();
                            $("#email").val("");
                            $("#ok_msg").find("small").html(" " + data.mensaje);
                            $("#ok_msg").show();
                            //window.location.href="panel.php";
                        }/**/
                    },
                    error: function(data){
                        console.log(data);
                        //window.location.href="cuenta.php?success=no";
                    }
                });/**/
            }


        });
    </script>
</body>

</html>
