<?php
require_once('vendor/bin/connection.php');
require_once("vendor/class/usuario/usuarios_data.php");
require_once("vendor/class/propiedad/propiedad_data.php");
require_once("vendor/class/utilidades.php");

$bd = connection::getInstance()->getDb();
$user  = "";
$codigo  = "";
$hash  = "";
$titulo = ""; 
$precio = "";


    /* RECUERDAME DE INDEX */
    //if(isset($_COOKIE["recuerdame"]) && !empty($_COOKIE["recuerdame"])){
        session_start();

        if(isset($_SESSION["hash512"])){
            $user  = $_SESSION["nombre"];
            $hash  = $_SESSION["hash512"];
        }

        if(isset($_GET["cod"])){

            $codigo  = $_GET["cod"];
            $foto = Prop::obtener_primera_foto_prop($bd, $codigo);


            if(Prop::obtener_id($bd, $codigo)){
                $hash_usuario = Prop::obtener_hash_usuario($bd, $codigo);

                $id_tipo_usuario = Usuarios::obtener_id_tipo_usuario($bd, $hash_usuario);
                $logo = Usuarios::obtener_logo_path($bd, $hash_usuario);
                $telefono = Usuarios::obtener_telefonos($bd, $hash_usuario);
                
                if($telefono == "")
                    $telefono = "No posee numero telefonico";

                if($id_tipo_usuario == 1){
                    $usuario = Usuarios::obtener_nombre($bd, $hash_usuario);
                    $usuario .= " ". Usuarios::obtener_apellido($bd, $hash_usuario);
                    $tipo_usuario = "Particular";

                }
                else{
                    $usuario = Usuarios::obtener_nombre_inmobiliaria($bd, $hash_usuario);
                    $tipo_usuario = "Inmobiliaria";
                }

                $bathroom = Prop::obtener_bathroom($bd,$codigo);
                if($bathroom == "")
                    $bathroom = 0;

                $elevator = Prop::obtener_elevator($bd,$codigo);
                if($elevator == "")
                    $elevator = 0;

                $keywords = Prop::obtener_keywords_prop($bd, $codigo);

                $id_tipo_oper = Prop::obtener_id_tipo_oper($bd, $codigo);

                $tipo_oper = Prop::obtener_tipo_oper($bd, $codigo);
                $tipo_prop = Prop::obtener_tipo_prop($bd, $codigo);

                $id_moneda = Prop::obtener_id_moneda($bd, $codigo);
                $id_tipo_superficie = Prop::obtener_id_tipo_superficie($bd, $codigo);

                $tipo_moneda = Prop::obtener_tipo_moneda_by_id($bd, $id_moneda);
                $tipo_superficie = Prop::obtener_tipo_superficie_by_id($bd, $id_tipo_superficie);


                $precio_unitario = Prop::obtener_precio($bd, $codigo);
                if($precio_unitario != ""){
                    $precio = number_format(doubleval($precio_unitario),"0",",",".");

                    $precio_final =  $tipo_moneda ." ". $precio;
                }

                $estado_inmueble = Prop::obtener_estado_inmueble($bd, $codigo);
                $estado_prop = Prop::obtener_estado_propiedad($bd, $codigo);

                $id_barrio = Prop::obtener_id_barrio($bd, $codigo);

                $barrio = Prop::obtener_barrio($bd, $codigo);
                $calle =  Prop::obtener_calle($bd, $codigo);
                $altura = Prop::obtener_altura($bd, $codigo);

                $direccion = $barrio . ", " . $calle . " " . $altura;
                $titulo = $tipo_oper . " de " . $tipo_prop . " en " . $direccion; 

                $fav = Prop::verificar_favoritos_prop($bd, $codigo, $hash);
                //echo $fav;
                if($fav){
                    $estilo_fav = "success";
                }
                else{
                    $estilo_fav = "info";
                }

                   /* if(isset($_GET["act"])){
                        $accion = $_GET["act"];
                    }    

                    if($accion == 3){
                        $codigo_nuevo=Data::generateRandomLettersMay(3);
                        $codigo_nuevo.=Data::generateRandomNum(4);
                        $_SESSION["codigo"] = $codigo_nuevo;
                        $titulo = "Nueva Propiedad #" .$codigo_nuevo;
                    }
                    else{
                        $_SESSION["codigo"] = $codigo;
                        $titulo = "Editar Propiedad #" .$codigo;
                    }/**/
            }    
            else{
                header("Location:404.php");
            }
        }
        else{
            header("Location:404.php");
        }
    //}
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php Utilidades::obtener_meta($bd); ?>">
    <title><?php echo $titulo ?> - BuscaHogar</title> 
     
    <meta property="og:title" content="<?php echo $titulo?>"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.buscahogar.com.ar/propiedad-<?php echo $codigo?>" />
    <meta property="og:description"   content="<?php Utilidades::obtener_meta($bd); ?>" />
    <meta property="og:image" content="http://www.buscahogar.com.ar/prop/<?php echo $codigo?>/<?php echo $foto?>" />
    <meta property="fb:app_id" content="379588499214074" />

    <?php include_once("vendor/includes/metas.php");  ?>
    <link href="vendor/plugin/lightbox/css/lightbox.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
        .imgOtros{
            max-width: 100%;
            height: 150px;
        }

        .img-fluid {
            max-width: 100%;
            height: 100%;
        }

        .img-fluid-1 {
            max-width: 100%;
            height: auto;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120003817-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-120003817-1');
    </script>

  </head>

  <body>
    <div id="loader-wrapper" class="loader-wrapper">
        <div id="loader" class="loader"></div>
    </div>
    <!-- Navigation -->
    <?php include_once("vendor/includes/header.php");  ?>

    <!-- Page Content -->
    <div class="container pt-5" style="padding-left: 2em;padding-right: 2em;">
        <input type="hidden" id="direccion" value="<?php echo $direccion ?>">
        <input type="hidden" id="codigo" value="<?php echo $codigo ?>">
        <input type="hidden" id="hash" value="<?php echo $hash ?>">

        <input type="hidden" id="hash_usuario" value="<?php echo $hash_usuario ?>">
        <input type="hidden" id="usuario" value="<?php echo $usuario ?>">
        <input type="hidden" id="titulo" value="<?php echo $titulo ?>">

      <!-- Page Heading/Breadcrumbs -->
    <div class="row mt-2 mb-3">
        <div class="col-12 col-sm-6 col-md-7 col-lg-7 mt-2">
            <h1 class="mb-3 h1">
                <small><?php echo $titulo ?></small>
            </h1>
        </div>
        <div class="col-12 col-sm-6 col-md-5 col-lg-5 text-center ">
            <div class="item-price text-info">
                <span ><?php echo $precio_final  ?></span>
            </div>
            
        </div>
    </div>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="categoria.php?cat=<?php echo $id_tipo_oper?>"><i class="fa fa-arrow-left fa-bg"></i> Volver <?php// echo $tipo_oper?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo $titulo . " - Cod: " . $codigo ?></li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-sm-12 col-md-8 col-lg-8">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                 <?php echo Prop::obtener_fotos_prop_slider($bd, $codigo); ?>

              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
              </a>
            </div>
          

            <div class="col-md-12 text-right pt-3">
                <div class="btn-group">
                    <button id="contacbtn" type="button" class="btn btn-info btn-sm mr-1 mt-1" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="bottom" data-content="<?php echo $telefono ?>" >Contactar <i class="fa fa-phone"></i></button>
                </div>

                <div class="btn-group">
                    <button id="shared" type="button" class="btn btn-info btn-sm mr-1 mt-1" title="Compartir">Compartir <i class="fa fa-share-alt"></i></button>
                </div>

                <div class="btn-group">
                    <button id="fav" cod="code" type="button" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Agregado a favoritos" class="btn btn-<?php echo $estilo_fav ?> btn-sm mr-1 mt-1" >Favoritos <i class="fa fa-star"></i></button>
                </div>

                <div class="btn-group">
                    <button type="button" id="mensaje" class="btn btn-info btn-sm mr-1 mt-1" title="Contactar">Dejar Mensaje <i class="fa fa-envelope"></i></button>
                </div>

            </div>
            <hr>
        </div>

        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="row">
                <div class="col-sm-12 col-md-12 text-center">
                    <div class="col-md-12 palabras pt-2 pb-2 bg-info rounded ">
                        <span class="text-white "><?php echo $keywords; ?></span>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 text-center pt-1">
                    <?php if($logo != ""){?>
                    <img src="img/users/<?php echo $logo ?>" class="imgLogo1">
                    <?php }?>
                </div>
                 <div class="col-sm-12 col-md-12 col-lg-12 pb-4 text-center">
                    <div class="">
                        <hr>
                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-between">
                              <strong class="mb-1"><?php echo $usuario ?></strong>
                              <small class="text-muted"> <?php echo $tipo_usuario ?></small>
                            </div>
                            <!--div class="col-sm-12 col-md-12 col-lg-12 text-center ">
                                <img class="img-fluid rounded" src="img/500x300.png" alt=""> 
                            </div-->
                          </a>
                        <ul class="list-group">
                          <li class="list-group-item d-flex justify-content-between align-items-center">
                            Baños
                            <span class="badge badge-info"><?php echo $bathroom; ?></span>
                          </li>
                          <li class="list-group-item d-flex justify-content-between align-items-center">
                            Ascensor
                            <span class="badge badge-info"><?php echo $elevator ?></span>
                          </li>
                          <li class="list-group-item d-flex justify-content-between align-items-center">
                            Superficie
                            <span class="badge badge-info"><?php echo Prop::obtener_superficie($bd, $codigo) . $tipo_superficie ?></span>
                          </li>
                          <li class="list-group-item d-flex justify-content-between align-items-center">
                            Ambientes
                            <span class="badge badge-info"><?php echo Prop::obtener_ambiente($bd, $codigo);?></span>
                          </li>
                          <li class="list-group-item d-flex justify-content-between align-items-center">
                            Antiguedad
                            <span class="badge badge-info"><?php echo Prop::obtener_antiguedad($bd, $codigo); ?></span>
                          </li>
                          <li class="list-group-item d-flex justify-content-between align-items-center">
                            Inmueble
                            <span class="badge badge-info"><?php echo Prop::obtener_estado_inmueble($bd, $codigo);?></span>
                          </li>
                          <li class="list-group-item d-flex justify-content-between align-items-center">
                            Edificio
                            <span class="badge badge-info"><?php echo Prop::obtener_estado_propiedad($bd, $codigo);?></span>
                          </li>
                        </ul>
                    </div>
                   
                </div>

                
            </div>
        </div>





      </div>
      <!-- /.row -->

       <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-12 col-lg-12 prop mt-2 pl-4 pr-4">
         <p class="h5 pb-3 text-info">DETALLES</p>
            <!-- Post Content -->
            <div class="btn-toolbar text-center" role="toolbar" aria-label="Toolbar with button groups">
                <?php Prop::obtener_caracteristicas_prop($bd, $codigo) ?>
            </div>
          <hr>
        </div>

        <div class="col-md-12 col-lg-12 prop mt-2 pl-4 pr-4">
         <p class="h5 pb-3 text-info">ADICIONALES</p>
            <!-- Post Content -->
            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                <?php Prop::obtener_adicionales_prop($bd, $codigo) ?>
                <!--div class="btn-group mr-2 mt-2" role="group">
                    <button type="button" class="btn btn-light">Frente</button>
                </div>
                <div class="btn-group mr-2 mt-2" role="group">
                    <button type="button" class="btn btn-light">Seguridad 24hs</button>
                </div-->
            </div>
          <hr>
        </div>

        <div class="col-md-12 col-lg-12 prop mt-4 pl-4 pr-4">
          <p class="h5 pb-3 text-info">DESCRIPCIÓN</p>

          <!-- Post Content -->
          <p class="lead text-justify " >
           <?php echo Prop::obtener_descripcion($bd, $codigo) ?>
          </p>

          <hr>
        </div>

        <div class="col-md-12 col-sm-12 col-lg-12 pl-4 pr-4 mt-4 mb-4">
            <p class="h5 pb-3 text-info">UBICACIÓN<p class="h5">
            <div id="map_canvas" class="rounded mapa_propiedad "></div>
           
        </div><!-- MAPA -->
         <hr>
      </div>
      <!-- /.row -->

      

    </div>
    <!-- /.container -->
    <!-- Related Projects Row -->
    <?php include_once("vendor/includes/widget_propiedades.php");  ?>
      <!-- /.row -->

    <!-- Modal -->
    <?php include_once("vendor/includes/modal_contacto.php");  ?>
    <?php include_once("vendor/includes/modal_mensaje_enviado.php");  ?>


    <!-- Footer -->
    <?php include_once("vendor/includes/footer.php");  ?>

    <!-- Bootstrap core JavaScript -->
    <?php include_once("vendor/includes/jsreferences.php");  ?>
    <script src="vendor/plugin/lightbox/js/lightbox.js"></script>
    <script type="text/javascript" src="js/maps.js?v=1"></script>
    <script src="js/utilidades.js"></script>
    <script type="text/javascript" src="js/facebook.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANQhLIp0yHg0vWvYScNHOvpAw8NDgfA2g&callback=initialize" async defer></script><!---->
    <script type="text/javascript">
        var error = false;
        var titulo;
        var usuario;
        var hash;
        var codigo;

        $(document).ready(function(){
            $("#loader-wrapper").fadeOut("slow");


        
            $("#shared").click(function(e){

                //console.log($("#direccion").val());
                url = window.location.href;
                console.log(url);

                FB.ui({
                  method: 'share',
                  href: url
                }, function(response){
                    // Debug response (optional)
                    console.log(response);
                });
                /*FB.ui({
                  method: 'share_open_graph',
                  action_type: 'og.likes',
                  action_properties: JSON.stringify({
                    object:url,
                  })
                }, function(response){
                  // Debug response (optional)
                  console.log(response);
                });/**/
            });

            $("#phone").keypress(function (e) {
                 //if the letter is not digit then display error and don't type anything
                 if (e.which != 8 && e.which != 0 && e.which != 32 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    return false;
                }
            });

            $(function () {
              $('#contacbtn[data-toggle="popover"]').popover()
            })

            $('.popover-dismiss').popover({
              trigger: 'focus'
            })

            lightbox.option({
              'maxWidth': 900,
              'resizeDuration': 200,
              'wrapAround': true
            })

            /*$("#contacbtn").click(function(e){

            });*/

            $(".form-control").on("keyup",function(e){
                var id=$(this).attr("id");

                if(id != null)
                    if(id=="name_consulta" || id=="email_consulta" || id=="tlf_consulta" || id=="txt_consulta"){
                        $("#"+id).removeClass('is-invalid').addClass('is-valid'); 

                        ocultar_err_msg("#error_"+id); 
                    }
            });



            $('#fav').click(function(e){
                e.preventDefault();

                codigo = $("#codigo").val();
                hash = $("#hash").val();
                console.log($("#hash").val());

                if(hash != ""){
                    if($(this).hasClass('btn-success')){
                        $(this).removeClass('btn-success').addClass('btn-info'); 
                        $(this).attr("data-content","Eliminado de favoritos");
                        $(this).popover("show");

                        eliminar_fav(codigo, hash);
                    }
                    else{
                        $(this).removeClass('btn-info').addClass('btn-success'); 
                        $(this).attr("data-content","Agregado a favoritos");
                        $(this).popover("show");

                        agregar_fav(codigo, hash);
                    }
                }
                else{
                    $(this).attr("data-content","Inicia sesion para agregar a favoritos");
                     $(this).popover("show");
                }
                

            });


            function agregar_fav(codigo, hash){
                $.ajax({
                    data:  {accion: 6, codigo : codigo, hash : hash, estatus: 1},
                    url:   'vendor/class/propiedad/propiedad_acciones.php',
                    type:  'post',
                    dataType: "json",
                    success:  function (data) {
                        //respuesta = JSON.stringify(data);
                        //console.log(data);

                        if(data.estado == 0){
                            //$("#alert_wrong").show();
                        }
                        else{
                            //$("#alert_ok").show();
                            //window.location.href="perfil.php";
                        }
                    },
                    error: function(data){
                        console.log(data);
                       // window.location.href="cuenta.php?success=no";
                    }
                });/**/
            }

            function eliminar_fav(codigo, hash){
                $.ajax({
                    data:  {accion: 7,codigo : codigo, hash : hash, estatus: 1},
                    url:   'vendor/class/propiedad/propiedad_acciones.php',
                    type:  'post',
                    dataType: "json",
                    success:  function (data) {
                        //respuesta = JSON.stringify(data);
                        //console.log(data);

                        if(data.estado == 0){
                            //$("#alert_wrong").show();
                        }
                        else{
                            //$("#alert_ok").show();
                            //window.location.href="perfil.php";
                        }
                    },
                    error: function(data){
                        console.log(data);
                       // window.location.href="cuenta.php?success=no";
                    }
                });/**/
            }

            $("#mensaje").click(function(e){

                titulo = $("#titulo").val();
                hash = $("#hash_usuario").val();
                usuario = $("#usuario").val();
                codigo = $("#codigo").val();

                //console.log(codigo);
                $('#contact').find(".modal-title").html("<small><strong>" +titulo + " - " + codigo+"</strong></small>");

                //console.log(hash);
                //console.log(usuario);

                mensaje = "Hola " + usuario + ", estoy interesado en su anuncio. Contácteme! Gracias.";
                $('#contact').find("#txt_consulta").html(mensaje);

                $('#contact').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $("#tlf_consulta").on("keypress",function(e){
                if (e.which != 8 && e.which != 0 && e.which != 32 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                $("#tlf_consulta").removeClass('is-invalid').addClass('is-valid'); 
                ocultar_err_msg("#error_tlf_consulta"); 
            });/**/

            $('#send').click(function(e){
                e.preventDefault();

                error = false;

                validar_inputs("#name_consulta", "#error_name_consulta");
                validar_inputs("#email_consulta", "#error_email_consulta");
                validar_inputs("#tlf_consulta", "#error_tlf_consulta");
                validar_inputs("#txt_consulta", "#error_txt_consulta");

                if(!error){

                    nombre = $("#name_consulta").val();
                    tlf = $("#tlf_consulta").val();
                    email = $("#email_consulta").val();
                    msj = $("#txt_consulta").val();

                    /*titulo = $("#titulo").val();
                    hash = $("#hash_usuario").val();
                    usuario = $("#usuario").val();
                    codigo = $("#codigo").val();*/
                    //console.log(msj);

                    $.ajax({
                        data:  {accion: 0, nombre : nombre, tlf : tlf, email: email, msj : msj, usuario : usuario, hash : hash, titulo : titulo, codigo : codigo},
                       // url:   'vendor/mail/contacto_acciones.php',
                        url: 'vendor/class/soporte/soporte_acciones.php',
                        type:  'post',
                        dataType: "json",
                        success:  function (data) {
                            //respuesta = JSON.stringify(data);
                            console.log(data);
                            $('#contact').modal('hide');
                            
                            $("#name_consulta").val("");
                            $("#tlf_consulta").val("");
                            $("#email_consulta").val("");
                            $("#txt_consulta").val("");

                            $('#modal_send').modal();
                            /*if(data.estado == 0){
                                //$("#alert_wrong").show();
                            }
                            else{
                                //$("#alert_ok").show();
                                //window.location.href="perfil.php";
                            }*/
                        },
                        error: function(data){
                           
                            //console.log(data);
                            $('#contact').modal('hide');
                            $("#name_consulta").val("");
                            $("#tlf_consulta").val("");
                            $("#email_consulta").val("");
                            $('#modal_send').modal();
                            //$("#txt_consulta").val("");
                           // window.location.href="cuenta.php?success=no";
                        }
                    });/**/
                }

            });

        });
    </script>

  </body>

</html>
