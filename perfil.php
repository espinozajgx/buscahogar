<?php
require_once('vendor/bin/connection.php');
require_once("vendor/class/usuario/usuarios_data.php");
require_once("vendor/class/utilidades.php");
/* RECUERDAME DE INDEX */

$user  = "";
$foto = "";

    session_start();
    if(isset($_SESSION["hash512"])){
        $bd = connection::getInstance()->getDb();
        $user  = $_SESSION["nombre"];

        $hash = $_SESSION["hash512"];

        $id_tipo_usuario = Usuarios::obtener_tipo($bd,$hash);

        $foto = Usuarios::obtener_logo_path($bd,$hash);


        $estilo_par = "";
        $estilo_mob = "";

        if($id_tipo_usuario == 1){
            $estilo_mob = "display:none";
            //$user = Usuarios::obtener_nombre($bd, $row["hash"]);
        }
        else{
            $estilo_par = "display:none";
            //$user = Usuarios::obtener_nombre_inmobiliaria($bd, $$row["hash"]);
        }
    }else{
        header("Location:ingresar.php");
    }

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="favoritos  inmuebles, hogar, venta, alquiler, temporal, simple, facil, accesible, pruebalo gratis, mas filtros de busqueda">
    <title>Mis Favoritos - BuscaHogar</title>  
    <?php include_once("vendor/includes/metas.php");  ?>
    
    <style type="text/css">
        .fotoPerfil{
            margin: 1em;
            min-height: 100px;
            max-height: 100%;
           /*max-width: 228px;/**/
            background-color: #eaedef;
            border: 3px dashed #d8d8d8;
            padding: 1em;
        }

        .img_perfil{
            min-height: 100px;
            max-height: 110px;
        }


    </style>
  </head>

  <body>
  <div id="loader-wrapper" class="loader-wrapper">
    <div id="loader" class="loader"></div>
  </div>
    <!-- Navigation -->
    <?php include_once("vendor/includes/header.php");  ?>


    <!-- Page Content -->
    <div class="container mt-5">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Mi Cuenta
        <!--small>Subheading</small-->
      </h1>

        <?php 
            Utilidades::obtener_menu(3);
        ?>
        <hr>

        <div class="row mb-5">
            <input type="hidden" class="form-control" id="hash" value="<?php echo $hash ?>" >
            <input type="hidden" class="form-control" id="id_tipo_usuario" value="<?php echo $id_tipo_usuario ?>">
            <div class="col-md-12">  
            <form>
                <div class="row">
                    <div class="col-sm-12 col-md-4 my-3" style="<?php echo $estilo_mob ?>">
                        <div class="col-sm-12 col-md-12 my-3"> 
                            <div class="custom-file ">
                                <form method="post" id="formFoto" enctype="multipart/form-data">
                                    <label class="custom-file-label" for="fileToUpload">Selecciona</label>
                                    <input type="file" name="fileToUpload" id="fileToUpload" accept=".jpg,.png,.jpeg" lang="es" class="custom-file-input">
                                    <label class="custom-file-label" for="fileToUpload">Selecciona</label>
                                </form>
                            </div>
                        </div>

                        <div class="fotoPerfil rounded text-center pull-center">
                            <?php if($foto == ""){ ?>
                                <strong class="text-info" ><p>Carga tu logo <?php echo $foto ?></p></strong>
                                <input id="fp" type="hidden" value="<?php echo $foto ?>"></input>
                            <?php }else
                            { ?>
                                <img class="img_perfil" src="img/users/<?php echo $foto ?>">
                                <input id="fp" type="hidden" value="<?php echo $foto ?>"></input>

                            <?php }?>    
                        </div>
                    </div>  

                    <?php if($id_tipo_usuario == 1){ ?>
                        <div class="col-sm-12 col-md-4 my-3">  
                        </div> 
                    <?php
                    } 
                    ?>


                    <div class="col-sm-12 col-md-8 my-3"> 

                        <div id="div_par" class="form-row" style="<?php echo $estilo_par ?>">
                            <div class="form-group col-6 col-sm-12 col-md-12">
                                <small><strong><label for="doc">Identificacion</label></strong></small>
                                    <div class="input-group">
                                        <input id="doc" type="text" class="form-control" value="<?php  echo Usuarios::obtener_identificacion($bd,$hash) ?>">

                                        <div class="input-group-append col-6">
                                            <?php  $tipo_doc = Usuarios::obtener_tipo_doc($bd,$hash) ?>
                                            <select class="custom-select" id="doc_sel">
                                                <option value="1" <?php if($tipo_doc==1) echo "selected"; ?>>Documento Único</option>
                                                <option value="2" <?php if($tipo_doc==2) echo "selected"; ?>>CUIT</option>
                                                <option value="3" <?php if($tipo_doc==3) echo "selected"; ?>>Libreta de Enrolamiento</option>
                                                <option value="4" <?php if($tipo_doc==4) echo "selected"; ?>>Libreta cívica</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                    </div>
                                    <div id="error_doc" class="text-danger" style="display:none">
                                        <i class="fa fa-exclamation"></i><small> Campo Obligatorio</small>
                                    </div>
                            </div>

                            <div class="form-group col-6 col-sm-6 col-md-6">
                                <small><strong><label for="name">Nombre</label></strong></small>
                                <input type="text" class="form-control" id="name" placeholder="Nombre" value="<?php  echo Usuarios::obtener_nombre($bd,$hash) ?>" autocomplete="off">
                                <div id="error_name" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Ingresa tu nombre</small>
                                </div>
                            </div>
                            <div class="form-group col-6 col-sm-6 col-md-6">
                                <small><strong><label for="last_name">Apellido</label></strong></small>
                                <input type="text" class="form-control" id="last_name" placeholder="Apellido" value="<?php echo Usuarios::obtener_apellido($bd,$hash); ?>" autocomplete="off">
                                <div id="error_last_name" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Ingresa tu apellido</small>
                                </div>
                            </div>
                        </div>

                        <div id="div_mobil" class="form-row" style="<?php echo $estilo_mob ?>">
                            <div class="form-group col-6 col-sm-12 col-md-12">
                                <small><strong><label for="inmobiliaria">Nombre de la inmobiliaria</label></strong></small>
                                <input type="text" class="form-control" id="inmobiliaria" placeholder="inmobiliaria" value="<?php echo Usuarios::obtener_nombre_inmobiliaria($bd,$hash); ?>" autocomplete="off">
                                <div id="error_inmobiliaria" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Ingresa el nombre de la inmobiliaria</small>
                                </div>
                            </div>
                            <div class="form-group col-6 col-sm-6 col-md-6">
                                <small><strong><label for="rs">Razón Social</label></strong></small>
                                <input type="text" class="form-control" id="rs" placeholder="Razón Social" value="<?php echo Usuarios::obtener_rs($bd,$hash); ?>" autocomplete="off">
                                <div id="error_rs" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Ingresa la razon social</small>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <small><strong><label for="cuit">CUIT</label></strong></small>
                                <input type="text" class="form-control" id="cuit" placeholder="CUIT" value="<?php echo Usuarios::obtener_cuit($bd,$hash); ?>" autocomplete="off">
                                <div id="error_cuit" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Ingresa el CUIT</small>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">    
                            <div class="form-group col-6 col-md-6">
                                <small><strong><label for="email">Email</label></strong></small>
                                <input type="email" class="form-control" id="email" placeholder="Email" value="<?php echo Usuarios::obtener_email($bd,$hash); ?>" autocomplete="off">
                                <div id="error_email" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Ingresa tu email</small>
                                </div>
                                <hr>
                            </div>

                            <div class="form-group col-4 col-md-4">
                                <small><strong><label for="change_btn">Password</label></strong></small>
                                <a id="change_btn" class="form-control btn btn-info text-white" data-toggle="modal" data-target="#modal_pass">Cambiar</a>
                            </div>


                            <div class="form-group col-6 col-sm-6 col-md-6">
                                <small><strong><label for="phone">Telefonos</label></strong></small>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+54</span>
                                  </div>
                                  <input id="phone" type="text" class="form-control" placeholder="" value="<?php echo Usuarios::obtener_telefonos($bd,$hash); ?>" aria-describedby="basic-addon1">
                                </div>

                                <div id="error_phone" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Campo Obligatorio</small>
                                </div>
                            </div>

                            <div class="form-group col-6 col-sm-6 col-md-6">

                                <?php $cond_iva = Usuarios::obtener_cond_iva($bd,$hash); ?>
                                <small><strong><label for="iva">Condición frente al IVA</label></strong></small>
                                    <select class="custom-select" id="iva_sel">
                                        <option value="1" <?php if($cond_iva==1) echo "selected"; ?>>Responsable Inscrito</option>
                                        <option value="2" <?php if($cond_iva==2) echo "selected"; ?>>Responsable monotributo</option>
                                        <option value="3" <?php if($cond_iva==3) echo "selected"; ?>>Exento</option>
                                        <option value="4" <?php if($cond_iva==4) echo "selected"; ?>>Consumidor final</option>
                                    </select>
                                    <div id="error_iva" class="text-danger" style="display:none">
                                        <i class="fa fa-exclamation"></i><small> Campo Obligatorio</small>
                                    </div>
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-12">
                                <small><strong><label for="direccion">Direccion</label></strong></small>
                                <textarea row="3" class="form-control" id="direccion"><?php echo Usuarios::obtener_direccion($bd,$hash); ?></textarea>
                                <div id="error_direccion" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Campo Obligatorio</small>
                                </div>

                            </div>

                            <div class="form-group col-6 col-sm-6 col-md-6">
                                <small><strong><label for="localidad">Localidad</label></strong></small>
                                <input type="text" class="form-control" id="localidad" placeholder="Localidad" value="<?php echo Usuarios::obtener_localidad($bd,$hash); ?>" autocomplete="off">
                                <div id="error_localidad" class="text-danger" style="display:none">
                                    <i class="fa fa-exclamation"></i><small> Ingresa la localidad</small>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                
                <div id="alert_wrong" class="alert alert-warning alert-dismissible" role="alert" style="display:none">
                  <strong><i class="fa fa-thumbs-down"></i> Su perfil no fue actualizado!</strong> verifique los datos e intente nuevamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <!--span aria-hidden="true">&times;</span-->
                  </button>
                </div>

                <div id="alert_ok" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                  <strong><i class="fa fa-thumbs-up"></i> Perfil Actualizado!</strong>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <!--span aria-hidden="true">&times;</span-->
                  </button>
                </div>
                <hr>
                <div class="col-md-12 col-sm-12 col-xs-12 py-2 margin-bottom-20 pull-right text-right ">
                    <button type="button" id="btnguardar" class="btn btn-info btn-cons">Modificar</button>
                </div>

            </form>



            </div>

        </div>



    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("vendor/includes/footer.php");  ?>

    <div id="modal_pass" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Cambiar Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="col-md-12">

                  <!--div class="col-md-12 mb-2">
                    <label for="pass_current">Password Actual</label>
                    <input class="form-control form-control-sm" id="pass_current" type="password" placeholder="Password" value="">
                    <div id="error_current" class="text-danger" style="display:none">
                        <i class="fa fa-exclamation"></i><small> Ingresa tu password</small>
                    </div>
                  </div-->

                  <div class="col-md-12 mb-2">
                    <label for="pass">Password Nuevo</label>
                    <input class="form-control form-control-sm" id="pass" type="password" placeholder="Password" value="">
                    <div id="error_pass" class="text-danger" style="display:none">
                        <i class="fa fa-exclamation"></i><small> Ingresa tu password</small>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <label for="re_pass">Confirmar Password</label>
                    <input class="form-control form-control-sm" id="re_pass" type="password" placeholder="Confirmar Password" value="">
                    <div id="error_re_pass" class="text-danger" style="display:none">
                        <i class="fa fa-exclamation"></i><small>Contraseñas no coinciden</small>
                    </div>
                  </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button id="cambiar_pass" type="button" class="btn btn-info" disabled>Cambiar</button>
          </div>
        </div>
      </div>
    </div>




    <!-- Bootstrap core JavaScript -->
    <?php include_once("vendor/includes/jsreferences.php");  ?>
    <script src="js/utilidades.js"></script>

    <script type="text/javascript">
        var storedFiles = [];  
        var error = false; 
        var data = new FormData();
        //$('.dropzone').html5imageupload();
        /*$('.html5imageupload').html5imageupload({
            onAfterProcessImage: function() {
                $('#filename').val($(this.element).data('name'));
            },
            onAfterCancel: function() {
                $('#filename').val('');
            }
        });/**/

        $(document).ready(function(){
            $("#loader-wrapper").fadeOut("slow");
        });

        $("input[id='fileToUpload']").on("change", function(){
            var files = this.files;
            var i = 0;

            //if(files.length > 10){
                //$("#error_img").show();
            //}
            //else{
                //var old = $('.cvf_uploaded_files').sortable('toArray', {attribute: 'file'});
                //limit = (old.length-1) + files.length;
                //console.log(old);
                //console.log(limit);

                //if (limit > 10){
                //    $("#error_img").show();
                //}
                //else{
                    //$("#error_img").hide();
                    for (i = 0; i < files.length; i++) {
                        var readImg = new FileReader();
                        var file = files[i];
                        
                        //console.log(file);

                        if (file.type.match('image.*')){
                            storedFiles.push(file);

                            $('.fotoPerfil').html('<div id="'+ file.lastModified+file.size +'" file="'+ file.name +'" '+
                                '<img class = "imgClock" src = "img/clock.png" />'+
                                '</div>'); 

                            readImg.onload = (function(file) {
                                return function(e) {
                                    $('#'+file.lastModified+file.size).append(
                                        '<img class = "imgPhotoItem" src = "' + e.target.result + '" />'
                                        //'<a href ="#" class="cvf_delete_image" title="Eliminar"><img class = "delete-btn" src = "img/delete-btn.png" /></a>'
                                    );
                                    $('#'+file.lastModified+file.size).find(".imgClock").hide();   
                                };
                            })(file);
                            readImg.readAsDataURL(file);
                            //cvf_reload_order();
                        } else {
                            //alert('the file '+ file.name + ' is not an image<br/>');
                        }
                    }
                //}/**/
           // }
        });/**/    


        $("#re_pass").on("keyup",function(e){
            var pass = $("#pass").val().trim();
            var re_pass = $(this).val().trim();
            $("#error_re_pass").hide();

            if(pass != "" && re_pass != ""){
                if(pass == re_pass){
                    $(this).removeClass('is-invalid').addClass('is-valid'); 
                    $("#error_re_pass").hide();
                    $("#cambiar_pass").prop('disabled', true);
                }
                else{
                    $(this).removeClass('is-valid').addClass('is-invalid'); 
                    $("#error_re_pass").show();
                    $("#cambiar_pass").prop('disabled', true);
                    
                }
            }
            else{
                $(this).removeClass('is-valid').addClass('is-invalid'); 
                $("#error_re_pass").show();
                $("#cambiar_pass").prop('disabled', false);
            }
        });


        $(".form-control").on("keyup",function(e){
            var id=$(this).attr("id");

            if(id != null)
                if(id=="name" || id=="last_name" || id=="email" || id=="phone1" || id=="phone" || id=="direccion" || id=="doc" || id=="inmobiliaria" || id=="rs" || id=="cuit" || id=="pass"){
                    $("#"+id).removeClass('is-invalid').addClass('is-valid'); 

                    ocultar_err_msg("#error_"+id); 
                }
        });

        $('#btnguardar').click(function(e){
            e.preventDefault();

            error = false;
            usuario = new Array();

            id_tipo_usuario = $("#id_tipo_usuario").val();

            validar_inputs("#email", "#error_email");
            validar_inputs("#phone", "#error_phone");
            validar_inputs("#direccion", "#error_direccion"); 
            validar_inputs("#iva_sel", "#error_iva_sel"); 
            validar_inputs("#localidad", "#error_localidad"); 

            //console.log(id_tipo_usuario);

            if(id_tipo_usuario == 1){
                validar_inputs("#name", "#error_name");
                validar_inputs("#last_name", "#error_last_name");
                validar_inputs("#doc", "#error_doc");
            }
            else{
                validar_inputs("#inmobiliaria", "#error_inmobiliaria");
                validar_inputs("#rs", "#error_rs");
                validar_inputs("#cuit", "#error_cuit");
            }/**/

            //console.log(error);

            if(!error){

                email = $("#email").val();
                phone = $("#phone").val();
                direccion = $("#direccion").val();
                logo = $("#filename").val();
                hash = $("#hash").val();
                iva_sel = $("#iva_sel").val();
                localidad = $("#localidad").val();

                usuario.push(id_tipo_usuario);

                if(id_tipo_usuario == 1){
                    name = $("#name").val();
                    last_name = $("#last_name").val();
                    doc = $("#doc").val();
                    tipo_doc = $("#doc_sel").val();

                    usuario.push(name);
                    usuario.push(last_name);
                    usuario.push(doc);
                    usuario.push(tipo_doc);
                    
                }
                else{
                    inmobiliaria = $("#inmobiliaria").val();
                    rs = $("#rs").val();
                    cuit = $("#cuit").val();

                    usuario.push(inmobiliaria);
                    usuario.push(rs);
                    usuario.push(cuit);

                }/**/

                $("#loader-wrapper").fadeIn("fast");
                var tam = storedFiles.length;
                if(tam > 0){
                    cargar_logo();
                }
                else{
                    if($("#fp").length){
                        logo = $("#fp").val();
                    }
                    else{
                        logo = "";
                    }
                    //console.log(logo);
                    editar_perfil();
                }

                //console.log(usuario);
            }


        });

        function editar_perfil(){
            $.ajax({
                data:  {accion:2,email : email, phone : phone, direccion : direccion, logo : logo, hash: hash, iva: iva_sel, localidad : localidad,usuario:JSON.stringify(usuario)},
                url:   'vendor/class/usuario/usuario_acciones.php',
                type:  'post',
                dataType: "json",
                success:  function (data) {
                    //respuesta = JSON.stringify(data);
                    //console.log(data);

                    if(data.estado == 0){
                        $("#alert_wrong").show();
                         $("#alert_ok").hide();
                    }
                    else{
                        $("#alert_ok").show();
                        $("#alert_wrong").hide();
                        //window.location.href="perfil.php";
                    }
                    $("#loader-wrapper").fadeOut("fast");
                },
                error: function(data){
                    console.log(data);
                    $("#alert_wrong").show();
                    $("#loader-wrapper").fadeOut("fast");
                   // window.location.href="cuenta.php?success=no";
                }
            });
        }

        function cargar_logo(){
            var ruta = "vendor/class/usuario/simple_upload.php";
            
            data.append('fileToUpload', storedFiles[0]);
            //console.log( storedFiles[0]);

            $.ajax({
                url: ruta,
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                //dataType: "json",
                success: function(datos)
                {
                    //respuesta = JSON.stringify(datos);
                    logo = datos;

                    //console.log("datos:" + datos);
                    editar_perfil();
                    
                    //console.log("datos:" + respuesta.estado);
                    /*if(respuesta.estado == 1){
                       logo = respuesta.res;
                       console.log("aqui");
                       editar_perfil();
                    }*/
                    //console.log("datos:" + respuesta);
                    //$("#divimg").html(datos);
                    //$("#error_img").hide();
                },
                error: function(data){
                    console.log(data);
                    $("#alert_wrong").show();
                    $("#loader-wrapper").fadeOut("fast");
                   // window.location.href="cuenta.php?success=no";
                }
            });
        }

        /*
        $("input[name='file']").on("change", function(){
            var ruta = "assets/class/idiomas/upload.php";
            var f = $(this);
            var formData = new FormData();
            formData.append(f.attr("name"), $(this)[0].files[0]);
            $.ajax({
                url: ruta,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    //console.log("datos:" + datos);
                    $("#divimg").html(datos);
                    $("#error_img").hide();
                }
            });
        });/**/

        $('#cambiar_pass').click(function(e){
            e.preventDefault();


            var error_pass = false;
            //console.log($("#pass_current").val());
            //error_pass = validar_inputs("#pass_current", "#error_curren");
            error_pass = validar_inputs("#pass", "#error_pass");
            error_pass = validar_inputs("#re_pass", "#error_re_pass");

            email = $("#email").val();
            hash = $("#hash").val();
            pass = $("#pass").val();

                console.log(error_pass);
                if(!error_pass){
                    $.ajax({
                        data:  {accion:3,email : email, hash : hash, password : pass},
                        url:   'vendor/class/usuario/usuario_acciones.php',
                        type:  'post',
                        dataType: "json",
                        success:  function (data) {
                            //respuesta = JSON.stringify(data);
                            console.log(data);

                            if(data.estado == 0){
                                $("#alert_wrong").show();
                                 $("#alert_ok").hide();
                            }
                            else{
                                $("#alert_ok").show();
                                $("#alert_wrong").hide();
                                $("#modal_pass").hide();
                                //window.location.href="perfil.php";
                            }
                            $("#loader-wrapper").fadeOut("fast");
                        },
                        error: function(data){
                            console.log(data);
                            $("#alert_wrong").show();
                            $("#loader-wrapper").fadeOut("fast");
                           // window.location.href="cuenta.php?success=no";
                        }
                    });
                }
        });



        $('#extra').click(function(e){
            if($("#extra").is(":checked")){
                //console.log("agree");
                $("#inmobiliaria").prop('disabled', false);
                $("#rs").prop('disabled', false);
                $("#cuit").prop('disabled', false);
            }
            else{
                $("#inmobiliaria").prop('disabled', true);
                $("#rs").prop('disabled', true);
                $("#cuit").prop('disabled', true);
            }
        });
    </script>
  </body>

</html>
