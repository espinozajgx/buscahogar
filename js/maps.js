var map;
var marker;
var addressInput2;

function initMap() {
	var address ="Buenos Aires";
    geocoder = new google.maps.Geocoder();

    latlng = new google.maps.LatLng(-34.607537, -58.404951);

    var myOptions = {
        zoom: 11,
        center: latlng,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP/**/
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    if(geocoder){
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          map.setCenter(results[0].geometry.location);

            var infowindow = new google.maps.InfoWindow(
                { content: '<b>'+address+'</b>',
                  size: new google.maps.Size(150,50)
                });

          } else {
         //   alert("No results found");
          }
        } else {
        //  alert("Geocode was not successful for the following reason: " + status);
        }
      });
    }
}/**/

function initialize() {

    initMap();
    addressInput2 = $("#direccion").val();
    //console.log(addressInput2);

    if(addressInput2 != 1)
      searchAddress(addressInput2);/**/
}

function searchAddress(addressInput) {

  //Buenos Aires, 
  addressInput += ",CABA, Argentina";
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode({address: addressInput}, function(results, status) {

    if (status == google.maps.GeocoderStatus.OK) {

      var myResult = results[0].geometry.location; // reference LatLng value

      createMarker(myResult); // call the function that adds the marker

      map.setCenter(myResult);

      map.setZoom(15);

    }
  });
}

function createMarker(latlng) {

   // If the user makes another search you must clear the marker variable
   if(marker != undefined && marker != ''){
    marker.setMap(null);
    marker = '';
   }

   marker = new google.maps.Marker({
      map: map,
      position: latlng
   });

}