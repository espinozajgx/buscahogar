	function validar_inputs(input, div_error){
	/*console.log(input);
	console.log($(input).val());/**/
	if($(input).val().trim() == ""){
        $(div_error).show();
        error = true;
        mostrar_error(input)
    }
    else{
        $(div_error).hide();
        mostrar_exito(input);
        //error = false;
    }

    return error;
	}

	function mostrar_error(input){
	    $(input).removeClass('is-valid').addClass('is-invalid');  
	}

	function mostrar_exito(input){
	    $(input).removeClass('is-invalid').addClass('is-valid'); 
	}

	function ocultar_err_msg(selector){
		$(selector).hide();
	}

    function validar_email(emailAddress) {
	    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	    return pattern.test(emailAddress);
	}