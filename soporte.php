<?php
require_once('vendor/bin/connection.php');
require_once("vendor/class/usuario/usuarios_data.php");
require_once("vendor/class/utilidades.php");

$bd = connection::getInstance()->getDb();
$user  = "";
		/* RECUERDAME DE INDEX */
		//if(isset($_COOKIE["recuerdame"]) && !empty($_COOKIE["recuerdame"])){
			session_start();

			if(isset($_SESSION["hash512"])){
					$user  = $_SESSION["nombre"];
			}
		//}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<?php Utilidades::obtener_meta($bd); ?>">
		<title>Contacto & Ayuda - BuscaHogar</title>  
		<?php include_once("vendor/includes/metas.php");  ?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120003817-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-120003817-1');
		</script>

	</head>

	<body>
	<!--div id="loader-wrapper" class="loader-wrapper">
		<div id="loader" class="loader"></div>
	</div-->

		<!-- Navigation -->
		<?php include_once("vendor/includes/header.php");  ?>

		<!-- Page Content -->
		<div class="container pt-5">

			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3">Contacto
				<small>&amp; Ayuda</small>
			</h1>

			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="index.html">Inicio</a>
				</li>
				<li class="breadcrumb-item active">Contacto</li>
			</ol>

			<!-- Content Row -->
			<div class="row">
				<!-- Map Column -->
				<div class="col-lg-8 mb-4">
					<!-- Embedded Google Map -->
					<!--iframe width="100%" height="500px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?hl=en&amp;ie=UTF8&amp;ll=37.0625,-95.677068&amp;spn=56.506174,79.013672&amp;t=m&amp;z=4&amp;output=embed"></iframe-->

						<iframe width="100%" height="500px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
							src="https://www.google.com/maps/embed/v1/place?key=AIzaSyANQhLIp0yHg0vWvYScNHOvpAw8NDgfA2g&q=PHOENIX+SOLUTIONS+S.A,+C1037ACC+CABA,+Argentina&attribution_source=Google+Maps+Embed+API&attribution_web_url=http://buscahogar.com.ar/" >
						</iframe>
				</div>
				<!-- Contact Details Column -->
				<div class="col-lg-4 mb-4" >
					<div id="mailer">
						<h3>Envianos un mensaje</h3>
							<form name="sentMessage" id="contactForm">
									<div class="control-group form-group">
										<div class="controls">
											<label>Nombre Completo:</label>
											<input type="text" class="form-control" id="name_consulta" maxlength="30">
											<div id="error_name_consulta" class="text-danger" style="display:none">
													<i class="fa fa-exclamation"></i><small> Ingrese su nombre</small>
											</div>
										</div>
									</div>
									<div class="control-group form-group">
										<div class="controls">
											<label>Numero Telefonico:</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text" id="basic-addon1">+54</span>
												</div>
												<input id="tlf_consulta" type="text" class="form-control" placeholder="" value="" aria-describedby="basic-addon1" autocomplete="off" maxlength="20">
											</div>
											<div id="error_tlf_consulta" class="text-danger" style="display:none">
													<i class="fa fa-exclamation"></i><small> Ingrese su telefono</small>
											</div>
										</div>


									</div>
									<div class="control-group form-group">
										<div class="controls">
											<label>Direccion de Email:</label>
											<input type="email" class="form-control" id="email_consulta" maxlength="30">
											<div id="error_email_consulta" class="text-danger" style="display:none">
													<i class="fa fa-exclamation"></i><small> Ingrese su email</small>
											</div>
										</div>
									</div>
									<div class="control-group form-group">
										<div class="controls">
											<label>Mensaje:</label>
											<textarea rows="4" cols="100" class="form-control" id="txt_consulta" maxlength="999" style="resize:none"></textarea>
										</div>
											<div id="error_txt_consulta" class="text-danger" style="display:none">
													<i class="fa fa-exclamation"></i><small> Ingrese su mensaje</small>
											</div>
									</div>

									<!-- For success/fail messages -->
									<button type="submit" class="btn btn-info" id="contactar">Enviar</button>
						</div>
						<div class="clearfix"><br></div>
						<div id="success" class="alert alert-success pt-4" role="alert" style="display:none">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<i class="fa fa-thumbs-up"></i>Su mensaje ha sido enviado correctamente
						</div>
						<div class="alert alert-danger pt-2" id="msgerror_danger" style="display:none">
		                    <button class="close" data-dismiss="alert"></button>
		                    <div><i class="fa fa-thumbs-down"></i> <b>Atención:&nbsp;</b> Ocurrio un error inesperado, verifica tu conexion de red e intenta nuevamente.</div>
		                </div>
					</form>
				</div>   
			</div>
			<!-- /.row -->
	</div>		
    <!-- Footer -->
    <?php include_once("vendor/includes/footer.php");  ?>

		<!-- Bootstrap core JavaScript -->
	<?php include_once("vendor/includes/jsreferences.php");?>
	<script src="js/utilidades.js"></script>

	<script type="text/javascript">
		var error = false;

		$("#tlf_consulta").on("keypress",function(e){
            if (e.which != 8 && e.which != 0 && e.which != 32 && (e.which < 48 || e.which > 57)) {
                return false;
            }
            $("#tlf_consulta").removeClass('is-invalid').addClass('is-valid'); 
            ocultar_err_msg("#error_tlf_consulta"); 
        });/**/

			$('#contactar').click(function(e){
					e.preventDefault();

					error = false;

					validar_inputs("#name_consulta", "#error_name_consulta");
					validar_inputs("#email_consulta", "#error_email_consulta");
					validar_inputs("#tlf_consulta", "#error_tlf_consulta");
					validar_inputs("#txt_consulta", "#error_txt_consulta");

					if(!error){

							nombre = $("#name_consulta").val();
							tlf = $("#tlf_consulta").val();
							email = $("#email_consulta").val();
							msj = $("#txt_consulta").val();

							//console.log(msj);
							$.ajax({
									data:  {accion: 4, nombre : nombre, tlf : tlf, email: email, msj : msj},
									//url:   'vendor/mail/contacto_acciones.php',
									url: 'vendor/class/soporte/soporte_acciones.php',
									type:  'post',
									dataType: "json",
									success:  function (data) {
											//respuesta = JSON.stringify(data);
											//console.log(data);
											$("#mailer").hide();

											if(data.estado == 0){
												$("#success").hide(); 
												$("#msgerror_danger").show();
											}
											else{
												$("#msgerror_danger").hide();
												$("#success").show();
											}
									},
									error: function(data){
											console.log(data);
											$("#msgerror_danger").show();
											$("#success").hide(); 
										 // window.location.href="cuenta.php?success=no";
									}
							});/**/
					}
			});

	</script>

	</body>

</html>
