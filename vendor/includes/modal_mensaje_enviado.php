<!-- Modal -->
<div class="modal fade" id="modal_send" tabindex="-1" role="dialog" aria-labelledby="modal_trash" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="body_trash" class="modal-body">
				<div class="col-12 ">
					<div class="alert alert-info text-center pull-center" role="alert">
						<p class="mb-0">Su mensaje se ha enviado correctamente, sera contactado por el propietario de la publicacion</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>