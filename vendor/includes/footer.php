    <footer class="footer text-center">
      <div class="container">
        <div class="row">
          <div class="col-6 col-sm-4 col-md-4 col-lg-4 mb-5 mb-lg-0">
            <p class="text-uppercase column-title">Informacion</p>
            <ul class="column-links">
	            <li class="column-item">
	              <a href="soporte" class="text-link">Contacto &amp; Soporte</a>
	            </li>
	            <li class="column-item">
	            	<a href="terminos" class="text-link">Terminos y Condiciones</a>
	            </li>
	            <li class="column-item">
	            	<a href="proteccion" class="text-link">Protección de datos personales</a>
	            </li>
	        </ul>
          </div>

          <div class="col-6 col-sm-4 col-md-4 col-lg-4 mb-5 mb-lg-0">
            <p class="text-uppercase column-title">Servicios</p>
            <ul class="column-links">
	            <li class="column-item">
	                <a rel="nofollow" href="publicar" class="text-link">Publica Gratis</a>
	            </li>
                <?php if(!isset($_SESSION["hash512"])){ ?>
	            <li class="column-item">
	            	<a rel="nofollow" href="cuenta" class="text-link">Crear tu cuenta</a>
	            </li>
                <?php } ?>
	            <li class="column-item">
	            	<!--a rel="nofollow" href="#" class="text-link" data-increment-metric="zendesk.footer.about">Empresas</a-->
	            </li>
	        </ul>
          </div>

          <div class="col-12 col-sm-4 col-md-4 col-lg-4 mb-5 mb-lg-0">
            <p class="text-uppercase column-title">Nuestras Redes</p>
            <ul class="list-inline mb-0 column-links">
              <li class="list-inline-item">
                <a class="btn btn-social" target="_Blank" href="<?php echo Utilidades::obtener_facebook($bd);?>">
                  <i class="fa fa-facebook-square"></i>
                </a>
              </li>
              <!--li class="list-inline-item">
                <a class="btn btn-social" target="_Blank" href="#">
                  <i class="fa fa-google-plus-square"></i>
                </a>
              </li-->
              <li class="list-inline-item">
                <a class="btn btn-social" target="_Blank" href="<?php echo Utilidades::obtener_twitter($bd);?>">
                  <i class="fa fa-twitter-square"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn btn-social" target="_Blank" href="<?php echo Utilidades::obtener_instagram($bd);?>">
                  <i class="fa fa-instagram"></i>
                </a>
              </li>
            </ul>
          </div>
          
        </div>
      </div>
    </footer>
    <div class="copyright text-white">
      <div class="container">
        <small><?php echo Utilidades::obtener_footer($bd);?></small>
      </div>
    </div>
