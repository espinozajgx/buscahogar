<!-- Modal -->
    <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="contact" aria-hidden="true">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div id="mdbody" class="modal-body">

            <div class="container-fluid">
                <div class="form-group">
                    <label for="name_consulta" class="col-form-label">Nombre:</label>
                    <input type="text" class="form-control" placeholder="Completar" id="name_consulta" maxlength="50">
                    <!--div id="error_name_consulta" class="text-danger" style="display:none">
                        <i class="fa fa-exclamation"></i><small> Ingrese su nombre</small>
                    </div-->
                </div>
        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email_consulta" class="col-form-label">Email:</label>
                            <input type="email" class="form-control" placeholder="Completar" id="email_consulta" maxlength="30">
                            <!--div id="error_email_consulta" class="text-danger" style="display:none">
                                <i class="fa fa-exclamation"></i><small> Ingrese su email</small>
                            </div-->
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tlf_consulta" class="col-form-label">Teléfono:</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">+54</span>
                              </div>
                              <input id="tlf_consulta" type="text" class="form-control" placeholder="" value="<?php echo Usuarios::obtener_telefonos($bd,$hash); ?>" aria-describedby="basic-addon1" autocomplete="off" maxlength="20">
                            <!--div id="error_tlf_consulta" class="text-danger" style="display:none">
                                <i class="fa fa-exclamation"></i><small> Ingrese su telefono</small>
                            </div-->
                            </div>

                        </div>
                    </div>  

                </div>

                <div class="form-group">
                    <label for="txt_consulta" class="col-form-label">Consulta:</label>
                    <textarea class="form-control" rows="8" placeholder="Completar" id="txt_consulta"></textarea>
                    <div id="error_txt_consulta" class="text-danger" style="display:none">
                        <i class="fa fa-exclamation"></i><small> Ingrese su mensaje</small>
                    </div>
                </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" id="send" class="btn btn-info">Enviar</button>
          </div>
        </div>
      </div>
    </div>