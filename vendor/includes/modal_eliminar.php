<!-- Modal -->
<div class="modal fade" id="modal_trash" tabindex="-1" role="dialog" aria-labelledby="modal_trash" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="modal_trash">Esta seguro de eliminar el elemento seleccionado?</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="body_trash" class="modal-body">
        <input type="hidden" id="code">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button id="erase" type="button" class="btn btn-danger">Confirmar</button>
      </div>
    </div>
  </div>
</div>