<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container">
  	<div class="col-8 col-sm-8 col-md-6 col-lg-4">
    	<a class="navbar-brand" href="inicio"><img src="img/logo/<?php Utilidades::obtener_logo($bd);?>" class="imgLogo" alt="logo"></a>
    </div>

      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="inicio">Inicio</a>
        </li>
        <div class="dropdown-divider"></div>
        <li class="nav-item">
          <a class="nav-link" href="propiedades">Propiedades</a>
        </li>
        <?php if(!isset($_SESSION["hash512"])){ ?>
        <li class="nav-item">
          <a class="nav-link" href="cuenta">Crea tu cuenta</a>
        </li>
        <?php } ?>

        <div class="dropdown-divider"></div>

        <li class="nav-item">
          <a class="nav-link" href="publicar">Publicar</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="blog/">Actualízate</a>
        </li>

        <?php if(!isset($_SESSION["hash512"])){ ?>
            <li class="nav-item">
                <a class="nav-link" href="ingresar">Ingresar</a>
            </li>
        <?php }else{ ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $user; ?> <i class="fa fa-user fa-fw"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                    <a class="dropdown-item" href="panel"><i class="fa fa-user fa-fw"></i> Mi Cuenta</a>
                    <a class="dropdown-item" href="panel-soporte"><i class="fa fa-question-circle fa-fw"></i> Soporte</a>
                    <a class="dropdown-item" href="vendor/class/usuario/logout"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                </div>
            </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</nav>