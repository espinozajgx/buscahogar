<?php
require_once("propiedad_data.php");
require_once('../../bin/connection.php');
require_once('../usuario/usuarios_data.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $bd = connection::getInstance()->getDb();

        $codigo = "";
        $tipo_oper = $_POST["tipo_oper"];
        $tipo_prop = $_POST["tipo_prop"];
        $id_barrio = $_POST["id_barrio"];
        $street = $_POST["street"];
        $ambientes = $_POST["ambientes"];
        $antiguedad = $_POST["antiguedad"];
        $inmueble = $_POST["inmueble"];
        $edificio = $_POST["edificio"];
        $tipo_usuario = $_POST["tipo_usuario"];
        $precio_a = $_POST["precio_a"];
        $precio_b = $_POST["precio_b"];
        $moneda = $_POST["moneda"];
        $superficie_a = $_POST["superficie_a"];
        $superficie_b = $_POST["superficie_b"];
        $superficie = $_POST["superficie"];
        
        $detalles = $_POST["detalles"];
        $countd = $_POST["countd"];
        $adicionales = $_POST["adicionales"];
        $counta = $_POST["counta"];

        $start = $_POST["start"];
        $limit = $_POST["limit"];


    $datos = Prop::obtener_listado_propiedades_by_filtros($bd, $codigo, $tipo_oper, $tipo_prop, $id_barrio, $street, $ambientes, $antiguedad, $inmueble, $edificio, $tipo_usuario, $precio_a, $precio_b, $moneda, $superficie_a, $superficie_b, $superficie, $detalles, $countd, $adicionales, $counta ,1, 1, $start, $limit);

    $listado = "";
    //print_r($adicional);
    $src = "prop/";
    $precio_final = "";
    $paginacion = "";

    $no_found ='<div class="col-lg-12">
                <div class="alert alert-info" role="alert">
                  <h4 class="alert-heading">No existen resultados para el criterio de busqueda!</h4>
                  <hr>
                  <p class="mb-0">Modifica los parametros e intenta nuevamente!</p>
                </div>
            </div>';/**/

    if($datos){
        $i=1;
        $cantidad = count($datos);

        foreach ($datos as $prop) {
            $codigo = $prop["codigo"];
            $ok = true;

            if($countd > 0){
                if($prop["idc"] != $countd){
                    $ok = false;
                }
                else{
                    $ok = true;
                }
            }

            if($counta > 0){
                if($prop["ida"] != $counta){
                    $ok = false;
                }
                else{
                    $ok = true;
                }
            }

            if($ok){

                //$hash_usuario = Prop::obtener_hash_usuario($bd, $codigo);
                $hash_usuario = $prop["hash"];

                $id_tipo_usuario = Usuarios::obtener_id_tipo_usuario($bd, $hash_usuario);
                $logo = Usuarios::obtener_logo_path($bd, $hash_usuario);

                if($id_tipo_usuario == 1){
                    //$usuario = Usuarios::obtener_nombre($bd, $hash_usuario);
                    //$usuario .= " ". Usuarios::obtener_apellido($bd, $hash_usuario);
                    $usuario = "Particular";
                    $tipo_usuario = "Particular";

                }
                else{
                    $usuario = Usuarios::obtener_nombre_inmobiliaria($bd, $hash_usuario);
                    $tipo_usuario = "Inmobiliaria";
                }

                //$estatus = Prop::obtener_estatus($bd, $codigo);

                $tipo_prop = Prop::obtener_tipo_prop($bd, $codigo);
                $barrio = Prop::obtener_barrio($bd, $codigo);
                $calle = $prop["calle"];
                $altura = $prop["altura"];
                $foto = Prop::obtener_primera_foto_prop($bd, $codigo);

                if($foto == ""){
                    $foto = "img/desing/no-pic.jpg";
                }
                else{
                    $foto = $src . $codigo ."/". $foto;
                }

                //$id_moneda = Prop::obtener_id_moneda($bd, $codigo);
                $id_moneda = $prop["id_moneda"];
                $tipo_moneda = Prop::obtener_tipo_moneda_by_id($bd, $id_moneda);
                $precio_unitario = Prop::obtener_precio($bd, $codigo);

                if($precio_unitario != ""){
                    $precio = number_format(doubleval($precio_unitario),"0",",",".");

                    $precio_final =  $tipo_moneda ." ". $precio;
                }

                //$fecha = Prop::obtener_fecha($bd, $codigo);
                $fecha = $prop["fecha"];

                $tipo_oper = Prop::obtener_tipo_oper($bd, $codigo);
                $direccion = $tipo_prop . " en " . $barrio . ", " . $calle ." ". $altura;
                $titulo = $tipo_oper . " de " . $direccion;

                /*if($cantidad == 1){
                    $listado .= '<div class="col-12 col-sm-6 col-md-6 col-lg-10 mb-4">';
                }
                else
                if($cantidad == 2){
                    $listado .= '<div class="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">';
                }
                else{
                    $listado .= '<div class="col-12 col-sm-6 col-md-6 col-lg-4 mb-4">'; 
                }/**/

                $listado .= '<div class="col-12 col-sm-6 col-md-6 col-lg-4 mb-4 invisible">';
                    $listado .= '<div class="card h-100">';
                        $listado .= '<a class="text-center card-img-top-cat" href="propiedad-'. $codigo .'"><img class="card-img-top-cat" src="'. $foto .'" alt="'. $codigo .'" title="'. $codigo .'">';
                        $listado .= '<div class="card-body border-top">';
                            $listado .= '<small><p class="card-title col-12">';
                                $listado .= '<a href="propiedad-'. $codigo .'">'. $titulo .'</a>';
                            $listado .= '</p></small>';
                            $listado .= '<span class="text-right pull-right my-2">';
                                $listado .= '<strong>'. $precio_final .'</strong>
                                        </span><br>';
                            $listado .= '<small class="text-muted text-center pull-center mt-4">
                                            <strong>COD: '. $codigo .'</strong>';
                                            
                            //$listado .= '<br><em class="tiny">Publicado el '. $fecha .'</em>
                                        $listado .= ' </small>
                                    </div>';
                        $listado .= '<div class="card-footer" >
                                        <div class="col-12 text-center pull-center" style="height: 50px;"><a href="#">';
                                        if($logo != "")
                                $listado .= '<img src="img/users/'. $logo .'" class="img-footer-prop"; alt="'. $usuario .'" title="'. $usuario .'">';
                                        else
                                $listado .= '<small><strong>'. $usuario .'</strong></small>';          
                           $listado .= '</a></div>';

                        $listado .= '</div>
                                </div>
                            </div>';

                if($i >= $limit){
                    break;
                }

                $i++;
            }/**/
            
        }

        if($listado != ""){
            if($cantidad == 1){
                $listado .= '<div class="col-12 col-sm-6 col-md-6 col-lg-4 mb-4 invisible">';
                    $listado .= '<div class="card h-100">';
                        $listado .= '<a class="text-center card-img-top-cat" href="propiedad-'. $codigo .'"><img class="card-img-top-cat" src="'. $foto .'" alt="'. $codigo .'" title="'. $codigo .'">';
                        $listado .= '<div class="card-body border-top">';
                            $listado .= '<small><p class="card-title col-12">';
                                $listado .= '<a href="propiedad-'. $codigo .'">'. $titulo .'</a>';
                            $listado .= '</p></small>';
                            $listado .= '<span class="text-right pull-right my-2">';
                                $listado .= '<strong>'. $precio_final .'</strong>
                                        </span><br>';
                            $listado .= '<small class="text-muted text-center pull-center mt-4">
                                            <strong>COD: '. $codigo .'</strong>';
                                            
                            //$listado .= '<br><em class="tiny">Publicado el '. $fecha .'</em>
                                        $listado .= ' </small>
                                    </div>';
                        $listado .= '<div class="card-footer" >
                                        <div class="col-12 text-center pull-center" style="height: 50px;"><a href="#">';
                                        if($logo != "")
                                $listado .= '<img src="img/users/'. $logo .'" style="height: 50px;" alt="'. $usuario .'" title="'. $usuario .'">';
                                        else
                                $listado .= '<small><strong>'. $usuario .'</strong></small>';          
                           $listado .= '</a></div>';

                        $listado .= '</div>
                                </div>
                            </div>';

                $listado .= '<div class="col-12 col-sm-6 col-md-6 col-lg-4 mb-4 invisible">';
                    $listado .= '<div class="card h-100">';
                        $listado .= '<a class="text-center card-img-top-cat" href="propiedad-'. $codigo .'"><img class="card-img-top-cat" src="'. $foto .'" alt="'. $codigo .'" title="'. $codigo .'">';
                        $listado .= '<div class="card-body border-top">';
                            $listado .= '<small><p class="card-title col-12">';
                                $listado .= '<a href="propiedad-'. $codigo .'">'. $titulo .'</a>';
                            $listado .= '</p></small>';
                            $listado .= '<span class="text-right pull-right my-2">';
                                $listado .= '<strong>'. $precio_final .'</strong>
                                        </span><br>';
                            $listado .= '<small class="text-muted text-center pull-center mt-4">
                                            <strong>COD: '. $codigo .'</strong>';
                                            
                            //$listado .= '<br><em class="tiny">Publicado el '. $fecha .'</em>
                                        $listado .= ' </small>
                                    </div>';
                        $listado .= '<div class="card-footer" >
                                        <div class="col-12 text-center pull-center" style="height: 50px;"><a href="#">';
                                        if($logo != "")
                                $listado .= '<img src="img/users/'. $logo .'" style="height: 50px;" alt="'. $usuario .'" title="'. $usuario .'">';
                                        else
                                $listado .= '<small><strong>'. $usuario .'</strong></small>';          
                           $listado .= '</a></div>';

                        $listado .= '</div>
                                </div>
                            </div>';       
            }
            else
            if($cantidad == 2){
                $listado .= '<div class="col-12 col-sm-6 col-md-6 col-lg-4 mb-4 invisible">';
                    $listado .= '<div class="card h-100">';
                        $listado .= '<a class="text-center card-img-top-cat" href="propiedad-'. $codigo .'"><img class="card-img-top-cat" src="'. $foto .'" alt="'. $codigo .'" title="'. $codigo .'">';
                        $listado .= '<div class="card-body border-top">';
                            $listado .= '<small><p class="card-title col-12">';
                                $listado .= '<a href="propiedad-'. $codigo .'">'. $titulo .'</a>';
                            $listado .= '</p></small>';
                            $listado .= '<span class="text-right pull-right my-2">';
                                $listado .= '<strong>'. $precio_final .'</strong>
                                        </span><br>';
                            $listado .= '<small class="text-muted text-center pull-center mt-4">
                                            <strong>COD: '. $codigo .'</strong>';
                                            
                            //$listado .= '<br><em class="tiny">Publicado el '. $fecha .'</em>
                                        $listado .= ' </small>
                                    </div>';
                        $listado .= '<div class="card-footer" >
                                        <div class="col-12 text-center pull-center" style="height: 50px;"><a href="#">';
                                        if($logo != "")
                                $listado .= '<img src="img/users/'. $logo .'" style="height: 50px;" alt="'. $usuario .'" title="'. $usuario .'">';
                                        else
                                $listado .= '<small><strong>'. $usuario .'</strong></small>';          
                           $listado .= '</a></div>';

                        $listado .= '</div>
                                </div>
                            </div>';
            }
            echo $listado ;
        }
        else{
            echo $no_found;
        }
    }
    else{
        echo $no_found;
    }
}
else{
    echo $no_found;
}
?>
