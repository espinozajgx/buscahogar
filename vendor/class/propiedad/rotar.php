<?php
ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_input_time', 100000);
ini_set('max_execution_time', 100000);
ini_set('memory_limit','1024M');
ini_set ('gd.jpeg_ignore_warning', 1);
ini_set('default_charset', 'UTF-8'); 
//error_reporting(0);
error_reporting(E_ALL & E_NOTICE);

session_start();

if(isset($_POST["imagen"])){
    $imagen = "../../../" . $_POST["imagen"];
    $grados = $_POST["grados"];
    //$grados = gmp_neg($grados);
    //echo $grados;
    rotar($imagen, $grados);
           
}
else{
    echo "no photo";
}


function limpiarString($texto){
    $textoLimpio = preg_replace('([^A-Za-z0-9.])', '', $texto);                            
    return $textoLimpio;
}
                // Archivo y rotacion
function rotar($imagen, $grados){

    // Tipo de contenido
    //header('Content-type: image/jpeg');
    // Cargar
    $origen = imagecreatefromjpeg($imagen);
    // Rotar
    echo $grados;
    if($grados == 0)
        $rotar = imagerotate($origen, 0, 0);
    else
        $rotar = imagerotate($origen, $grados, 0);
        
    // Imprimir
    echo imagejpeg($rotar,$imagen,90);
    // Liberar la memoria
    imagedestroy($origen);
    imagedestroy($rotar);

    //return $img;
}