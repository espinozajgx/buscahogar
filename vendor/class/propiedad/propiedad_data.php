<?php
	class Prop{


		/*clase que contiene todas las funciones relacionadas a las props*/
		function __construct(){

		}

		/**
		retorna 
		*/
		public static function obtener_id($bd, $codigo){

			$consulta = "SELECT id_prop FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_prop"];								
				}
				return false;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_codigo($bd, $id_prop){

			$consulta = "SELECT codigo FROM prop WHERE id_prop = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_prop));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["codigo"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_estatus($bd, $codigo){

			$consulta = "SELECT estatus FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["estatus"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_fecha($bd, $codigo){

			$consulta = "SELECT fecha FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["fecha"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_estatus_general($bd, $codigo){

			$consulta = "SELECT estatus_general FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_prop));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["estatus_general"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

				/**
		retorna 
		*/
		public static function obtener_hash_usuario($bd, $codigo){

			$consulta = "SELECT hash FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["hash"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_estado_inmueble($bd, $codigo){

			$consulta = "SELECT nombre FROM prop p INNER JOIN estado_prop ep ON p.estado_inmueble = ep.id_estado_prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];							
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_estado_propiedad($bd, $codigo){

			$consulta = "SELECT nombre FROM prop p INNER JOIN estado_prop ep ON p.estado_propiedad = ep.id_estado_prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];						
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_descripcion($bd, $codigo){

			$consulta = "SELECT descripcion FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row["descripcion"]){		                        
					return $row["descripcion"];								
				}
				//return "COD #". $codigo;
				//return '<small>No existe informacion para esta propiedad</small>';   

			} catch (Exception $e) {
				echo $e;
				return $e;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_id_antiguedad($bd, $codigo){

			$consulta = "SELECT antiguedad FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row["antiguedad"]){		                        
					return $row["antiguedad"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_antiguedad($bd, $codigo){

			$consulta = "SELECT nombre FROM prop p INNER JOIN antiguedad a ON p.antiguedad = a.id_antiguedad WHERE p.codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row["nombre"]){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_ambiente($bd, $codigo){

			$consulta = "SELECT ambiente FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row["ambiente"]){
					if($row["ambiente"] == 5)
						return "más";

					return $row["ambiente"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_id_tipo_oper($bd, $codigo){

			$consulta = "SELECT id_tipo_oper FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_tipo_oper"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION

				/**
		retorna 
		*/
		public static function obtener_tipo_oper($bd, $codigo){

			$consulta = "SELECT nombre FROM prop p INNER JOIN tipo_operacion top ON p.id_tipo_oper = top.id_tipo_oper WHERE codigo ='". $codigo . "'";
			//echo $consulta;		
			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}/**/
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_tipo_oper_by_id($bd, $id){

			$consulta = "SELECT nombre FROM tipo_operacion WHERE id_tipo_oper = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_id_tipo_prop($bd, $codigo){

			$consulta = "SELECT id_tipo_prop FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_tipo_prop"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_tipo_prop($bd, $codigo){

			$consulta = "SELECT nombre FROM prop p INNER JOIN tipo_propiedad tp ON p.id_tipo_prop = tp.id_tipo_prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_tipo_prop_by_id($bd, $id){

			$consulta = "SELECT nombre FROM tipo_propiedad WHERE id_tipo_prop = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 



		/**
		retorna 
		*/
		public static function obtener_id_moneda($bd, $codigo){

			$consulta = "SELECT id_moneda FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_moneda"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_tipo_moneda_by_id($bd, $id){

			$consulta = "SELECT simbolo FROM moneda WHERE id_moneda = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["simbolo"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_precio($bd, $codigo){

			$consulta = "SELECT precio FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["precio"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_id_tipo_superficie($bd, $codigo){

			$consulta = "SELECT id_tipo_superficie FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_tipo_superficie"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_tipo_superficie_by_id($bd, $id){

			$consulta = "SELECT simbolo FROM tipo_superficie WHERE id_tipo_superficie = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["simbolo"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_superficie($bd, $codigo){

			$consulta = "SELECT superficie FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["superficie"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_id_barrio($bd, $codigo){

			$consulta = "SELECT id_barrio FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_barrio"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_barrio_by_id($bd, $id_barrio){

			$consulta = "SELECT nombre FROM barrio WHERE id_barrio = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_barrio));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_barrio($bd, $codigo){

			$consulta = "SELECT nombre FROM prop p INNER JOIN barrio b ON p.id_barrio = b.id_barrio WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_calle($bd, $codigo){

			$consulta = "SELECT calle FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["calle"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_altura($bd, $codigo){

			$consulta = "SELECT altura FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["altura"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_bathroom($bd, $codigo){

			$consulta = "SELECT bathroom FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row["bathroom"]){		                        
					return $row["bathroom"];								
				}
				return "";

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_elevator($bd, $codigo){

			$consulta = "SELECT elevator FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row["elevator"]){		                        
					return $row["elevator"];								
				}
				return "";

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_keywords_prop($bd, $codigo){

			$consulta = "SELECT keywords_prop FROM prop WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["keywords_prop"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_fotos_prop($bd, $codigo, $ruta){

			$consulta = "SELECT * FROM foto_prop WHERE codigo = ? ORDER BY orden ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$datos = "";

				foreach ($row as $foto) {
					$datos .=  '<div id="'. $foto["id_fp"] .'" file="'. $foto["foto"] .'" class="col-sm-4 col-md-3 col-lg-3 divPhotoItem rounded">';
					$datos .=  '<img class = "imgPhotoItem" src = "'.$ruta.'prop/'.$codigo ."/". $foto["foto"] .'" />';
					$datos .=  '<a href ="#" class="cvf_delete_image" title="Eliminar"><img class = "delete-btn" src = "'.$ruta.'img/delete-btn.png" /></a>';
					$datos .=  '<img class = "der-btn" src = "img/der.png" />';
                    $datos .=  '<img class = "izq-btn" src = "img/izq.png" />';
					//$datos .=  '<img src="'. $foto["foto"] .'">';
					$datos .=  '</div>';
				}

				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;

			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_fotos_prop_slider($bd, $codigo){

			$consulta = "SELECT * FROM foto_prop WHERE codigo = ? ORDER BY orden ASC";
    		$src = "prop/";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$fotos = '<div class="carousel-inner">';
				$indicadores = '<ol class="carousel-indicators">';
				$i = 0;

				if($row){
					foreach ($row as $foto) {
						if($i == 0) $activa = "active";
						else $activa = "";

							$foto_path = $src . $codigo ."/". $foto["foto"];
						
	                		$indicadores .= '<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'" class="'.$activa.'"></li>';

							$fotos .=  '<div class="carousel-item '.$activa.'">';
				            $fotos .=  '<a href="'. $foto_path .'" data-lightbox="roadtrip"><img class="img-fluid rounded" src="'.$foto_path .'" alt="'.$codigo.'" title ="'.$codigo.'"></a>';
				            $fotos .=  '</div>';

		            	$i++;
					}
				}
				else{
						$foto_path = "img/desing/no-pic.png";
						
                		$indicadores .= '<li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>';

						$fotos .=  '<div class="carousel-item active">';
			            $fotos .=  '<a href="'. $foto_path .'" data-lightbox="roadtrip"><img class="img-fluid rounded" src="'.$foto_path .'" alt="'.$codigo.'" title ="'.$codigo.'"></a>';
			            $fotos .=  '</div>';
				}



				$indicadores .= '</ol>';
				$fotos .= '</div>';

				$datos = $indicadores . $fotos;
				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;

			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_primera_foto_prop($bd, $codigo){

			$consulta = "SELECT * FROM foto_prop WHERE codigo = ? AND  orden = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){
					return $row["foto"];
				}

			} catch (Exception $e) {
				echo $e;
				return false;

			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_cant_prop_by_user($bd, $hash){

			$consulta = "SELECT count(id_prop) AS cant FROM prop WHERE hash = ? AND estatus = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["cant"];								
				}
				else{
					return 0;
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION /**/


		/**
		retorna 
		*/
		public static function obtener_otras_prop_by_user($bd, $hash, $codigo){

			$consulta = "SELECT p.codigo, b.nombre AS barrio, p.calle, p.altura, top.nombre AS top, tp.nombre AS tp FROM prop p INNER JOIN barrio b ON b.id_barrio = p.id_barrio INNER JOIN tipo_operacion top ON p.id_tipo_oper = top.id_tipo_oper INNER JOIN tipo_propiedad tp ON p.id_tipo_prop = tp.id_tipo_prop WHERE p.hash = ? AND p.codigo != ? AND p.estatus = 1 AND p.estatus_general = 1 ORDER BY RAND() DESC LIMIT 4";

    		$src = "prop/";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash,$codigo));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$datos = "";
				foreach ($row as $prop) {

					$titulo = $prop["top"] . " de " . $prop["tp"] . " en " . $prop["barrio"] . ", " . $prop["calle"] . " " . $prop["altura"];
					$codigo = $prop["codigo"];
					$foto = Prop::obtener_primera_foto_prop($bd, $codigo);;

					if($foto == ""){
		                $foto = "img/desing/no-pic.png";
		            }
		            else{
		                $foto = $src . $codigo ."/". $foto;
		            }

					$datos .=  '<div class="col-md-3 col-sm-6 mb-4 border-left border-right" title="'.$titulo.'">
						        <a href="propiedad-'.$prop["codigo"].'" >
						          <img class="imgOtros" src="'.$foto.'" alt="'.$titulo.'" >
						        </a>
						      </div>';
				}

				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION

		/**
		retorna 
		*/
		public static function obtener_otras_prop_by_cat($bd, $tipo_oper, $codigo){

			$consulta = "SELECT foto FROM prop p INNER JOIN foto_prop fp ON p.codigo = fp.codigo WHERE p.id_tipo_oper = ? ANd fp.orden = 1 AND p.codigo != ? ORDER BY fecha DESC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($tipo_oper,$codigo));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$datos = "";
				foreach ($row as $prop) {
					$datos .=  '<div class="col-md-3 col-sm-6 mb-4">
						        <a href="'.$codigo.'">
						          <img class="img-fluid imgLogo" src="prop/'.$codigo.'/'.$prop["foto"].'" alt="">
						        </a>
						      </div>';
				}

				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION

		/**
		retorna 
		*/
		public static function obtener_campos_select_keyword($bd){

			$consulta = "SELECT * FROM keywords ORDER BY id_keywords ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$datos = "";
				foreach ($row as $keywords) {
					$datos .=  '<option value="'. $keywords["id_keywords"] .'">'. $keywords["nombre"] .'</option>';
				}

				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_campos_check_adicionales_filtro($bd){

			$consulta = "SELECT id_adicional AS id, nombre AS name FROM adicionales ORDER BY id_adicional ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_cant_campos_check_adicionales($bd){

			$consulta = "SELECT count(id_adicional) AS cant FROM adicionales";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				return $row["cant"];

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_campos_check_caracteristicas_filtro($bd){

			$consulta = "SELECT id_caracteristica AS id, nombre AS name FROM caracteristicas ORDER BY id_caracteristica ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_cant_campos_check_caracteristicas($bd){

			$consulta = "SELECT count(id_caracteristica) AS cant FROM caracteristicas";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				return $row["cant"];

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_campos_check_carac($bd, $codigo){

			$consulta = "SELECT * FROM caracteristicas ORDER BY id_caracteristica ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$datos = "";
				foreach ($row as $caracteristicas) {

					$datos .=  '<div class="col-6 col-xs-6 col-sm-4 col-md-3 col-lg-3 mb-4 pl-4">
				                    <div class="form-check custom-control custom-checkbox">
				                        <input type="checkbox" class="custom-control-input detail" id="'. $caracteristicas["id_caracteristica"] .'" value="'. $caracteristicas["id_caracteristica"] .'"';

										    if(Prop::verificar_campos_check_prop($bd, $caracteristicas["id_caracteristica"], $codigo)){
										    	$datos .= 'checked';
										    }/**/
				    					$datos .= '>
				                        <label class="custom-control-label" for="'. $caracteristicas["id_caracteristica"] .'">'. $caracteristicas["nombre"] .'</label>
				                    </div>
				                </div>';
				}

				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function verificar_campos_check_prop($bd, $id_caracteristica, $codigo){

			$consulta = "SELECT id_caracteristica FROM caract_prop WHERE codigo = ? AND id_caracteristica = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo,$id_caracteristica));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return true;								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 




		/**
		retorna 
		*/
		public static function obtener_caracteristicas_prop($bd, $codigo){

			$consulta = "SELECT DISTINCT id_cp, nombre FROM caract_prop cp INNER JOIN caracteristicas c ON  cp.id_caracteristica = c.id_caracteristica AND cp.codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);
				
				if($row){		
    				$listado = '';  
					foreach ($row as $caract) {
						$listado .= '<div class="btn-group-sm mr-2 mt-2" role="group">
					                    <button type="button" class="btn btn-info"> '. $caract["nombre"] .' <i class="fa fa-check fa-fw"></i> </button>
					                </div>';    
					}	
					echo $listado;							
				}
				else{
					echo $listado = 'No existe informacion para esta propiedad';   
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_campos_check_adicionales($bd, $codigo){

			$consulta = "SELECT * FROM adicionales ORDER BY id_adicional ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$datos = "";
				$i = 1;
				foreach ($row as $adicionales) {

					if($i != 4){
						$col = "col-lg-2";
						$padding = "";
					}
					else{
						$col = "col-lg-3";
						$padding = "";
					}

					$datos .=  '<div class="col-6 col-xs-6 col-sm-4 col-md-3 '.$col.' mb-4 pl-3">
				                    <div class="form-check custom-control custom-checkbox">
				                        <input type="checkbox" class="custom-control-input adicional" id="'. $adicionales["id_adicional"] .'_ad" value="'. $adicionales["id_adicional"] .'"';

										    if(Prop::verificar_campos_check_adicionales($bd, $adicionales["id_adicional"], $codigo)){
										    	$datos .= 'checked';
										    }/**/
				    					$datos .= '>
				                        <label class="custom-control-label" for="'. $adicionales["id_adicional"] .'_ad">'. $adicionales["nombre"] .'</label>
				                    </div>
				                </div>';

				    $i++;
				}

				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function verificar_campos_check_adicionales($bd, $id_adicional, $codigo){

			$consulta = "SELECT id_adicional FROM adicionales_prop WHERE codigo = ? AND id_adicional = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo,$id_adicional));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return true;								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_adicionales_prop($bd, $codigo){

			$consulta = "SELECT nombre FROM adicionales_prop ap INNER JOIN adicionales a ON  a.id_adicional = ap.id_adicional WHERE codigo = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);
				
				$listado = '';   
				if($row){		
                     
					foreach ($row as $ad) {
						$listado .= '<div class="btn-group-sm mr-2 mt-2" role="group">
					                    <button type="button" class="btn btn-success"> '. $ad["nombre"] .' <i class="fa fa-check fa-fw"></i> </button>
					                </div>';    
					}	
					echo $listado;							
				}
				else{
					echo $listado = 'No existe informacion para esta propiedad';   
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function agregar($bd, $codigo, $estatus, $estatus_general, $estado_inmueble, $estado_propiedad, $descripcion, $antiguedad, $ambiente,
			$id_tipo_oper, $id_tipo_prop, $id_moneda, $precio, $id_tipo_superficie, $superficie, $id_barrio, $calle, $altura, $bathroom, $elevator, $keywords_prop,
			$hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO prop ( " .
				" codigo, estatus, estatus_general, estado_inmueble, estado_propiedad,".
				" descripcion, antiguedad, ambiente, id_tipo_oper,".
				" id_tipo_prop, id_moneda, precio, id_tipo_superficie, superficie,".
				" id_barrio, calle, altura, bathroom, elevator, keywords_prop, hash)".
				" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($codigo, $estatus, $estatus_general, $estado_inmueble, $estado_propiedad, $descripcion, $antiguedad, $ambiente,
			    $id_tipo_oper, $id_tipo_prop, $id_moneda, $precio, $id_tipo_superficie, $superficie, $id_barrio, $calle, $altura, $bathroom, $elevator, $keywords_prop,
			    $hash));

				/*if($resultado){
					return Prop::obtener_max_id($bd,"id_prop","prop");		        	
				}*/
				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			} 	
		}


		/**
		retorna 
		*/
		public static function editar($bd, $estatus, $estado_inmueble, $estado_propiedad, $descripcion, $antiguedad, $ambiente, $id_tipo_oper, $id_tipo_prop, $id_moneda, $precio,$id_tipo_superficie, $superficie, $id_barrio, $calle, $altura, $bathroom, $elevator, $keywords_prop, $codigo, $hash){
			
			// Sentencia INSERT
			$consulta = "UPDATE prop SET" .
				" estatus = ?, estado_inmueble = ?, estado_propiedad = ?, descripcion = ?, antiguedad = ?, ambiente = ?, id_tipo_oper = ?," .
				" id_tipo_prop = ?, id_moneda = ?, precio = ?, id_tipo_superficie = ?,superficie = ?, id_barrio = ?," .
				" calle = ?, altura = ?, bathroom = ?, elevator = ?, keywords_prop = ?" .
				" WHERE codigo = ? AND hash = ?";

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($estatus, $estado_inmueble, $estado_propiedad, $descripcion, $antiguedad, $ambiente, $id_tipo_oper, $id_tipo_prop, $id_moneda ,$precio, $id_tipo_superficie, $superficie, $id_barrio, $calle, $altura, $bathroom, $elevator, $keywords_prop, $codigo, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}	

		/**
		retorna 
		*/
		public static function verificar_favoritos_prop($bd, $codigo, $hash){

			$consulta = "SELECT * FROM favoritos WHERE codigo = ? AND hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($codigo, $hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return true;								
				}
				return false;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function agregar_fav($bd, $codigo, $hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO favoritos (codigo, hash) VALUES (?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($codigo, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			} 	
		}

		/**
		retorna 
		**/
		public static function eliminar_fav($bd, $codigo, $hash)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM favoritos WHERE codigo = ? AND hash = ?";
		    //echo $consulta;		
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($codigo, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			}/**/
		}/**/


		/**
		retorna 
		*/
		public static function agregar_fotos_masivo($bd, $datos)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO foto_prop (foto, orden, codigo) VALUES " . $datos;
			
			try {
		        // Preparar la sentencia
		        $comando = $bd->prepare($consulta);
		        $resultado = $comando->execute();

		        return $resultado;
		       
	        } catch (PDOException $e) {
	            // Aquí puedes clasificar el error dependiendo de la excepción
	            // para presentarlo en la respuesta Json
	            echo $e;
	            return $e;
	        } /**/	
		}

		/**
		retorna 
		*
		public static function agregar_keyworks_masivo($bd, $datos)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO keywords_prop (id_keyword,id_prop) VALUES " . $datos;
			
			try {
		        // Preparar la sentencia
		        $comando = $bd->prepare($consulta);
		        $resultado = $comando->execute();
		       
	        } catch (PDOException $e) {
	            // Aquí puedes clasificar el error dependiendo de la excepción
	            // para presentarlo en la respuesta Json
	            //echo $e;
	            return $e;
	        } 	
		} /**/

		/**
		retorna 
		*/
		public static function agregar_caracteristicas_masivo($bd, $datos)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO caract_prop (id_caracteristica, codigo) VALUES " . $datos;
			
			try {
		        // Preparar la sentencia
		        $comando = $bd->prepare($consulta);
		        $resultado = $comando->execute();

		        return $resultado;
		       
	        } catch (PDOException $e) {
	            // Aquí puedes clasificar el error dependiendo de la excepción
	            // para presentarlo en la respuesta Json
	            //echo $e;
	            return $e;
	        } /**/	
		}

/**
		retorna 
		*/
		public static function agregar_adicional_masivo($bd, $datos)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO adicionales_prop (id_adicional, codigo) VALUES " . $datos;
			
			try {
		        // Preparar la sentencia
		        $comando = $bd->prepare($consulta);
		        $resultado = $comando->execute();

		        return $resultado;
		       
	        } catch (PDOException $e) {
	            // Aquí puedes clasificar el error dependiendo de la excepción
	            // para presentarlo en la respuesta Json
	            //echo $e;
	            return $e;
	        } /**/	
		}

		/**
		retorna 
		**/
		public static function eliminar_fotos_prop($bd, $codigo, $hash)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM foto_prop WHERE codigo like '". $codigo ."'";
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}/**/

		/**
		retorna 
		**/
		public static function eliminar_caract_prop($bd, $codigo, $hash)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM caract_prop WHERE codigo like '". $codigo ."'";
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}/**/
		
		/**
		retorna 
		**/
		public static function eliminar_adicionales_prop($bd, $codigo, $hash)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM adicionales_prop WHERE codigo like '". $codigo ."'";
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}/**/

		/**
		retorna 
		*/
		public static function cambiar_estatus_by_hash_usuario($bd, $estatus, $hash){
			
			// Sentencia INSERT
			$consulta = "UPDATE prop SET estatus_general = ? WHERE hash = ?";
		   
		    try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($estatus, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			}
		}


		/**
		retorna 
		*/
		public static function cambiar_estatus($bd, $estatus, $codigo){
			
			// Sentencia INSERT
			$consulta = "UPDATE prop SET estatus = ? WHERE codigo = ?";
		   
		    try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($estatus, $codigo));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			}
		}

		/**
		retorna 
		*/
		public static function cambiar_estatus_general($bd, $estatus, $codigo){
			
			// Sentencia INSERT
			$consulta = "UPDATE prop SET estatus_general = ? WHERE codigo = ?";
		   
		    try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($estatus, $codigo));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			}
		}

		/**
		retorna 
		*/
		public static function obtener_listado_propiedades_by_hash($bd, $hash, $estatus_general){

			$consulta = "SELECT * FROM prop WHERE hash = ? AND estatus_general = ? AND estatus != 3 ORDER BY id_prop DESC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash, $estatus_general));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_listado_propiedades_by_hash_abd_filtros($bd, $hash, $codigo, $id_tipo_oper, $id_tipo_prop, $estatus_general, $offset, $limit){
			$filtro = 0;
			$consulta = "SELECT * FROM prop WHERE ";

			if($id_tipo_oper != 0){

				if($filtro > 0)
					$consulta .= "AND id_tipo_oper = ". $id_tipo_oper ." ";
				else
					$consulta .= "id_tipo_oper = ". $id_tipo_oper ." ";

				$filtro++;
			}

			if($id_tipo_prop != 0){
				if($filtro > 0)
					$consulta .= "AND id_tipo_prop = ". $id_tipo_prop ." ";
				else
					$consulta .= "id_tipo_prop = ". $id_tipo_prop ." ";

				$filtro++; 
			}

			if($codigo != ""){
				if($filtro > 0)
					$consulta .= "AND codigo = '". $codigo ."' ";
				else
					$consulta .= "codigo = '". $codigo ."' ";

				$filtro++;
			}

			if($filtro > 0)
				$consulta .=  "AND hash = '". $hash ."' AND estatus_general = ". $estatus_general ." AND estatus !=3 ORDER BY id_prop DESC LIMIT " . $offset . "," .$limit;
			else
				$consulta .=  "hash = '". $hash ."' AND estatus_general = ". $estatus_general ." AND estatus !=3 ORDER BY id_prop DESC LIMIT " . $offset . "," .$limit;

			//echo $consulta;
			
			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}/**/
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_listado_propiedades_by_filtros($bd, $codigo, $id_tipo_oper, $id_tipo_prop, $id_barrio, $calle, $ambientes, $antiguedad, $inmueble, $edifico, $tipo_usuario, $precio_a, $precio_b, $moneda, $superficie_a, $superficie_b, $superficie, $detalles, $countd, $adicionales, $counta, $estatus_general, $estatus, $offset, $limit){

			$filtro = 0;
			$consulta = "";

			$consulta1 = "SELECT p.calle, p.altura, p.id_prop, p.codigo, p.estado_inmueble, p.estado_propiedad, p.id_moneda, p.precio, DATE_FORMAT(p.fecha, '%d-%m-%Y %H:%i:%s') AS fecha, u.id_tipo_usuario, u.hash, u.logo";

			$consulta2 = " FROM prop p INNER JOIN usuario u ON p.hash = u.hash ";
			if($countd > 0){

				if($filtro > 0)
					$consulta .= " AND cp.id_caracteristica IN ". $detalles ." ";
				else
					$consulta .= " cp.id_caracteristica IN ". $detalles ." ";

				$consulta1 .= ", count(DISTINCT cp.id_caracteristica) AS idc ";
				$consulta2 .= "INNER JOIN caract_prop cp ON p.codigo = cp.codigo ";

				$filtro++;
			}

			if($counta > 0){

				if($filtro > 0)
					$consulta .= " AND ap.id_adicional IN ". $adicionales ." ";
				else
					$consulta .= " ap.id_adicional IN ". $adicionales ." ";

				$consulta1 .= ", count(DISTINCT ap.id_adicional) AS ida";
				$consulta2 .= "INNER JOIN adicionales_prop ap ON p.codigo = ap.codigo ";
				$filtro++;
			}

			if($codigo != ""){

				if($filtro > 0)
					$consulta .= " AND pp.codigo = '". $codigo ."' ";
				else
					$consulta .= " p.codigo = '". $codigo ."' ";

				$filtro++;
			}

			if($id_tipo_oper != 0){

				if($filtro > 0)
					$consulta .= " AND id_tipo_oper = ". $id_tipo_oper ." ";
				else
					$consulta .= " id_tipo_oper = ". $id_tipo_oper ." ";

				$filtro++;
			}

			if($id_tipo_prop != 0){
				if($filtro > 0)
					$consulta .= " AND id_tipo_prop = ". $id_tipo_prop ." ";
				else
					$consulta .= " id_tipo_prop = ". $id_tipo_prop ." ";

				$filtro++; 
			}

			if($id_barrio != 0){
				if($filtro > 0)
					$consulta .= " AND id_barrio = ". $id_barrio ." ";
				else
					$consulta .= " id_barrio = ". $id_barrio ." ";

				$filtro++;
			}


			if($calle != ""){
				if($filtro > 0)
					$consulta .= " AND calle like '%". $calle ."%' ";
				else
					$consulta .= " calle like '%". $calle ."%' ";

				$filtro++;
			}

			if($ambientes != 0){
				if($filtro > 0)
					$consulta .= " AND ambiente = ". $ambientes ." ";
				else
					$consulta .= " ambiente = ". $ambientes ." ";

				$filtro++;
			}

			if($antiguedad != 0){
				if($filtro > 0)
					$consulta .= " AND antiguedad = ". $antiguedad ." ";
				else
					$consulta .= " antiguedad = ". $antiguedad ." ";

				$filtro++;
			}

			if($inmueble != 0){
				if($filtro > 0)
					$consulta .= " AND estado_inmueble = ". $inmueble ." ";
				else
					$consulta .= " estado_inmueble = ". $inmueble ." ";

				$filtro++;
			}

			if($edifico != 0){
				if($filtro > 0)
					$consulta .= " AND estado_propiedad = ". $edifico ." ";
				else
					$consulta .= " estado_propiedad = ". $edifico ." ";

				$filtro++;
			}

			if($superficie_a != 0 AND $superficie_b != 0){
				if($filtro > 0)
					$consulta .= " AND superficie BETWEEN ". $superficie_a ." AND ". $superficie_b ." ";
				else
					$consulta .= " superficie BETWEEN ". $superficie_a ." AND ". $superficie_b ." ";

				$filtro++;
			}

			if($superficie_a != 0 AND $superficie_b == 0){
				if($filtro > 0)
					$consulta .= " AND superficie >= ". $superficie_a ." ";
				else
					$consulta .= " superficie >= ". $superficie_a ." ";

				$filtro++;
			}

			if($superficie_a == 0 AND $superficie_b != 0){
				if($filtro > 0)
					$consulta .= " AND superficie <= ". $superficie_b ." ";
				else
					$consulta .= " superficie <= ". $superficie_b ." ";

				$filtro++;
			}

			if($moneda != 0){
				if($filtro > 0)
					$consulta .= " AND id_moneda = ". $moneda ." ";
				else
					$consulta .= " id_moneda = ". $moneda ." ";

				$filtro++;
			}

			if($precio_a != 0 AND $precio_b != 0){

				$precio_a = str_replace(",", "", $precio_a);
				$precio_b = str_replace(",", "", $precio_b);
				if($filtro > 0)
					$consulta .= " AND precio BETWEEN ". $precio_a ." AND ". $precio_b ." ";
				else
					$consulta .= " precio BETWEEN ". $precio_a ." AND ". $precio_b ." ";

				$filtro++;
			}

			if($precio_a != 0 AND $precio_b == 0){

				$precio_a = str_replace(",", "", $precio_a);

				if($filtro > 0)
					$consulta .= " AND precio >= ". $precio_a ." ";
				else
					$consulta .= " precio >= ". $precio_a ." ";

				$filtro++;
			}

			if($precio_a == 0 AND $precio_b != 0){
				
				$precio_b = str_replace(",", "", $precio_b);

				if($filtro > 0)
					$consulta .= " AND precio <= ". $precio_b ." ";
				else
					$consulta .= " precio <= ". $precio_b ." ";

				$filtro++;
			}

			if($superficie != 0){
				if($filtro > 0)
					$consulta .= " AND id_tipo_superficie = ". $superficie ." ";
				else
					$consulta .= " id_tipo_superficie = ". $superficie ." ";

				$filtro++;
			}

			if($tipo_usuario != 0){
				if($filtro > 0)
					$consulta .= " AND u.id_tipo_usuario = ". $tipo_usuario ." ";
				else
					$consulta .= " u.id_tipo_usuario = ". $tipo_usuario ." ";

				$filtro++;
			}/**/

			if($filtro > 0)
				$consulta .=  " AND estatus_general = ". $estatus_general ." AND p.estatus = ". $estatus ." GROUP BY p.codigo ORDER BY id_prop DESC LIMIT " . $offset . "," .$limit;
			else
				$consulta .=  " estatus_general = ". $estatus_general ." AND p.estatus = ". $estatus ." GROUP BY p.codigo ORDER BY id_prop DESC LIMIT " . $offset . "," .$limit;


			$consulta2 .= "WHERE ";

			$consulta_sql = $consulta1 . $consulta2 . $consulta;

			//echo $consulta_sql;

			try {
				$comando = $bd->prepare($consulta_sql);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}/**/
		} //FIN FUNCION 




		/**
		retorna 
		*/
		public static function obtener_propiedades_by_cat($bd, $cat){

			$consulta = "SELECT * FROM prop WHERE id_tipo_oper = ? AND estatus_general = 1 AND estatus = 1 ORDER BY RAND() DESC LIMIT 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($cat));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

			    if($row){
			    	$precio_final = "";
				    $listado = "";
				    $src = "prop/";

			        foreach ($row as $prop) {
			            $codigo = $prop["codigo"];

			            $hash_usuario = $prop["hash"];

			            $id_tipo_usuario = Usuarios::obtener_id_tipo_usuario($bd, $hash_usuario);
			            $logo = Usuarios::obtener_logo_path($bd, $hash_usuario);
			            $telefono = Usuarios::obtener_telefonos($bd, $hash_usuario);

			            if($id_tipo_usuario == 1){
			                $tipo_usuario = "Particular";
			                $usuario = "Particular";
			            }
			            else{
			                $usuario = Usuarios::obtener_nombre_inmobiliaria($bd, $hash_usuario);
			                $tipo_usuario = "Inmobiliaria";
			            }

			            $estatus = $prop["estatus"];

			            $tipo_prop = Prop::obtener_tipo_prop($bd, $codigo);
			            $tipo_oper = Prop::obtener_tipo_oper($bd, $codigo);
			            $barrio = Prop::obtener_barrio($bd, $codigo);
			            $calle = $prop["calle"];
			            $altura = $prop["altura"];

			            $foto = Prop::obtener_primera_foto_prop($bd, $codigo);

			            if($foto == ""){
			                $foto = "img/desing/no-pic.png";
			            }
			            else{
			                $foto = $src . $codigo ."/". $foto;
			            }

			            $id_moneda = $prop["id_moneda"];
			            $tipo_moneda = Prop::obtener_tipo_moneda_by_id($bd, $id_moneda);
			            $precio_unitario = $prop["precio"];

			            if($precio_unitario != ""){
			                $precio = number_format(doubleval($precio_unitario),"0",",",".");

			                $precio_final =  $tipo_moneda ." ". $precio;
			            }

			            $fecha = $prop["fecha"];

			            $direccion = $tipo_oper . " de " . $tipo_prop . " en " . $barrio . ", " . $calle ." ". $altura;

			            $listado .= ' <div class="col-12 col-sm-6 col-lg-4 portfolio-item my-2" title="'.$tipo_oper.'">';
			                $listado .= '<div class="card h-100">';
			                    $listado .= '<a class="text-center pull-center " href="propiedad-'. $codigo .'"><img class="text-center pull-center card-img-top-dest" src="'. $foto .'" alt="'. $codigo .'">';
			                    $listado .= '<div class="card-body border-top">';
			                        $listado .= '<p class="card-title-dest col-12">';
			                            $listado .= '<a href="propiedad-'. $codigo .'">'. $direccion .'</a>';
			                        $listado .= '</p>';
			                        $listado .= '<p class="card-text text-right h4">';
			                            $listado .= '<strong>'. $precio_final .'</strong>
			                                    </p>';
			                         $listado .= '<p class="text-muted text-center pull-center mt-4">
			                                        <strong>COD: '. $codigo .'</strong>';
			                                        
			                        //$listado .= '<br><em class="tiny">Publicado el '. $fecha .'</em>
			                                    $listado .= ' </p>
			                                </div>';
			                    $listado .= '<div class="card-footer" >
			                                    <div class="col-12 text-center pull-center" style="height: 50px;">';
			                                    if($logo != "")
			                            $listado .= '<img src="img/users/'. $logo .'" style="height: 50px;" alt="'.$usuario.'">';
			                                    else
			                            $listado .= '<strong>'. $usuario .'</strong>';          
			                       $listado .= '</div>';
			                    $listado .= '</div>
			                            </div>
			                        </div>';
			        }
			        echo $listado;
			    }
			    else{
			        echo '<div class="col-6 col-sm-4 col-lg-4 portfolio-item my-2" >
			            <div class="card h-100">
			                <a class="text-center" href="categoria.php">
			                    <img class="card-img-top" src="img/desing/no-pic.jpg" alt="sin resultados"></a>
			                <div class="card-body border-top"><p class="card-title col-12 text-center pull-center"></p>
			                    <div class="alert alert-info text-center pull-center" role="alert">
			                      <p class="mb-0">No existen publicaciones para esta categoria!</p>
			                    </div>
			                </div>

			                <div class="card-footer" >
			                    <div class="col-12 text-center pull-center" style="height: 50px;">
			                        <img src="" style="width: 50px;">
			                    </div>
			                </div>
			            </div>
			        </div>';
			    }

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_listado_propiedades_favoritos_by_hash($bd, $hash, $estatus_general, $estatus){

			$consulta = "SELECT * FROM favoritos f INNER JOIN prop p ON f.codigo = p.codigo WHERE f.hash = ? AND p.estatus_general = ? AND p.estatus = ? ORDER BY id_fav DESC";

			//echo $consulta;
			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash, $estatus_general, $estatus));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_listado_propiedades_by_estatus($bd, $estatus_general, $estatus){

			$filtro = 0;
			$consulta = "SELECT * FROM prop p iNNER JOIN usuario u ON p.hash = u.hash WHERE ";

			//echo $estatus_general;
			if($estatus_general >= 0){
				$consulta .= " p.estatus_general = " . $estatus_general;
				$filtro++;
			}

			if($estatus >= 0){
				if($filtro == 0)
					$consulta .= " p.estatus = " . $estatus;
				else
					$consulta .= " AND p.estatus = " . $estatus;

				$filtro++;	
			}

			$consulta .= " ORDER BY fecha DESC";

			//echo $consulta;
			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}/**/
		} //FIN FUNCION 		

		public static function obtener_max_id($bd, $campo, $tabla){

			$consulta = "SELECT MAX(".$campo.") AS ".$campo." FROM ". $tabla;

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row[$campo];								
				}
				else{
					return 0;
				}

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 

		}

		//obtener el sql para la carga masiva de datos
		public static function obtener_sql_masivo($data,$id){

			$sql="";

				for($j=0; $j<count($data); $j++){
					//saco el valor de cada elemento
					$dato = $data[$j];

					if($sql!="")
						$sql .= ",";

					$sql .= "(" . $dato . ",'" . $id . "')";
				}

			return $sql;
		}	

		//obtener el sql para la carga masiva de datos
		public static function obtener_sql_m($data,$id){

			$sql="";

				for($j=0; $j<count($data); $j++){
					//saco el valor de cada elemento
					$dato = $data[$j];

						if($dato == "")
							$sql .= "'',";
						else
							$sql .= "'" . $dato . "',";

					//echo "<\br>";
				}
				$sql .= "'" . $id . "')";

			return $sql;
		}	

}
