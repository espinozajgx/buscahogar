<?php
require_once("propiedad_data.php");
require_once('../../bin/connection.php');
require_once('../usuario/usuarios_data.php');
require_once('../../mail/mailer.php');
require_once("../../plugin/facebook/autoload.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if(isset($_POST["accion"])){
		$bd = connection::getInstance()->getDb();

		$accion = $_POST["accion"];

		//HASH USUARIO
		$hash = $_POST['hash'];

		// 1. CODIGO
		$codigo = $_POST["codigo"];

		// 2. ESTATUS
		$estatus = $_POST["estatus"];

		if($accion < 4 ){

			// 4 .ESTADO INMUEBLES
			$estado_inmueble = $_POST["est_inm"];

			// 5. ESTADO EDIFICIO
			$estado_propiedad = $_POST["est_edi"];

			// 6. DESCRIPCION
			$descripcion = $_POST["desc"];

			// 7. ANTIGUEDAD
			$antiguedad = $_POST["ant"];

			// 8. AMBIENTES
			$ambiente = $_POST["amb"];

			// 9. TIPO DE OPERACION
			$id_tipo_oper = $_POST["tipo_oper"];

			// 10. TIPO DE PROPIEDAD
			$id_tipo_prop = $_POST["tipo_prop"];

			// 11. TIPO MONEDA
			$id_moneda = $_POST["tipo_precio"];

			// 12. PRECIO
			$precio = $_POST["price"];

			// 13. TIPO SUPERFICIE
			$id_tipo_superficie = $_POST["sup"];

			// 14. SUPERFICIE
			$superficie = $_POST["surface"];

			// 15. ID BARRIO
			$id_barrio = $_POST["id_barrio"];
			if($id_barrio == 0)
				$id_barrio = 71;
			
			// 16. CALLE
			$calle = $_POST["street"];
			
			// 17. ALTURA
			$altura = $_POST["high"];

			// 18. BAÑO
			$bathroom = $_POST["bathroom"];
			
			// 19. ASCENSOR
			$elevator = $_POST["elevator"];

			//PALABRAS (KEYWORKS)
			$palabras = json_decode($_POST['palabras']);
			$tam = count($palabras);
			$keyworks = "";

			if($tam > 0){
				for($i = 0; $i < $tam; $i++){
					if($i == ($tam-1))
						$keyworks .= $palabras[$i];
					else
						$keyworks .= $palabras[$i] . ", ";
				}
			}

			//DETALLES (CARACTERISTICAS DE LA PROPIEDAD)
			$detalles = json_decode($_POST['detalles']);

			//FOTOS
			$fotos = json_decode($_POST['fotos']);

			$adicional = json_decode($_POST['adicional']);



		}

		if($accion==1){

			//AGREGAR 

			// 3. ESTATUS GENERAL
			$estatus_general = 1;

			//if(mkdir("../../../prop/".$codigo, 0700)){
				$id = Prop::agregar($bd, $codigo, $estatus, $estatus_general, $estado_inmueble, $estado_propiedad, $descripcion, $antiguedad, $ambiente,
									$id_tipo_oper, $id_tipo_prop, $id_moneda, $precio, $id_tipo_superficie, $superficie, $id_barrio, $calle, $altura,
									$bathroom, $elevator, $keyworks, $hash);/**/

				agregar_extra($bd, $codigo, $fotos, $detalles, $adicional);

				if($estatus == 1){
					//$res = "Registro exitoso";
					$res = publicar_facebook("Nueva Propiedad Cod #". $codigo, $codigo);
				}
				else{
					$res = "Registro exitoso";
				}

				$estado=1;

				enviar_notificacion($bd, $codigo, $id_barrio, $calle, $altura, $id_tipo_prop, $id_tipo_oper, $hash, 1);
		}
		else
		if($accion==2){
			
			//EDITAR
			$res = Prop::editar($bd, $estatus, $estado_inmueble, $estado_propiedad, $descripcion, $antiguedad, $ambiente, $id_tipo_oper, $id_tipo_prop, $id_moneda,
			 					$precio, $id_tipo_superficie, $superficie, $id_barrio, $calle, $altura, $bathroom, $elevator, $keyworks, $codigo, $hash);/**/

				Prop::eliminar_fotos_prop($bd, $codigo, $hash);

				Prop::eliminar_caract_prop($bd, $codigo, $hash);
				
				Prop::eliminar_adicionales_prop($bd, $codigo, $hash);

				agregar_extra($bd, $codigo, $fotos, $detalles, $adicional);
				
				$estado=1;
				//$res = $adicional;
		}
		else
		if($accion==3){
			
			//PUBLICAR SIMILAR 

			// 3. ESTATUS GENERAL
			$estatus_general = 1;

			// CODIGO ANTERIOR
			$cod_ant = $_POST["cod_ant"];

			$ruta_ant = "../../../prop/".$cod_ant."/";
			$ruta_nueva = "../../../prop/".$codigo."/";

			if(file_exists($ruta_nueva)){
				$tam = count($fotos);
				if($tam > 0){	
					for($i = 0; $i < $tam; $i++){
						if (!copy($ruta_ant.$fotos[$i][0], $ruta_nueva.$fotos[$i][0])) {
						    $i = $tam;
						}
					}/**/
				}
			}
			else{
				if(mkdir($ruta_nueva, 0700)){
					$tam = count($fotos);
					if($tam > 0){	
						for($i = 0; $i < $tam; $i++){
							if (!copy($ruta_ant.$fotos[$i][0], $ruta_nueva.$fotos[$i][0])) {
							    $i = $tam;
							}
						}/**/
					}
				}
			}

			$id = Prop::agregar($bd, $codigo, $estatus, $estatus_general, $estado_inmueble, $estado_propiedad, $descripcion, $antiguedad, $ambiente,
								$id_tipo_oper, $id_tipo_prop, $id_moneda, $precio, $id_tipo_superficie, $superficie, $id_barrio, $calle, $altura,
								$bathroom, $elevator, $keyworks, $hash);/**/

			agregar_extra($bd, $codigo, $fotos, $detalles, $adicional);

			if($estatus == 1)
				//$res = "Registro exitoso";
				$res = publicar_facebook("Nueva Propiedad Cod #". $codigo, $codigo);
			else
				$res = "Registro exitoso";

			$estado=1;

			enviar_notificacion($bd, $codigo, $id_barrio, $calle, $altura, $id_tipo_prop, $id_tipo_oper, $hash, 1);
		}
		else
		if($accion==4){
			
			//CAMBIAR ESTATUS
			$estado=1;
			//$res = $codigo;
			$res=Prop::cambiar_estatus($bd, $estatus,$codigo);
		}
		else
		if($accion==5){

			//ELIMINAR (SUSPENDER)
			$estado=1;
			$res=Prop::cambiar_estatus_general($bd, $estatus, $codigo);	

			if($estatus == 0){
				$hash = Prop::obtener_hash_usuario($bd, $codigo);
				$id_tipo_oper = Prop::obtener_id_tipo_oper($bd, $codigo);
				$id_tipo_prop = Prop::obtener_id_tipo_prop($bd, $codigo);
				$id_barrio = Prop::obtener_id_barrio($bd, $codigo);
				$calle =  Prop::obtener_calle($bd, $codigo);
                $altura = Prop::obtener_altura($bd, $codigo);

				enviar_notificacion($bd, $codigo, $id_barrio, $calle, $altura, $id_tipo_prop, $id_tipo_oper, $hash, 2);
			}

		}
		else
		if($accion==6){
			
			//AGREGAR FAVORITOS
			$estado=1;
			$res=Prop::agregar_fav($bd, $codigo, $hash, $estatus);
		}
		else
		if($accion==7){
			
			//ELIMINAR FAVORITOS
			$estado=1;
			$res= Prop::eliminar_fav($bd, $codigo, $hash);
		}

    	echo json_encode(array("estado"=>$estado,"mensaje"=>$res), JSON_FORCE_OBJECT);	
	
	}
	
}

	function enviar_notificacion($bd, $codigo, $id_barrio, $calle, $altura, $id_tipo_prop, $id_tipo_oper, $hash, $tipo){
		$id_tipo_usuario = Usuarios::obtener_id_tipo_usuario($bd, $hash);
		$correo_destinatario = Usuarios::obtener_email($bd, $hash);

		$destinatario = "";
        if($id_tipo_usuario == 1){
            $destinatario = Usuarios::obtener_nombre($bd, $hash);
            $destinatario .= " ". Usuarios::obtener_apellido($bd, $hash);
        }
        else{
            $destinatario = Usuarios::obtener_nombre_inmobiliaria($bd, $hash);
        }

		$barrio = Prop::obtener_barrio_by_id($bd, $id_barrio);
		$tipo_prop = Prop::obtener_tipo_prop_by_id($bd, $id_tipo_prop);
		$tipo_oper = Prop::obtener_tipo_oper_by_id($bd, $id_tipo_oper);

		$direccion = $barrio . ", " . $calle . " " . $altura;
		$titulo = $tipo_oper . " de " . $tipo_prop . " en " . $direccion; 

		if($tipo == 1){
			$res =  Mailer::correo_notificacion_nueva_publicacion($bd, $destinatario, $correo_destinatario, $codigo, $titulo);
		}
		else{
			$res =  Mailer::correo_notificacion_publicacion_suspendida($bd, $destinatario, $correo_destinatario, $codigo, $titulo);
		}
	}

	function agregar_extra($bd, $codigo, $fotos, $detalles, $adicional){
		
		$tam = count($fotos);
		if($tam > 0){
			$fotos_sql = "";
			for($i = 0; $i < $tam; $i++){
				
				if($fotos_sql != "")
					$fotos_sql .= ",";

				    $cadena = str_replace(' ', '', $fotos[$i]);
    				$cadena = limpiarString($cadena);

				$fotos_sql .= "(" . Prop::obtener_sql_m($cadena,$codigo);
			
			}/**/
			Prop::agregar_fotos_masivo($bd,$fotos_sql);
		}

		$tam = count($detalles);
		if($tam > 0){
			$detalles_sql = Prop::obtener_sql_masivo($detalles,$codigo);
			Prop::agregar_caracteristicas_masivo($bd,$detalles_sql);
		}

		$tam = count($adicional);
		if($tam > 0){
			$adicional_sql = Prop::obtener_sql_masivo($adicional,$codigo);
			Prop::agregar_adicional_masivo($bd,$adicional_sql);
		}
	}

	function limpiarString($texto){
	    $textoLimpio = preg_replace('([^A-Za-z0-9.])', '', $texto);                            
	    return $textoLimpio;
	}

	function publicar_facebook($mensaje, $codigo){

		//$accessToken = 'EAAFZAO9WoDvoBANS29zbYSRZBGwn85HH8JZBw3ko9rVBZAJmDwlAfZCZAZCWziCU2a5UBMnG6BENKRQ57QP8qZCHlY5TRRDUnFj0BDGxZBKLXxeOcD2ZCMJ9VglgAB33jVhhGjJEIJ8IXqdS4OA3O3JcKkf03rAWOL8232nc0UnzAp62lL9cTZAWjhf0V0wU7gPYsMZD';

		$accessToken = 'EAAC9rDusQDUBAPKZA7zofmaiZAjLXS2htoRHEMZAYim72bICpnGBZAgGNmP3AiczLhUjcaZCVcZC0ZC5PfWP7d1xNZCczpJGQ7VgUPKmZCwHUqUUpHgvaIqegPsleVVBlcV7UusO9c0JN4UYkypL2CVZBQlsRaL7OWlxHsYuuw29rZBSLwLYY64SfYdnEsMroUTyIEZD';

		$fb = new \Facebook\Facebook([
		  'app_id' => '379588499214074',
		  'app_secret' => '7e0f8684ffc591aff58029ed4147524d',
		  'default_graph_version' => 'v3.0',
		  'default_access_token' => $accessToken, // optional
		]);

		try {
		  $response = $fb->post(
		    '/1510323085942182/feed',
		    array (
		      'message' => $mensaje,
		      'link' => 'http://buscahogar.com.ar/propiedad-'.$codigo
		    ));
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  //echo 'Graph returned an error: ' . $e->getMessage();
			return -1;		 // exit;
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  //echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  //exit;
			return -1;
		}
	}/**/
		

?>