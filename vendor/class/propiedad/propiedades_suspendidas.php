<?php

$codigo = "";
$id_tipo_oper = 0;
$id_tipo_prop = 0;
$precio = "";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	require_once("propiedad_data.php");
	require_once('../../bin/connection.php');
    $bd = connection::getInstance()->getDb();

	if(isset($_POST["hash"])){
		$codigo = $_POST["codigo"];
		$hash = $_POST["hash"];
		$id_tipo_oper = $_POST["id_tipo_oper"];
		$id_tipo_prop = $_POST["id_tipo_prop"];

	}
}
	//ESTATUS GENERAL
	//SUSPENDIDOS
	$estatus_general = 0;

	$datos = Prop::obtener_listado_propiedades_by_hash_abd_filtros($bd, $hash, $codigo, $id_tipo_oper, $id_tipo_prop, $estatus_general, 0 ,100);
	$listado = "";
	$src = "prop/";
    $precio = "";

	if($datos){

		foreach ($datos as $prop) {
			$codigo = $prop["codigo"];
			$estatus = 0;

				$estilo_div = "noactive";
				$estilo_badge = "secondary";
				$estilo_btn = "noactivea";
			

			$barrio = Prop::obtener_barrio($bd, $codigo);
			$tipo_oper = Prop::obtener_tipo_oper($bd, $codigo);
			$tipo_prop = Prop::obtener_tipo_prop($bd, $codigo);

			$foto = Prop::obtener_primera_foto_prop($bd, $codigo);
			
			if($foto == ""){
				$foto = "img/desing/no-pic.jpg";
			}
			else{
				$foto = $src . $codigo ."/". $foto;
			}

			$tipo_moneda = Prop::obtener_tipo_moneda_by_id($bd, $prop["id_moneda"]);
			$tipo_superficie = Prop::obtener_tipo_superficie_by_id($bd, $prop["id_tipo_superficie"]);

			$direccion = $tipo_prop . " en " . $barrio . ", " . $prop["calle"] ." ". $prop["altura"];

			$precio_unitario = Prop::obtener_precio($bd, $codigo);
            if($precio_unitario != ""){
                $precio = number_format(doubleval($precio_unitario),"0",",",".");

                $precio_final =  $tipo_moneda ." ". $precio;
            }

			$listado .= '<!-- propiedad -->';
			$listado .= '<div id="'. $codigo .'" class="row py-2 px-2 shadow-sm '. $estilo_btn .'">';
			   $listado .= '<div class="col-md-6 text-center pull-center">
						      <a class="btn-link text-center pull-center" href="propiedad-'. $codigo .'" >
						        <img class="img-fluid rounded mb-3 mb-md-0 img-panel '. $estilo_btn .'" src="'.$foto.'" alt="'. $codigo .'" title="'. $codigo .'">
						      </a>
						    </div>
						    
						    <div class="col-12 col-sm-12 col-md-6 py-2 cuerpo-panel">
		            			<div class="p-1 col-md-12 text-center pb-2">';
		        		$listado .= '<strong class="name_prop">'. $direccion .'</strong>
		        				</div>';

		        	$listado .= '<div class="p-1 col-md-12 text-center">';			
		        	$listado .= '<small><strong>'. $prop["codigo"] .'</strong></small> <span class="badge badge-'. $estilo_badge .' px-2 py-2">'. $tipo_oper .'</span>
		        				</div>';

		        	$listado .= '<div class="p-2 col-md-12 text-center">';		
			        	$listado .= '<small class="py-2 px-2"><strong>Ambientes:</strong> '. $prop["ambiente"] .'</small>';		
			        	$listado .= '<small class="py-2 px-2"><strong>Superficie:</strong> '. $prop["superficie"] . "" . $tipo_superficie . '</small>';		
			        	$listado .= '<small class="py-2 px-2"><strong>Precio:</strong> '. $precio_final .'</small>
		        					<hr>
		        				</div>';	

		        	$listado .= '<div class="p-1 col-md-12">
					                <div class="row">
					                    <div class="col-8">';
								$listado .= '<small><em> Publicado 2018-04-28 </em></small>
										</div>
		                    			<div class="col-4 pull-right text-right">
		                        			<div id="'. $prop["codigo"] .'_toogle" cod="'. $prop["codigo"] .'" class="button_on toggle-haractive pr0 '. $estilo_btn .'">';
			                        $listado .= '<i class="fa fa-toggle-on on" id="ontipos"  title="Ocultar"';
			                        			if($estatus == 0){ $listado .= 'style="display:none;"';} $listado .= '></i>';
			                        $listado .= '<i class="fa fa-toggle-off off" id="offtipos" title="Publicar"';
			                        			if($estatus == 1){ $listado .= 'style="display:none;"';} $listado .= '></i>';
		                        $listado .= '</div>
		                    			</div>
		                    		</div>
		                		</div>';

		            $listado .= '<div class="p-2 col-md-12 pull-right text-right">
					                <div class="btn-toolbar pull-right text-right" role="toolbar" aria-label="Toolbar with button groups">
					                    <div class="btn-group mr-2" role="group" aria-label="Second group">
					                        <a class="btn btn-sm btn-block btn-info btn-link shared '. $estilo_btn .'" href="#" data-href="http://www.buscahogar.com.ar/propiedad-'. $codigo .'" title="compartir" disabled><i class="fa fa-share-alt"></i></a>
					                    </div>
					                    <div class="btn-group mr-2" role="group" aria-label="Third group">
					                        <a class="btn btn-sm btn-info btn-link '. $estilo_btn .'" target="_Blank" href="propiedad-'. $codigo .'" title="ver publicacion" disabled><i class="fa fa-eye"></i></a>
					                    </div>
					                    <div class="btn-group mr-2" role="group" aria-label="Third group">
					                        <a class="btn btn-sm btn-info '. $estilo_btn .'" href="publicar-'. $codigo .'-3" title="publicar similar"><i class="fa fa-share"></i></a>
					                    </div>
					                    <div class="btn-group mr-2" role="group" aria-label="First group">
					                        <a class="btn btn-sm btn-block btn-info '. $estilo_btn .'" href="publicar-'. $codigo .'-2" title="editar" disabled><i class="fa fa-edit"></i></a>
					                    </div>
					                    <div class="btn-group" role="group" aria-label="Second group">
					                        <a class="btn btn-sm btn-block btn-danger eliminar '. $estilo_btn .'" cod="'. $codigo .'" data-toggle="modal" data-target="#modal_trash" href="#" title="eliminar" disabled><i class="fa fa-trash"></i></a>
					                    </div>

					                </div>
					            </div>
					        </div>
						</div>';
					$listado .= '<!-- /.propiedad -->';
					$listado .= '<hr>';
		}	
	}
	else{
        $listado = '<div class="col-12 "><div class="alert alert-info text-center pull-center" role="alert">
                      <p class="mb-0">No existen publicaciones para este criterio de busqueda!</p>
                    </div>';
	}
	
	echo $listado;
?>



