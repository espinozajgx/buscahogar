<?php
require_once("propiedad_data.php");
require_once('vendor/bin/connection.php');

    $bd = connection::getInstance()->getDb();

    if(isset($_SESSION["hash512"])){
        $hash  = $_SESSION["hash512"];
    }

    $datos = Prop::obtener_listado_propiedades_favoritos_by_hash($bd, $hash, 1 ,1);
    $listado = "";
    $precio = "";
    $src = "prop/";

    if($datos){

        foreach ($datos as $prop) {
            $codigo = $prop["codigo"];

            $hash_usuario = $prop["hash"];

            $id_tipo_usuario = Usuarios::obtener_id_tipo_usuario($bd, $hash_usuario);
            
            $telefono = Usuarios::obtener_telefonos($bd, $hash_usuario);

            if($id_tipo_usuario == 1){
                $usuario = Usuarios::obtener_nombre($bd, $hash_usuario);
                $usuario .= " ". Usuarios::obtener_apellido($bd, $hash_usuario);
                $tipo_usuario = "Particular";

            }
            else{
                $usuario = Usuarios::obtener_nombre_inmobiliaria($bd, $hash_usuario);
                $tipo_usuario = "Inmobiliaria";
                $logo = Usuarios::obtener_logo_path($bd, $hash_usuario);
            }

            $estatus = Prop::obtener_estatus($bd, $codigo);

            $barrio = Prop::obtener_barrio($bd, $codigo);
            $calle = Prop::obtener_calle($bd, $codigo);
            $altura = Prop::obtener_altura($bd, $codigo);

            $tipo_oper = Prop::obtener_tipo_oper($bd, $codigo);
            $tipo_prop = Prop::obtener_tipo_prop($bd, $codigo);

            //$tipo_oper = Prop::obtener_tipo_oper_by_id($bd, $$id_tipo_oper);
            //$tipo_prop = Prop::obtener_tipo_prop_by_id($bd, $id_tipo_prop);

            $foto = Prop::obtener_primera_foto_prop($bd, $codigo);

            if($foto == ""){
                $foto = "img/desing/no-pic.jpg";
            }
            else{
                $foto = $src . $codigo ."/". $foto;
            }

            $id_moneda = $prop["id_moneda"];
            $id_tipo_superficie = $prop["id_tipo_superficie"];

            $tipo_moneda = Prop::obtener_tipo_moneda_by_id($bd, $id_moneda);
            $tipo_superficie = Prop::obtener_tipo_superficie_by_id($bd, $id_tipo_superficie);

            $ambiente = Prop::obtener_ambiente($bd, $codigo);
            $superficie = $prop["superficie"];
            
            $precio_unitario = Prop::obtener_precio($bd, $codigo);
            if($precio_unitario != ""){
                $precio = number_format(doubleval($precio_unitario),"0",",",".");

                $precio_final =  $tipo_moneda ." ". $precio;
            }

            $estado_inmueble = Prop::obtener_estado_inmueble($bd, $codigo);
            $estado_prop = Prop::obtener_estado_propiedad($bd, $codigo);

            $direccion = $tipo_prop . " en " . $barrio . ", " . $calle ." ". $altura;

            $listado .= '<!-- propiedad -->';
            $listado .= '<div id="'. $codigo .'" class="row shadow-sm py-4 my-3">
                            <div class="col-md-4 img-fav">
                              <a target="_Blank" href="propiedad-'. $codigo .'" >
                                <img class="img-fluid rounded mb-3 mb-md-0 shadow-sm" src="'. $foto .'" alt="" >
                              </a>
                            </div>
                            <div class="col-md-5">
                                <div class="p-1 col-md-12 text-center my-2">';
                        $listado .= '<a target="_Blank" href="propiedad-'.$codigo.'"><strong class="name_prop text-info titulo_prop">'. $direccion .'</strong></a>
                                </div>
                                <div class="p-1 col-md-12 text-center">';
                        $listado .= '<small><strong class="name_prop">COD: '. $codigo .'</strong></small> <span class="badge badge-info px-2 py-2">'. $tipo_oper .'</span>
                                <input type="hidden" class="codigo_prop" value="'. $codigo .'">
                                </div>';
                    $listado .= '<div class="p-1 col-md-12 text-center">';
                        $listado .= '<small class="py-2 px-2"><strong>Ambientes:</strong> '. $ambiente .'</small>';     
                        $listado .= '<small class="py-2 px-2"><strong>Superficie:</strong> '. $superficie . "" . $tipo_superficie . '</small>';     
                        $listado .= '<small class="py-2 px-2"><strong>Precio:</strong> '. $precio_final .'</small>';
                    $listado .= '</div>';
                    $listado .= '<div class="p-1 col-md-12 text-center">';
                        $listado .= '<small class="py-2 px-2"><strong>Inmueble:</strong> '. $estado_inmueble .'</small>';     
                        $listado .= '<small class="py-2 px-2"><strong>Edificio:</strong> '. $estado_prop . '</small>';     
                        $listado .= '<hr>';
                    $listado .= '</div>';

                    $listado .= '<div class="p-2 col-md-12 pull-right text-right mb-2">
                                    <div class="btn-toolbar pull-right text-right" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2" role="group" aria-label="compartir">
                                            <a class="btn btn-sm btn-block btn-info shared" href="#" data-href="http://www.buscahogar.com.ar/propiedad-'. $codigo .'" title="Compartir"><i class="fa fa-share-alt"></i></a>
                                        </div>
                                        <div class="btn-group mr-2" role="group" aria-label="publicacion">
                                            <a class="btn btn-sm btn-info" target="_Blank" href="propiedad-'. $codigo .'" title="Ver publicacion"><i class="fa fa-eye"></i></a>
                                        </div>
                                        <div class="btn-group mr-2" role="group" aria-label="contactar">
                                            <a class="btn btn-sm btn-block btn-info contactar" href="#" cod='. $codigo .' title="Contactar"><i class="fa fa-envelope"></i></a>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="eliminar">
                                            <a class="btn btn-sm btn-block btn-danger eliminar" href="#" cod='. $codigo .'  data-toggle="modal" data-target="#modal_trash" title="Eliminar"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>';

                    $listado .= '
                            <div class="col-md-3">';
                            if($id_tipo_usuario == 2){
                    $listado .= '<div class="col-sm-12 col-md-12 col-lg-12 text-center pt-1 pb-1">
                                    <img src="img/users/'. $logo .'" class="imgLogo1" style="max-height: 100px;">
                                </div>';
                            }
                    $listado .= '<a class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                      <strong class="mb-1 usuario_prop">'. $usuario .' </strong>
                                      <small class="text-muted"> '. $tipo_usuario .'</small>
                                      <input type="hidden" class="hash_usuario" value="'. $hash .'">
                                    </div>
                                    <!--div class="col-sm-12 col-md-12 col-lg-12 text-center ">
                                        <img class="img-fluid rounded" src="img/500x300.png" alt=""> 
                                    </div-->
                                </a>
                                <ul class="list-group">
                                  <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Telefono
                                    <span class="badge badge-light">'. $telefono .'</span>
                                  </li>
                                </ul>
                            </div>
                        </div>
                    <!-- /.propiedad -->';



        }

        echo $listado;

    }
    else{
        echo '<div class="alert alert-info" role="alert">
              <h4 class="alert-heading">No ha seleccionado ningún favorito!</h4>
              <hr>
              <p class="mb-0">Las propiedad que agregue a favoritos apareceran en esta sección!</p>
            </div>';
    }


            




        

