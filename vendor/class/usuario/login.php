<?php 
require_once('../../bin/connection.php');
require_once("usuarios_data.php");

$bd = connection::getInstance()->getDb();

session_start();

//Página para login a la plataforma
	if (!isset($_POST["email"]) || ($_POST["pass"] == "")) {
		$estado= 0;
		$res = "Email Invalido";
	} else {
		$email = $_POST["email"];
		$contrasena = $_POST["pass"];

		//Comprobar usuario
		$row = Usuarios::comprobar_sesion($bd, $email, $contrasena);
		//print_r($row);
		if ($row)
		{	
			if($row["estatus"] == 0){
				$estado= 0;
				$res = "Usuario Inactivo <br><button type='button' class='btn btn-link reenviar'>Reenviar enlace de activacion</button>";
			}
			else
			if ($row["estatus"] == 1) {
				$_SESSION["hash512"] = $row["hash"];
				$_SESSION["id_tipo_usuario"] = $row["id_tipo_usuario"];
				$id_tipo_usuario = $row["id_tipo_usuario"];

				if($id_tipo_usuario == 1){
					$_SESSION["nombre"] = Usuarios::obtener_nombre($bd, $row["hash"]);
				}
				else{
					$_SESSION["nombre"] = Usuarios::obtener_nombre_inmobiliaria($bd, $row["hash"]);
				}

				if(isset($_POST["remember"])){
					if($_POST["remember"]=="1"){
						$_SESSION["remember"] = $row["hash"];
						//setcookie('recuerdame', $row["hash"], time() + 365 * 24 * 60 * 60); 	
					}
					else{
						//setcookie('recuerdame', false, 0); 	
					}
				}	
				else{
					//setcookie('recuerdame', false, 0); 		
				}	
			
				$estado= 1;
				$res = "Ingreso Existoso";
			} 
			else
			if($row["estatus"] == 3) {
				$estado= 0;
				$res = "Usuario Inhabilitado";
			}/**/
		} else {
			$estado= 0;
			$res = "Combinacion Email / Password incorrectos";
		}/**/
		echo json_encode(array("estado"=>$estado, "mensaje"=>$res), JSON_FORCE_OBJECT);	
	}
?>