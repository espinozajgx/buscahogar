<?php
require_once("usuarios_data.php");
require_once('../../bin/connection.php');
require_once('../../mail/mailer.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if(isset($_POST["accion"])){
		$bd = connection::getInstance()->getDb();

		$accion = $_POST["accion"];

		if($accion==1){
			//AGREGAR 

			$email = $_POST["email"];
			$mail = Usuarios::validar_email($bd, $email);
			if($email == $mail){
				$estado= 0;
				$res = "Email registrado";
			}
			else{
				$tipo  = $_POST["tipo"];
				$password = $_POST["pass"];
				$hash = password_hash($password,PASSWORD_DEFAULT) . substr(sha1(time()),0,6);
				$estatus = 0;

				$usuario = json_decode($_POST["usuario"]);
				$estado=1;

				$id_usuario = Usuarios::agregar($bd, $email, $password, $hash, $estatus, $tipo);

				if($tipo == 1){
					$nombre = $usuario[0];
					$apellido = $usuario[1];
					$usuario = $nombre ." ". $apellido;
					$res = Usuarios::agregar_particular($bd, $nombre, $apellido, $hash);
				}
				else{
					$nombre = $usuario[0];
					$usuario = $nombre;
					$res = Usuarios::agregar_inmobiliaria($bd, $nombre, $hash);
				}

				if($res != ""){
					$res =  Mailer::correo_registro_usuario($bd, $usuario, $email, $password, $id_usuario);
					$estado=1;
					//$res = "Registro exitoso";
				}/**/
			}

		}
		else
		if($accion==2){
			
			//EDITAR
			$email = $_POST["email"];
			$telefono = $_POST["phone"];
			$direccion = $_POST["direccion"];
			$hash = $_POST["hash"];
			$logo = $_POST["logo"];
			$localidad = $_POST["localidad"];

			$nombre ="";

			$cond_iva = $_POST["iva"];

			$usuario = json_decode($_POST["usuario"]);

			//print_r($usuario);

			$tipo = $usuario[0];

		 	$old =  Usuarios::obtener_email($bd, $hash);

		 	if($old != $email){
		 		
		 		$res = Usuarios::validar_email($bd, $email);
		 		if($res == null){
		 			$estado=1;
		 			$res= Usuarios::editar($bd, $logo, $email, $telefono, $direccion, $cond_iva, $localidad ,$hash);
		 		}
		 		else{
		 			$estado=0;
		 			$res = "Email registrado!";
		 		}
		 	}
		 	else{
				$estado=1;
				$res= Usuarios::editar($bd, $logo, $email, $telefono, $direccion, $cond_iva, $localidad ,$hash);
		 	}/**/

		 	if($res){
			 	if($tipo == 1){
					$nombre = $usuario[1];
					$apellido = $usuario[2];
					$identificacion = $usuario[3];
					$tipo_doc = $usuario[4];

					$res = Usuarios::editar_particular($bd, $nombre, $apellido, $identificacion, $tipo_doc, $hash);
				}
				else{
					$nombre = $usuario[1];
					$rs = $usuario[2];
					$cuit = $usuario[3];
					$res = Usuarios::editar_inmobiliaria($bd, $nombre, $rs, $cuit, $hash);

				}/**/

				if($res){
					$res = "Perfil Actualizado!";
					session_start();
					$_SESSION["nombre"] = $nombre;
				}
			}

		}
		else
		if($accion==3){
			//CAMBIAR CONTRASEÑA
			$hash = $_POST["hash"];
			$password = $_POST["password"];
			$email = $_POST["email"];

				$estado= 1;
				$res=Usuarios::cambiar_contraseña($bd, $email, $password, $hash);	

		}
		else
		if($accion==4){
			
			//CAMBIAR ESTADO
			$hash = $_POST["hash"];
			$estatus = $_POST["estatus"];
			$estado= 1;
			$res=Usuarios::cambiar_estatus_actividad($bd, $id, $estado);
		}
		else
		if($accion==5){
			//RECUPERAR CONTRASEÑA

			$email = $_POST["email"];
			$data = Usuarios::recuperar_pass_by_email($bd,$email);
			$tam = count($data);

			if($tam > 0){
				$estado=1;
				$hash = $data["hash"];
				$password = $data["password"];
				$id_tipo_usuario = $data["id_tipo_usuario"];

	            if($id_tipo_usuario == 1){
	                $destinatario = Usuarios::obtener_nombre($bd, $hash);
	                $destinatario .= " ". Usuarios::obtener_apellido($bd, $hash);
	            }
	            else{
	                $destinatario = Usuarios::obtener_nombre_inmobiliaria($bd, $hash);
	            }

				//$res = $destinatario . " - " . $data;
				Mailer::correo_recuperar_contraseña($bd,$destinatario, $email, $password);
				$res = "Sus datos de acceso han sido enviado a su direcion de email";
			}
			else{
				$estado= 0;
				$res = "Email Invalido";
			}

		}
		else
		if($accion==6){
			
			//REENVIAR MENSAJE ACTIVACION
			$email = $_POST["email"];
			$data = Usuarios::recuperar_pass_by_email($bd,$email);
			$tam = count($data);

			if($tam > 0){
				$estado=1;
				$hash = $data["hash"];
				$password = $data["password"];
				$id_tipo_usuario = $data["id_tipo_usuario"];

	            if($id_tipo_usuario == 1){
	                $destinatario = Usuarios::obtener_nombre($bd, $hash);
	                $destinatario .= " ". Usuarios::obtener_apellido($bd, $hash);
	            }
	            else{
	                $destinatario = Usuarios::obtener_nombre_inmobiliaria($bd, $hash);
	            }

				Mailer::correo_registro_usuario($bd, $destinatario, $email, $password, $hash);
				$res = "Mensaje de activacion enviado";
			}
			else{
				$estado= 0;
				$res = "Email Invalido";
			}
		}

		echo json_encode(array("estado"=>$estado, "mensaje"=>$res), JSON_FORCE_OBJECT);	
    		
	}
	
}
	

?>