<?php
	class Usuarios{


		/*clase que contiene todas las funciones relacionadas a las usuarios*/
		function __construct(){

		}

		/**
		retorna 
		*/
		public static function obtener_id($bd, $hash){

			$consulta = "SELECT id_usuario FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_usuario"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_hash_by_id($bd, $id){

			$consulta = "SELECT hash FROM usuario WHERE id_usuario = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["hash"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_hash($bd, $email){

			$consulta = "SELECT hash FROM usuario WHERE email = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["hash"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_nombre($bd, $hash){

			$consulta = "SELECT nombre FROM particular WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_apellido($bd, $hash){

			$consulta = "SELECT apellido FROM particular WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["apellido"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_identificacion($bd, $hash){

			$consulta = "SELECT identificacion FROM particular WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["identificacion"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_pass($bd, $hash){

			$consulta = "SELECT password FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["password"];							
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_tipo_doc($bd, $hash){

			$consulta = "SELECT tipo_documento FROM particular WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["tipo_documento"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_nombre_inmobiliaria($bd, $hash){

			$consulta = "SELECT nombre FROM inmobiliaria WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_rs($bd, $hash){

			$consulta = "SELECT rs FROM inmobiliaria WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["rs"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_cuit($bd, $hash){

			$consulta = "SELECT cuit FROM inmobiliaria WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["cuit"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_telefonos($bd, $hash){

			$consulta = "SELECT telefonos FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["telefonos"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_cond_iva($bd, $hash){

			$consulta = "SELECT cond_iva FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["cond_iva"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_direccion($bd, $hash){

			$consulta = "SELECT direccion FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["direccion"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_password($bd, $hash){

			$consulta = "SELECT password FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["password"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION

		/**
		retorna 
		*/
		public static function obtener_logo_path($bd, $hash){

			$consulta = "SELECT logo FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["logo"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function comprobar_sesion($bd, $email, $password){

			$consulta = "SELECT * FROM usuario WHERE email = ? AND password = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($email, $password));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row;
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function recuperar_pass_by_email($bd, $email){

			$consulta = "SELECT password, hash, id_tipo_usuario FROM usuario WHERE email = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($email));
				$row = $comando->fetch(PDO::FETCH_ASSOC);
	            
	            if($row)            
					return $row;
				else
					return null;


			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function validar_email($bd, $email){

			$consulta = "SELECT email FROM usuario WHERE email = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($email));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["email"];							
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_email($bd, $hash){

			$consulta = "SELECT email FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["email"];							
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_id_tipo_usuario($bd, $hash){

			$consulta = "SELECT id_tipo_usuario FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_tipo_usuario"];						
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION */

		/**
		retorna 
		*/
		public static function obtener_localidad($bd, $hash){

			$consulta = "SELECT localidad FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["localidad"];						
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION */

		/**
		retorna 
		*/
		public static function obtener_estado($bd, $hash){

			$consulta = "SELECT estatus FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["estatus"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_tipo($bd, $hash){

			$consulta = "SELECT id_tipo_usuario FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_tipo_usuario"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function agregar($bd, $email, $password, $hash, $estatus, $id_tipo)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO usuario ( " .
				" email,".
				" password,".
				" hash,".
				" estatus,".
				" id_tipo_usuario)".
				" VALUES(?,?,?,?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($email, $password, $hash, $estatus, $id_tipo));
				
				if($resultado){
					return usuarios::obtener_max_id($bd,"id_usuario","usuario");		        	
				}

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 	
		}


		/**
		retorna 
		*/
		public static function editar($bd, $logo, $email, $telefonos, $direccion, $cond_iva, $localidad ,$hash){
			
			// Sentencia INSERT
			$consulta = "UPDATE usuario SET" .
				" logo = ?," .
				" email = ?," .
				" telefonos = ?," .
				" direccion = ?," .
				" localidad = ?" .
				" WHERE hash = ?";

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($logo, $email, $telefonos, $direccion, $localidad, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}

		/**
		retorna 
		*/
		public static function cambiar_contraseña($bd, $email, $password, $hash){
			
			// Sentencia INSERT
			$consulta = "UPDATE usuario SET password = ? WHERE hash = ? AND email = ?";

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($password, $hash, $email));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}

		/**
		retorna 
		*/
		public static function agregar_particular($bd, $nombre, $apellido, $hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO particular ( " .
				" nombre,".
				" apellido,"./**/
				" hash)".
				" VALUES(?,?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($nombre, $apellido, $hash));

				return $resultado;	        	

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 	
		}	

		/**
		retorna 
		*/
		public static function editar_particular($bd, $nombre, $apellido, $identificacion, $tipo_doc, $hash){
			
			// Sentencia INSERT
			$consulta = "UPDATE particular SET" .
				" nombre = ?," .
				" apellido = ?," .
				" identificacion = ?," .
				" tipo_documento = ?" .
				" WHERE hash = ?";

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($nombre, $apellido, $identificacion, $tipo_doc, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}



		/**
		retorna 
		*/
		public static function agregar_inmobiliaria($bd, $nombre, $hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO inmobiliaria ( " .
				" nombre," .
				" hash)".
				" VALUES(?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($nombre, $hash));

				return $resultado;	        	

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			} 	
		}

		/**
		retorna 
		*/
		public static function editar_inmobiliaria($bd, $nombre, $rs, $cuit, $hash){
			
			// Sentencia INSERT
			$consulta = "UPDATE inmobiliaria SET" .
				" nombre = ?," .
				" rs = ?," .
				" cuit = ?" .
				" WHERE hash = ?";

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($nombre, $rs, $cuit, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}	


		/**
		retorna 
		*/
		public static function eliminar_mensaje($bd, $id_mc)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM contacto_prop WHERE id_mc = ". $id_mc;
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}


		/**
		retorna 
		*/
		public static function cambiar_estatus($bd, $estatus, $hash){
			
			// Sentencia INSERT
			$consulta = "UPDATE usuario SET" .
				" estatus = " . $estatus . 
				" WHERE id_usuario = " . $hash;
		   
		   //echo $consulta;
		    try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($estatus, $hash));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}

		public static function obtener_max_id($bd, $campo, $tabla){

			$consulta = "SELECT MAX(".$campo.") AS ".$campo." FROM ". $tabla;

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row[$campo];								
				}
				else{
					return 0;
				}

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 

		}

		public static function obtener_cant_publicaciones_by_user($bd, $hash){

			$consulta = "SELECT count(id_prop) AS cant FROM prop WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(Array($hash));
	            $row = $comando->fetch(PDO::FETCH_ASSOC);

	            return $row["cant"];

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_lista_particulares($bd){

			$consulta = "SELECT u.id_usuario, up.identificacion, up.nombre, up.apellido, u.email, u.estatus, u.logo ,u.hash FROM usuario u INNER JOIN particular up ON u.hash = up.hash";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_lista_inmobiliarias($bd){

			$consulta = "SELECT u.id_usuario, ui.nombre, ui.cuit, u.email, u.estatus, u.logo ,u.hash FROM usuario u INNER JOIN inmobiliaria ui ON u.hash = ui.hash";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_lista_mensajes_contacto($bd, $hash){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT cp.id_mc, cp.nombre, cp.telefonos ,cp.mensaje AS msj ,DATE_FORMAT(cp.fecha, '%d/%m/%Y') AS fecha, cp.codigo FROM contacto_prop cp ".
			"INNER JOIN prop p ON cp.codigo = p.codigo WHERE p.hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		}

		public static function obtener_mensaje_contacto($bd, $id_mc){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT nombre, mensaje, fecha, codigo, DATE_FORMAT(fecha, '%d-%m-%Y %H:%i:%s') AS fecha FROM contacto_prop WHERE id_mc = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_mc));
	            $row = $comando->fetch(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_mensajes_contacto($bd, $id_mc){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT mcp.id_mc, mcp.nombre, DATE_FORMAT(mcp.fecha, '%d-%m-%Y') AS fecha, mcp.codigo FROM mensaje_contacto_prop mcp ".
			"INNER JOIN prop p ON mc.codigo = p.codigo WHERE p.hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_mc));
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

}
