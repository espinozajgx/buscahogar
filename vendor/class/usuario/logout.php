<?php 
session_start();
session_destroy();

$_SESSION["nombre"] = "";
$_SESSION["password"] = "";
$_SESSION["hash512"] = "";
$_SESSION["id_rol"] = "";

unset($_SESSION["nombre"]);
unset($_SESSION["password"]);
unset($_SESSION["hash"]);
unset($_SESSION["id_rol"]);
header("Location:../../../index.php");
?>