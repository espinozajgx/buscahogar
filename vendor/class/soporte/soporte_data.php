<?php
	class Soporte{


		/*clase que contiene todas las funciones relacionadas a las usuarios*/
		function __construct(){

		}


		/**
		retorna 
		*/
		public static function obtener_nombre_contacto_prop($bd, $id_cp){

			$consulta = "SELECT nombre FROM contacto_prop WHERE id_cp = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_cp));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_email_contacto_prop($bd, $id_cp){

			$consulta = "SELECT email FROM contacto_prop WHERE id_cp = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_cp));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["email"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_nombre_contacto_admin($bd, $id_ca){

			$consulta = "SELECT nombre FROM contacto_admin WHERE id_ca = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_ca));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["nombre"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 
		
		/**
		retorna 
		*/
		public static function obtener_email_contacto_admin($bd, $id_ca){

			$consulta = "SELECT email FROM contacto_admin WHERE id_ca = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_ca));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["email"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_direccion($bd, $hash){

			$consulta = "SELECT direccion FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["direccion"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_codigo_contacto_prop($bd, $id_cp){

			$consulta = "SELECT codigo FROM contacto_prop WHERE id_cp = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_cp));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["codigo"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION

		/**
		retorna 
		*/
		public static function obtener_logo_path($bd, $hash){

			$consulta = "SELECT logo FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["logo"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function recuperar_pass_by_email($bd, $email){

			$consulta = "SELECT nombre, apellido, password FROM usuario WHERE email = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($email));
				$row = $comando->fetch(PDO::FETCH_ASSOC);
	            
	            if($row)            
					return $row;
				else
					return null;


			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function validar_email($bd, $email){

			$consulta = "SELECT email FROM usuario WHERE email = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($email));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["email"];							
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_email($bd, $hash){

			$consulta = "SELECT email FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["email"];							
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_id_tipo_usuario($bd, $hash){

			$consulta = "SELECT id_tipo_usuario FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_tipo_usuario"];						
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION */

		/**
		retorna 
		*/
		public static function obtener_localidad($bd, $hash){

			$consulta = "SELECT localidad FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["localidad"];						
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION */

		/**
		retorna 
		*/
		public static function obtener_estado($bd, $hash){

			$consulta = "SELECT estatus FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["estatus"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_tipo($bd, $hash){

			$consulta = "SELECT id_tipo_usuario FROM usuario WHERE hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["id_tipo_usuario"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_preguntas_frecuentes($bd){

			$consulta = "SELECT * FROM preguntas";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				if($row){	
					$listado = '<div class="accordion" id="accordion" >';	
					$i = 0;                        
					foreach ($row as $preguntas) {

                        $listado .= '<div class="card">
			                            <div class="card-header" id="heading'.$i.'" data-toggle="collapse" data-target="#collapse'.$i.'" aria-expanded="true" aria-controls="collapse'.$i.'">
			                              <div class="mb-0">
			                                <a class="text-info">';
			                    $listado .= $preguntas["pregunta"];
			                   $listado .= '</a>
			                              </div>
			                            </div>
		                            	<div id="collapse'.$i.'" class="collapse" aria-labelledby="heading'.$i.'" data-parent="#accordion">
			                              	<div class="card-body">';
			                    $listado .= $preguntas["respuesta"];
			                 	$listado .= '</div>
			                            </div>
			                        </div>';
					$i++;
					}
					$listado .= '</div>';				
				}
				return $listado;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function agregar_mensaje_contacto_prop($bd, $id_cp, $mensaje, $hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO mensaje_contacto_prop (id_cp , mensaje , hash) VALUES (?,?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($id_cp, $mensaje, $hash));
			
				return $resultado;	        	

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 	
		}

		/**
		retorna 
		*/
		public static function agregar_contacto_prop($bd, $nombre, $email ,$telefono, $mensaje, $codigo)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO contacto_prop (nombre, email, telefonos, mensaje, codigo) VALUES (?,?,?,?,?)";
			//echo $consulta;
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($nombre, $email ,$telefono, $mensaje, $codigo));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			} 	/**/
		}

		/**
		retorna 
		*/
		public static function agregar_contacto_admin($bd, $nombre, $email ,$telefono, $mensaje)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO contacto_admin (nombre, email, telefonos, mensaje) VALUES (?,?,?,?)";
			//echo $consulta;
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($nombre, $email ,$telefono, $mensaje));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			} 	/**/
		}

		/**
		retorna 
		*/
		public static function agregar_soporte($bd, $titulo, $msj, $hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO soporte (titulo, descripcion, hash, id_estatus) VALUES (?,?,?,1)";
			//echo $consulta;
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($titulo, $msj, $hash));

				$id = Soporte::obtener_max_id($bd, "id_soporte", "soporte");
				return $id;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				echo $e;
				return $e;
			} 	/**/
		}

		/**
		retorna 
		*/
		public static function agregar_mensaje_soporte($bd, $id_soporte, $mensaje, $hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO mensajes_soporte (id_soporte , mensaje , hash) VALUES (?,?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($id_soporte, $mensaje, $hash));
				
				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 	
		}

		/**
		retorna 
		*/
		public static function agregar_mensaje_soporte_ca($bd, $id_ca, $mensaje, $hash)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO mensajes_soporte (id_ca , mensaje , hash) VALUES (?,?,?)";

		   try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($id_ca, $mensaje, $hash));
				
				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 	
		}

		/**
		retorna 
		*/
		public static function eliminar_mensaje_contacto($bd, $id_cp)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM contacto_prop WHERE id_cp = ". $id_cp;
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				return $resultado;


			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}

		/**
		retorna 
		*/
		public static function eliminar_mensaje_contacto_admin($bd, $id_ca)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM contacto_admin WHERE id_ca = ". $id_ca;
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				return $resultado;


			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}

		/**
		retorna 
		*/
		public static function eliminar_mensaje_soporte($bd, $id_soporte)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM soporte WHERE id_soporte = ?";
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($id_soporte));

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}


		/**
		retorna 
		*/
		public static function cambiar_estatus($bd, $estatus, $id_soporte){
			
			// Sentencia INSERT
			$consulta = "UPDATE soporte SET" .
				" id_estatus = ? " .
				" WHERE id_soporte = ?";
		   
		    try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($estatus, $id_soporte));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}

		public static function obtener_max_id($bd, $campo, $tabla){

			$consulta = "SELECT MAX(".$campo.") AS ".$campo." FROM ". $tabla;

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row[$campo];								
				}
				else{
					return 0;
				}

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 

		}

		public static function obtener_lista_particulares($bd){

			$consulta = "SELECT up.identificacion, up.nombre, up.apellido, u.email, u.estatus, u.logo ,u.hash FROM usuario u INNER JOIN particular up ON u.hash = up.hash";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_lista_inmobiliarias($bd){

			$consulta = "SELECT ui.nombre, ui.cuit, u.email, u.estatus, u.logo ,u.hash FROM usuario u INNER JOIN inmobiliaria ui ON u.hash = ui.hash";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_lista_mensajes_contacto($bd, $hash){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT cp.id_cp, cp.nombre, cp.telefonos ,cp.mensaje AS msj ,DATE_FORMAT(cp.fecha, '%d/%m/%Y') AS fecha, cp.codigo FROM contacto_prop cp INNER JOIN prop p ON cp.codigo = p.codigo WHERE p.hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		}

		public static function obtener_mensaje_contacto($bd, $id_cp){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT nombre, mensaje, codigo, DATE_FORMAT(fecha, '%d-%m-%Y %H:%i:%s') AS fecha, telefonos FROM contacto_prop WHERE id_cp = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_cp));
	            $row = $comando->fetch(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_lista_mensajes_chat($bd, $id_cp){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT mensaje, DATE_FORMAT(fecha, '%d-%m-%Y %H:%i:%s') AS fecha, hash FROM mensaje_contacto_prop WHERE id_cp = ? ORDER BY fecha ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_cp));
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}/**/

		}

		public static function obtener_lista_mensajes_soporte($bd, $hash){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT s.id_soporte, s.titulo, DATE_FORMAT(s.fecha, '%d/%m/%Y') AS fecha, es.nombre FROM soporte s INNER JOIN estatus_soporte es ON s.id_estatus = es.id_estatus WHERE s.hash = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($hash));
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		}

		public static function obtener_mensaje_soporte($bd, $id_soporte){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT hash, descripcion AS mensaje, DATE_FORMAT(fecha, '%d-%m-%Y %H:%i:%s') AS fecha FROM soporte WHERE id_soporte = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_soporte));
	            $row = $comando->fetch(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}

		}

		public static function obtener_lista_mensajes_chat_soporte($bd, $id_soporte){
			//,p.id_tipo_prop, p.id_tipo_oper, p.id_barrio, p.calle, p.altura 
			$consulta = "SELECT mensaje, DATE_FORMAT(fecha, '%d-%m-%Y %H:%i:%s') AS fecha, hash FROM mensajes_soporte WHERE id_soporte = ? ORDER BY fecha ASC";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($id_soporte));
	            $row = $comando->fetchAll(PDO::FETCH_ASSOC);

	            return $row;

			} catch (Exception $e) {
				echo $e;
				return false;
			}/**/

		}

}
