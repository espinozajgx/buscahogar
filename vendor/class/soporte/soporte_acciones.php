<?php
require_once("../usuario/usuarios_data.php");
require_once("../propiedad/propiedad_data.php");
require_once('../../bin/connection.php');
require_once('../../mail/mailer.php');
require_once("soporte_data.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if(isset($_POST["accion"])){
		$bd = connection::getInstance()->getDb();

		$accion = $_POST["accion"];

		if($accion==0){
			//ENVIAR CONTACTO PROPIEDAD 

			$nombre = $_POST["nombre"];
			$tlf = $_POST["tlf"];
			$email = $_POST["email"];
			$msj = $_POST["msj"];

			$hash = $_POST["hash"];
			$destinatario = $_POST["usuario"];

			$correo_destinatario = Usuarios::obtener_email($bd, $hash);
			$prop = $_POST["titulo"];
			$codigo = $_POST["codigo"];

			$estado=1;
			//$res = $correo_destinatario;
			Soporte::agregar_contacto_prop($bd, $nombre, $email ,$tlf, $msj, $codigo);
			$res = Mailer::correo_contacto_prop($bd, $destinatario, $correo_destinatario, $nombre, $tlf, $email, $msj, $prop, $codigo);
			$res = Mailer::correo_contacto_cliente($bd, $nombre, $email);

		}
		else
		if($accion==1){
			//MENSAJE CONTACTO PROPIEDAD RESPUESTA
			$id_cp  = $_POST["id"];
			$mensaje = $_POST["mensaje"];
			$hash = $_POST["hash"];

            $codigo =  Soporte::obtener_codigo_contacto_prop($bd, $id_cp);

            $tipo_prop = Prop::obtener_tipo_prop($bd, $codigo);
			$barrio = Prop::obtener_barrio($bd, $codigo);
            $calle =  Prop::obtener_calle($bd, $codigo);
            $altura = Prop::obtener_altura($bd, $codigo);

            $direccion = $barrio . ", " . $calle . " " . $altura;
            $titulo = $tipo_prop . " en " . $direccion; 

			$propietario = Usuarios::obtener_nombre($bd, $hash);
			$destinatario = Soporte::obtener_nombre_contacto_prop($bd, $id_cp);
			$email_destinatario = Soporte::obtener_email_contacto_prop($bd, $id_cp);

			$estado=1;
			$res = Soporte::agregar_mensaje_contacto_prop($bd, $id_cp, $mensaje, $hash);
			//$res = $mensaje;

			if($res != ""){
				Mailer::correo_respuesta_contacto_prop($bd, $destinatario, $email_destinatario, $mensaje, $propietario, $titulo);
				$res = "Registro exitoso";
			}/**/	
		}
		else
		if($accion==2){
			//MENSAJE SOPORTE
			$titulo = $_POST["titulo"];
			$msj = $_POST["msj"];
			$hash = $_POST["hash"];

			$estado=1;

			$destinatario = Usuarios::obtener_nombre($bd, $hash);
			$correo_destinatario = Usuarios::obtener_email($bd, $hash);
			$id_soporte = Soporte::agregar_soporte($bd, $titulo, $msj, $hash);

			Mailer::correo_nuevo_ticket_admin($bd, $destinatario, $id_soporte, $titulo, $msj);
			Mailer::correo_nuevo_ticket_Cliente($bd, $destinatario, $correo_destinatario, $id_soporte, $titulo, $msj);
			
		}
		else
		if($accion==3){
			//MENSAJE SOPORTE RESPUESTA
			$id_soporte  = $_POST["id"];
			$mensaje = $_POST["mensaje"];
			$hash = $_POST["hash"];

			//$propietario = Usuarios::obtener_nombre($bd, $hash);
			//$destinatario = Soporte::obtener_nombre_contacto_prop($bd, $id_cp);
			//$email_destinatario = Soporte::obtener_email_contacto_prop($bd, $id_cp);

			$estado=1;
			$res = Soporte::agregar_mensaje_soporte($bd, $id_soporte, $mensaje, $hash);
			//$res = $mensaje;


			if($res != ""){
				//Mailer::correo_respuesta_soporte($bd, $destinatario, $correo_destinatario, $msj, $nombre, $id_soporte)
			}/**/		
		}
		else
		if($accion==4){
			
			//MENSAJE DE CONTACTO ADMIN
			$nombre = $_POST["nombre"];
			$tlf = $_POST["tlf"];
			$email = $_POST["email"];
			$msj = $_POST["msj"];	

			$estado=1;
			//$res = $email;
			Soporte::agregar_contacto_admin($bd, $nombre, $email ,$tlf, $msj);
			
			$res = Mailer::correo_contacto_admin($bd, $nombre, $tlf, $email, $msj);
			$res = Mailer::correo_contacto_cliente($bd, $nombre, $email);
		}
		else
		if($accion==5){
			//ELIMINAR MENSAJE CONTACTO
			$id = $_POST["id"];
			//$codigo = $_POST["codigo"];
			$estado= 1;
			$res=Soporte::eliminar_mensaje_contacto($bd, $id);

		}
		else
		if($accion==6){
			
			//ELIMINAR MENSAJE SOPORTE
			$id = $_POST["id"];
			//$codigo = $_POST["codigo"];
			$estado= 1;
			$res=Soporte::eliminar_mensaje_soporte($bd, $id);
		}
		else
		if($accion==7){
			
			//ELIMINAR MENSAJE ADMIN
			$id = $_POST["id"];
			//$codigo = $_POST["codigo"];
			$estado= 1;
			$res=Soporte::eliminar_mensaje_contacto_admin($bd, $id);
		}
		else
		if($accion==8){
			
			//CAMBIAR ESTATUS
			$id = $_POST["id"];
			$estatus = $_POST["estado"];
			$estado= 1;
			$res=Soporte::cambiar_estatus($bd, $estatus, $id);
		}
		else
		if($accion==9){
			//MENSAJE CONTACTO ADMIN RESPUESTA
			$id_ca  = $_POST["id"];
			$mensaje = $_POST["mensaje"];
			$hash = $_POST["hash"];

			//$propietario = Usuarios::obtener_nombre($bd, $hash);
			$destinatario = Soporte::obtener_nombre_contacto_admin($bd, $id_ca);
			$email_destinatario = Soporte::obtener_email_contacto_admin($bd, $id_ca);

			$estado=1;
			$res = Soporte::agregar_mensaje_soporte_ca($bd, $id_ca, $mensaje, $hash);

			if($res != ""){
				Mailer::correo_respuesta_contacto_admin($bd, $destinatario, $email_destinatario, $mensaje);
			}/**/		
		}
		echo json_encode(array("estado"=>$estado, "mensaje"=>$res), JSON_FORCE_OBJECT);	
    		
	}
	
}
	

?>