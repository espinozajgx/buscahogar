<?php

    $datos = Soporte::obtener_lista_mensajes_contacto($bd, $hash);
    $listado = "";

    if($datos){
        $i = 1;
        foreach ($datos as $prop) {
            $id = $prop["id_cp"];
            $codigo = $prop["codigo"];

            $tipo_prop = Prop::obtener_tipo_prop($bd, $codigo);
            $barrio = Prop::obtener_barrio($bd, $codigo);
            $calle = Prop::obtener_calle($bd, $codigo);
            $altura = Prop::obtener_altura($bd, $codigo);

            $fecha = $prop["fecha"];
            $usuario = $prop["nombre"];
            $telefonos = $prop["telefonos"];
            //$msj = $prop["msj"];

            $direccion = $tipo_prop . " en " . $barrio . ", " . $calle ." ". $altura;

            $listado .= '<tr id="'.$id.'">
                          <td>'.$i.'</td>
                          <td class="titulo_prop">'. $direccion.'</td>
                          <td>'. $telefonos.'</td>
                          <td>
                           
                            <btn class="btn btn-sm btn-info chat_prop" cod="'.$id.'" title="ver mensaje" disabled><i class="fa fa-comment"></i></btn>
                            <a class="btn btn-sm btn-danger eliminar" cod="'.$id.'" data-toggle="modal" data-target="#modal_trash" href="#" title="eliminar" disabled><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>';
            $i++;
        }
        echo $listado;
    }
    /*else{
        echo '<div class="col-12">
                <div class="alert alert-info" role="alert">
                  <h4 class="alert-heading">No existen resultados para el criterio de busqueda!</h4>
                  <hr>
                  <p class="mb-0">Modifica los parametros e intenta nuevamente!</p>
                </div>
            </div>';
    }*/

?>
