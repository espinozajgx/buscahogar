<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once("soporte_data.php");
    require_once('../../bin/connection.php');
    $bd = connection::getInstance()->getDb();
    $hash = "";
    if(isset($_POST["hash"])){

        $hash = $_POST["hash"];

    }
}
    $datos = Soporte::obtener_lista_mensajes_soporte($bd, $hash);
    $listado = "";

    if($datos){
        $i = 1;
        foreach ($datos as $soporte) {
            $id = $soporte["id_soporte"];

            $fecha = $soporte["fecha"];
            $estatus = $soporte["nombre"];
            $titulo = $soporte["titulo"];

            $listado .= '<tr id="'.$id.'">
                          <td>'.$i.'</td>
                          <td class="titulo_prop">'. $titulo.'</td>
                          <td>'. $estatus.'</td>
                          <td>
                           
                            <btn class="btn btn-sm btn-info chat_prop" cod="'.$id.'" title="ver mensaje" disabled><i class="fa fa-comment"></i></btn>
                            <a class="btn btn-sm btn-danger eliminar" cod="'.$id.'" data-toggle="modal" data-target="#modal_trash" href="#" title="eliminar" disabled><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>';
            $i++;
        }
        echo $listado;
    }
    /*else{
        echo '<div class="col-12">
                <div class="alert alert-info" role="alert">
                  <h4 class="alert-heading">No existen resultados para el criterio de busqueda!</h4>
                  <hr>
                  <p class="mb-0">Modifica los parametros e intenta nuevamente!</p>
                </div>
            </div>';
    }*/

?>
