<?php
	class Utilidades{

		/*clase que contiene todas las funciones relacionadas a las actividades*/
		function __construct(){

		}

		/**
		retorna 
		*/
		public static function obtener_menu($active){

			$consulta = '<ul class="nav justify-content-center nav-pills nav-fill">
	          <li class="nav-item col-sm-6 col-lg-3">
	            <a class="nav-link';
	            if($active == 1) $consulta .= ' active';
	        $consulta .= '" href="favs"><i class="fa fa-star fa-fw"></i>  Favoritos</a>
	          </li>
	          <li class="nav-item col-sm-6 col-lg-3">
	            <a class="nav-link'; 
	            if($active == 2) $consulta .= ' active';
	        $consulta .= '" href="panel"><i class="fa fa-building fa-fw"></i> Mis anuncios</a>
	          </li>
	          <li class="nav-item col-sm-6 col-lg-3">
	            <a class="nav-link';
	            if($active == 3) $consulta .= ' active';
	        $consulta .= '" href="perfil"><i class="fa fa-user fa-fw"></i> Mi Perfil</a>
	          </li>
	          <li class="nav-item col-sm-6 col-lg-3">
	            <a class="nav-link';
	            if($active == 4) $consulta .= ' active';
	         $consulta .= '" href="mensajes"><i class="fa fa-comment fa-fw"></i> Mis Interesados</a>
	          </li>
	        </ul>';

	        echo $consulta;

		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_fotos_propiedad($bd, $idPropiedad){

			$consulta = "SELECT * FROM fotopropiedad WHERE idPropiedad = ?";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute(array($idPropiedad));
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row;								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function eliminar_fotos_propiedad($bd, $idPropiedad)
		{
		
			// Sentencia INSERT
		   	$consulta = "DELETE FROM fotopropiedad WHERE idPropiedad = ?";
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($idPropiedad));

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}/**/
		}


		/**
		retorna 
		*/
		public static function obtener_fotos_slider_principal($bd){

			$consulta = "SELECT * FROM slider_principal ORDER BY orden ASC";
    		$src = "img/slider/";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				$fotos = '<div class="carousel-inner" role="listbox">';
				$indicadores = '<ol class="carousel-indicators">';
				$i = 0;

				if($row){
					foreach ($row as $foto) {
						if($i == 0) $activa = "active";
						else $activa = "";

							$foto_path = $src . $foto["nombre"];
						
	                		$indicadores .= '<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'" class="'.$activa.'"></li>';

							$fotos .=  '<div class="carousel-item '.$activa.'" style="background-image: url('. $foto_path .')"></div>';
				            

		            	$i++;
					}
				}
				/*else{
						$foto_path = "img/desing/no-pic.png";
						
                		$indicadores .= '<li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>';

						$fotos .=  '<div class="carousel-item active">';
			            $fotos .=  '<a href="'. $foto_path .'" data-lightbox="roadtrip"><img class="img-fluid rounded" src="'.$foto_path .'" alt="'.$codigo.'" title ="'.$codigo.'"></a>';
			            $fotos .=  '</div>';
				}*/



				$indicadores .= '</ol>';
				$fotos .= '</div>';

				$datos = $indicadores . $fotos;
				return $datos;

			} catch (Exception $e) {
				echo $e;
				return false;

			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_filas($bd){

			$consulta = "SELECT filas FROM web_info";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);
		                        
				return $row["filas"];								
			

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 
		
		/**
		retorna 
		*/
		public static function obtener_terminos($bd){

			$consulta = "SELECT terminos FROM web_info WHERE id_wi = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["terminos"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_titulo($bd){

			$consulta = "SELECT titulo FROM web_info WHERE id_wi = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					echo $row["titulo"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_logo($bd){

			$consulta = "SELECT logo FROM web_info WHERE id_wi = 1";

			try {
				if($bd!=null){
					$comando = $bd->prepare($consulta);
					$comando->execute();
					$row = $comando->fetch(PDO::FETCH_ASSOC);

					if($row){		                        
						echo $row["logo"];								
					}
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_footer($bd){

			$consulta = "SELECT footer FROM web_info WHERE id_wi = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					echo $row["footer"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_meta($bd){

			$consulta = "SELECT meta FROM web_info WHERE id_wi = 1";

			try {
				if($bd!=null){
					$comando = $bd->prepare($consulta);
					$comando->execute();
					$row = $comando->fetch(PDO::FETCH_ASSOC);

					if($row){		                        
						echo $row["meta"];								
					}
				}
			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_facebook($bd){

			$consulta = "SELECT facebook FROM web_info WHERE id_wi = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["facebook"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_twitter($bd){

			$consulta = "SELECT twitter FROM web_info WHERE id_wi = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["twitter"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 

		/**
		retorna 
		*/
		public static function obtener_instagram($bd){

			$consulta = "SELECT instagram FROM web_info WHERE id_wi = 1";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["instagram"];								
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION 


		/**
		retorna 
		*/
		public static function obtener_cant_actividad($bd){

			$consulta = "SELECT count(idactividades) AS cant FROM actividades";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row["cant"];								
				}
				else{
					return 0;
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		} //FIN FUNCION /**/


		/**
		retorna 
		*/
		public static function agregar_actividad($bd, $actividad, $dificultad, $estado)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO actividades ( " .
				" actividad," .
				" dificultad,".
				" activa)".
				" VALUES(?,?,?)";

		   try {

				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($actividad, $dificultad, $estado));


				if($resultado){
					return Actividades::obtener_max_id($bd,"idactividades","actividades");		        	
				}

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 	
		}


		/**
		retorna 
		*/
		public static function editar_actividad($bd, $actividad, $dificultad, $estado, $id_actividad){
			
			// Sentencia INSERT
			$consulta = "UPDATE actividades SET" .
				" actividad = ?," .
				" dificultad = ?," .
				" activa = ?" .
				" WHERE idactividades = ?";

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($actividad, $dificultad, $estado, $id_actividad));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}	


		/**
		retorna 
		*/
		public static function eliminar_propiedad($bd, $idPropiedad)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM propiedad WHERE IdPropiedad = ". $idPropiedad;
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}/**/
		}


		/**
		retorna 
		*/
		public static function cambiar_estatus_actividad($bd, $id_actividad, $estado){
			
			// Sentencia INSERT
			$consulta = "UPDATE actividades SET" .
				" activa = ? " .
				" WHERE idactividades = ?";
		   
		    try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute(array($estado, $id_actividad));

				return $resultado;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}
		}


		/**
		retorna 
		*/
		public static function agregar_actividad_analoga_masivo($bd, $datos)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO actividades_analogas (idactividad_b,idactividad_a) VALUES " . $datos;
			
			try {
		        // Preparar la sentencia
		        $comando = $bd->prepare($consulta);
		        $resultado = $comando->execute();
		       
	        } catch (PDOException $e) {
	            // Aquí puedes clasificar el error dependiendo de la excepción
	            // para presentarlo en la respuesta Json
	            //echo $e;
	            return $e;
	        } /**/	
		}

		/**
		retorna 
		*/
		public static function eliminar_actividad_analoga($bd, $id_actividad)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM actividades_analogas WHERE idactividad_a = ". $id_actividad ." OR idactividad_b = ". $id_actividad;
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}/**/
		}


		/**
		retorna 
		*/
		public static function agregar_etiqueta_actividad_masivo($bd, $datos)
		{
			// Sentencia INSERT
			$consulta = "INSERT INTO etiqueta_actividad (idetiqueta,idactividades) VALUES " . $datos;

			try {
		        // Preparar la sentencia
		        $comando = $bd->prepare($consulta);
		        $resultado = $comando->execute();

	        } catch (PDOException $e) {
	            // Aquí puedes clasificar el error dependiendo de la excepción
	            // para presentarlo en la respuesta Json
	            //echo $e;
	            return $e;
	        } /**/	
		}

		/**
		retorna 
		*/
		public static function eliminar_etiqueta_actividad($bd, $id_actividad)
		{
			// Sentencia INSERT
		   	$consulta = "DELETE FROM etiqueta_actividad WHERE idactividades = ". $id_actividad;
		   	//echo $consulta;
		   	
		   	try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$resultado = $comando->execute();

				if($resultado){
					return 1;       	
				}
				return 0;

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			}/**/
		}


		/******************************************************************************************************/
		/******************************************************************************************************/

		public static function obtener_max_id($bd, $campo, $tabla){

			$consulta = "SELECT MAX(".$campo.") AS ".$campo." FROM ". $tabla;

			try {
				// Preparar la sentencia
				$comando = $bd->prepare($consulta);
				$comando->execute();
				$row = $comando->fetch(PDO::FETCH_ASSOC);

				if($row){		                        
					return $row[$campo];								
				}
				else{
					return 0;
				}

			} catch (PDOException $e) {
				// Aquí puedes clasificar el error dependiendo de la excepción
				// para presentarlo en la respuesta Json
				//echo $e;
				return $e;
			} 

		}


		//obtener la lista de actividades
		public static function obtener_lista_actividades($bd){
				
			$estado = "";
			$consulta = "SELECT * FROM actividades";

			try {
				$comando = $bd->prepare($consulta);
				$comando->execute();

				$row = $comando->fetchAll(PDO::FETCH_ASSOC);

				if($row){
					$codigo = '';
					foreach ($row as $actividades) {
						
						if($actividades["activa"]==10)
							$estado = "active";
						else
							$estado = "noactive";

							$codigo.='<tr class="'. $estado .'">';
							
							$codigo.='<td class="v-align-middle">' .$actividades["actividad"]. '</td>
							<td class="v-align-middle text-center">
								<div id="'.$actividades["idactividades"].'" class="button_on toggle-haractive pr0 text-left">
								   <i class="fa fa-toggle-on on" title="Activo" ';
									if($actividades["activa"]==0) $codigo .= 'style="display:none" ';
									$codigo.='></i><i class="fa fa-toggle-off off" title="No activo"';
									if($actividades["activa"]==10) $codigo .= 'style="display:none" ';
								$codigo.='></i></div>                                
							</td>
							<td style="text-align:right">
								<div class="btn-group">
									<button class="btn btn-mini btn-default btn-demo-space"><a href="actividades.php?id='. $actividades["idactividades"] .'"><i class="fa fa-edit"></i></a></button>
									<button class="btn btn-mini btn-default dropdown-toggle btn-demo-space" data-toggle="dropdown" aria-expanded="false"> <span class="caret"></span> </button>
									<ul class="dropdown-menu">
										<li><a href="actividades.php?id='. $actividades["idactividades"] .'">Editar</a></li>
										<li><a href="#" data-toggle="modal" data-target="#modal-borrar" onclick="borrar_actividades('. $actividades["idactividades"] .',\''. $actividades["actividad"] .'\')">Borrar</a></li>
									</ul>

								</div>
								
							</td>
							</tr>'; 

					}
					return $codigo;
				}
				else{ 
					// caso en el que no hay nunguna actividades creada
					$codigo.='<tr>
							    <td class="v-align-middle" colspan="6">No hay ninguna actividad guardada</td>
							</tr>';
				}

			} catch (Exception $e) {
				echo $e;
				return false;
			}
		}//FIN FUNCION
		

		public static function obtener_ckeckbox_actividades_analogas($bd, $id_actividad){

			$consulta = "SELECT * FROM actividades where idactividades != ?";

				try {

					$comando = $bd->prepare($consulta);
					$comando->execute(array($id_actividad));
					$row = $comando->fetchAll(PDO::FETCH_ASSOC);
					$codigo ='';

					if($row){
						$codigo ='';
						$cont = 1;
						foreach ($row as $actividad) {
							
							if ($cont%2!=0)
							$codigo.='<label for="checks" class="col-md-4 col-sm-5 control-label"></label>';
 
	                        $codigo.='<div class="col-md-3 col-sm-3 col-xs-3">
	                            <div class="checkbox col-md-12 col-sm-12 col-xs-12 pl0 pr0">
	                              <input type="checkbox" id="'. $actividad["idactividades"] .'" value="'. $actividad["idactividades"] .'" class="analogas"';
	                              	
	                              	

	                              	if(Actividades::verificar_actividad_analoga_a($bd, $id_actividad, $actividad["idactividades"]) == true){
	                              		$codigo.='checked/>';
	                              	}
	                              	else
	                              	if(Actividades::verificar_actividad_analoga_b($bd, $id_actividad, $actividad["idactividades"]) == true){
	                              		$codigo.='checked/>';
	                              	}
	                              	else{
	                              		$codigo.='/>';
	                              	}

	                                $codigo.='<label for="'. $actividad["idactividades"] .'">'. $actividad["actividad"] .'</label>
	                            </div>
	                        </div>';

	                        $cont++;
						 }

						 return $codigo;
					}
	
				} catch (Exception $e) {
					echo $e;
					return false;
				}
		}//FIN FUNCION


		
		public static function verificar_actividad_analoga_a($bd, $id_actividad_a, $id_actividad_b){

			$consulta = "SELECT idactividad_b FROM actividades_analogas WHERE idactividad_a = ? AND idactividad_b = ?";

				try {

					$comando = $bd->prepare($consulta);
					$comando->execute(array($id_actividad_a,$id_actividad_b));
					$row = $comando->fetch(PDO::FETCH_ASSOC);
					$codigo ='';

					if($row){
						return true;
					}
					return false;
	
				} catch (Exception $e) {
					echo $e;
					return false;
				}
		}//FIN FUNCION

		public static function verificar_actividad_analoga_b($bd, $id_actividad_a, $id_actividad_b){

			$consulta = "SELECT idactividad_a FROM actividades_analogas WHERE idactividad_b = ? AND idactividad_a = ?";

				try {

					$comando = $bd->prepare($consulta);
					$comando->execute(array($id_actividad_a,$id_actividad_b));
					$row = $comando->fetch(PDO::FETCH_ASSOC);
					$codigo ='';

					if($row){
						return true;
					}
					return false;
	
				} catch (Exception $e) {
					echo $e;
					return false;
				}
		}//FIN FUNCION

		
		public static function obtener_medios($bd, $id_actividad){

			$consulta = "SELECT idetiqueta,etiqueta,ce.idce,categoria FROM etiquetas e INNER JOIN categoria_etiquetas ce ON e.idce = ce.idce";

				try {

					$comando = $bd->prepare($consulta);
					$comando->execute();

					$row = $comando->fetchAll(PDO::FETCH_ASSOC);
					
					$codigo ='';

					if($row){
						$codigo ='';
						$cont = 1;
						$id_etiqueta_an = 1;

						foreach ($row as $etiquetas) {
								
							if($cont%2!=0){
	                    		if($id_etiqueta_an != $etiquetas["idce"]){
	                    			$codigo.='<div class="clearfix p-t-10"></div>';
	                    			$codigo.='<label for="checks" class="col-md-4 col-sm-5 control-label">'. $etiquetas["categoria"] .'</label>';
	                    		}
	                    		else{
	                    			if($cont==1)
	                    				$codigo.='<label for="checks" class="col-md-4 col-sm-5 control-label">'. $etiquetas["categoria"] .'</label>';
	                    			else
	                    				$codigo.='<label for="checks" class="col-md-4 col-sm-5 control-label"></label>';
	                    		} 

	                    	}
						
	                        $codigo.='<div class="col-md-3 col-sm-3 col-xs-3">
	                            <div class="checkbox col-md-12 col-sm-12 col-xs-12 pl0 pr0">
	                              <input type="checkbox" id="'. $etiquetas["etiqueta"] .'" value="'. $etiquetas["idetiqueta"] .'" class="etiquetas"';
	                              	
	                              	if(Actividades::verificar_etiqueta($bd, $id_actividad, $etiquetas["idetiqueta"]) == true){
	                              		$codigo.='checked/>';
	                              	}
	                              	else{
	                              		$codigo.='/>';
	                              	}

	                                $codigo.='<label for="'. $etiquetas["etiqueta"] .'">'. $etiquetas["etiqueta"] .'</label>
	                            </div>
	                        </div>';

	                        $cont++;
	                        	                        
						 }

						 return $codigo;
					}
	
				} catch (Exception $e) {
					echo $e;
					return false;
				}
		}//FIN FUNCION	

		public static function verificar_etiqueta($bd, $id_actividad ,$id_etiqueta){

			$consulta = "SELECT idetiqueta FROM etiqueta_actividad WHERE idactividades = ? AND idetiqueta = ?";
				try {
					$comando = $bd->prepare($consulta);
					$comando->execute(array($id_actividad,$id_etiqueta));

					$row = $comando->fetch(PDO::FETCH_ASSOC);
					
					$codigo ='';
					if($row){
						return true;
					}
					return false;
	
				} catch (Exception $e) {
					echo $e;
					return false;
				}
		}//FIN FUNCION	


		//obtener el sql para la carga masiva de datos
		public static function obtener_sql_masivo($data,$id){

			$sql="";

				for($j=0; $j<count($data); $j++){
					//saco el valor de cada elemento
					$dato = $data[$j];

					if($sql!="")
						$sql .= ",";

					$sql .= "(" . $dato . "," . $id . ")";
				}

			return $sql;
		}		

	}



